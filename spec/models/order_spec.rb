require 'spec_helper'

describe Order do
  before(:each) do
    @order = Factory.build(:order)
  end

  describe "Notified" do
    it "Should be true if it is notified" do
      @order.is_notified = true
      @order.notified?.should eql(true)
    end
    it "Should be false if it is not notified" do
      @order.notified?.should eql(false)
    end
  end

  describe "Canceled" do
    it "Should be true if it is marked as canceled" do
      @order.canceled_by = User.last.id
      @order.cancelled?.should eql(true)
    end
    it "Should be false if it is not marked as canceled" do
      @order.cancelled?.should eql(false)
    end
  end

  describe "Confirmed" do
    it "Should be true if it is marked as confirmed" do
      @order.confirmed_by = User.last.id
      @order.confirmed?.should eql(true)
    end
    it "Should be false if it is not marked as canceled" do
      @order.confirmed?.should eql(false)
    end

  end

  describe "Paid" do
    it "Should not be happened if order is not notified" do
      @order.deal = Deal.last
      @order.is_notified = false
      @order.paid?.should eql(false)
    end
    it "should be happened if order is notified and payments are VALID" do
      @order.deal = Deal.last
      @order.is_notified = false
      @order.save
    end
  end

end