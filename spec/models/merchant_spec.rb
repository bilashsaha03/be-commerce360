require 'spec_helper'

describe Merchant do
  before (:each) do
    @merchant=Merchant.new
    @attr={:name => "test merchant", :address => "Gulshan"}
  end

  it "should create a valid merchant" do
    proc {
      merchant = Merchant.create!(@attr)
      merchant.new_record?.should be(false)
    }.should change(Merchant, :count)

  end

  it "should have a name" do
    no_merchant_name = Merchant.new(@attr.merge(:name => ""))
    no_merchant_name.should_not be_valid
  end

  it "should have a address" do
    no_merchant_address = Merchant.new(@attr.merge(:address => ""))
    no_merchant_address.should_not be_valid
  end


end
#pending "add some examples to (or delete) #{__FILE__}"