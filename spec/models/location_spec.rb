require 'spec_helper'

describe Location do
  before(:each) do
    @location = Factory.build(:location)
  end

  it "Should be created with valid location information" do
    @location.should be_valid
    count_before_save = Location.count
    @location.save.should eql(true)
    @location.errors.full_messages.size.should eql(0)
    Location.count.should eql(count_before_save + 1)
  end
end
