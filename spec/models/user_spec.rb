require 'spec_helper'


describe User do
  before(:each) do
    @user = User.new
    @attr = {:email => 'user1000@akhoni.com', :password => 'user123456', :phone => '8801716429700', :is_subscribed => true}

  end

  it "should create a valid user" do
    proc {
      user = User.create!(@attr)
      user.new_record?.should be(false)
    }.should change(User, :count)
  end

  it "should require an email address" do
    no_email_user = User.new(@attr.merge(:email => ""))
    no_email_user.should_not be_valid
  end

  it "should reject duplicate email addresses" do
    # Put a user with given email address into the database.
    User.create!(@attr)
    user_with_duplicate_email = User.new(@attr)
    user_with_duplicate_email.should_not be_valid
  end


  it "should reject email addresses identical up to case" do
    upcase_email = @attr[:email].upcase
    User.create!(@attr.merge(:email => upcase_email))
    user_with_duplicate_email = User.new(@attr)
    user_with_duplicate_email.should_not be_valid
  end

  #it "should accept valid email addresses" do
  #  addresses = %w[user@akhoni.com THE_USER@akhoni.dhaka.org first.last@akhoni.jp]
  #  addresses.each do |address|
  #    valid_email_user = User.new(@attr.merge(:email => address))
  #    valid_email_user.should be_valid
  #  end
  #end

  it "should reject invalid email addresses" do
    addresses = %w[user@akhoni,com user_at_akhoni.org example.user@akhoni.]
    addresses.each do |address|
      invalid_email_user = User.new(@attr.merge(:email => address))
      invalid_email_user.should_not be_valid
    end
  end

  describe "Phone" do
    it "should not be accepted when it is invalid" do
      phones = ["qwewq" "" "111111" "2222222222222" "017" "01711", "8801311270650"]
      phones.each do |phone|
        user = User.create(@attr.merge(:phone => phone))
        user.should_not be_valid
      end
    end

    it "Should be accepted when it is valid mobile phone" do
      phone = "88017"+ random_number(8)
      user = User.create(@attr.merge(:phone => phone))
      user.errors.full_messages.size.should eql(0)
    end

    it "Should be without country code" do
      phone = "88017"+ random_number(8)
      user = User.new(@attr.merge(:phone => phone))
      user.phone_without_country_code.should eql(phone[2, phone.length])
    end

    it "Should be set with country code when country code is not present in it" do
      phone = "017"+ random_number(11)
      user = User.new(@attr.merge(:phone => phone))
      user.set_phone.should eql('88' + phone)
    end

    it "Should not be set country code when country code is present in it" do
      phone = "88" + User::PHONE_INITIALS[rand(6)] + random_number(8)
      user = User.new(@attr.merge(:phone => phone))
      user.set_phone.should eql(phone)
    end
  end

  it "Should checks an user is member" do
    user = User.new(@attr)
    user.roles_mask = 32
    user.member?.should eql(true)
    user.roles_mask = nil
    user.member?.should eql(true)
  end

  it "Should returns 0.0 referral credit for a newly created user" do
    user = User.create(@attr)
    user.referral_balance.to_i.should eql(0)
  end

  describe CorporateDiscount do
    it "Should return corporate discount" do
      user = Factory.build(:user)
      user.email = 'some@aaa.com'
      user.corporate_discount(user).should == nil
    end

    it "should not return nil when user has corporate discount" do
      user = Factory.build(:user)
      user.email = 'abc@akhoni.com'
      puts user.inspect
      user.corporate_discount(user).should_not be_nil
    end
  end


end