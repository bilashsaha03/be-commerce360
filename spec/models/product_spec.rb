require 'spec_helper'

describe Product do
  before(:each) do
    @product = Factory.build(:product)
  end
  it "Should be created with valid product information" do
    @product.should be_valid
    count_before_save = Product.count
    @product.save.should eql(true)
    @product.errors.full_messages.size.should eql(0)
    Product.count.should eql(count_before_save + 1)
  end
  describe "Campaign Name" do
    it "Should return product name with campaign name concatenated if campaign name exists" do
      @product.title_with_campaign_name.should eql(@product.name + ' - ' + @product.campaign_name)
    end
    it "Should return product name only if campaign name does not exists" do
      @product.campaign_name = nil
      @product.title_with_campaign_name.should eql(@product.name)
    end
  end
end
