require 'spec_helper'
require 'deal'

describe Deal do
  before(:each) do
    @deal = Factory.build(:deal)
  end

  it "should create a valid deal with valid required information" do
    deal_count_before_create = Deal.count
    @deal.should be_valid
    @deal.save
    @deal.errors.size.should eql(0)
    deal_count_before_create.should eql(Deal.count - 1)
  end


  it "should have a title" do
    no_title_deal = Factory.build(:deal, :title => "")
    no_title_deal.should_not be_valid
  end


  it "should have a description" do
    no_description_deal = Factory.build(:deal, :description => "")
    no_description_deal.should_not be_valid
  end


  it "should require actual price" do
    no_actual_price_deal = Factory.build(:deal, :actual_price => "")
    no_actual_price_deal.should_not be_valid
  end

  it "should require discounted price" do
    no_discounted_price_deal = Factory.build(:deal, :discounted_price => "")
    no_discounted_price_deal.should_not be_valid
  end

  it "should require start date" do
    no_start_date_deal = Factory.build(:deal, :start_date => "")
    no_start_date_deal.should_not be_valid
  end

  it "should require end date" do
    no_end_date_deal = Factory.build(:deal, :end_date => "")
    no_end_date_deal.should_not be_valid
  end

  it "should have a marchant " do
    no_marchant_deal = Factory.build(:deal, :merchant_id => "")
    no_marchant_deal.should_not be_valid
  end

  it "not mandatory to have a copyrighter " do
    deal_no_copyrighter = Factory.build(:deal, :copyrighter_id => "")
    deal_no_copyrighter.should be_valid
  end

  it "should accept duplicate title deal" do
    deal = Factory.build(:deal, :title => 'new test title')
    deal.save
    deal = Factory.build(:deal, :title => 'new test title')
    deal.should be_valid
    deal.save
    deal.errors.full_messages.size.should eql(0)
  end


  it "Should be active when current date is in between start date and end date and it's status is approved" do
    @deal.start_date = 2.days.ago
    @deal.end_date = Date.today + 2
    @deal.active?.should eql(true)
  end

  it "Should be inactive when current date is in between start date and end date and it's status is pending" do
    @deal.status = 'pending'
    @deal.start_date = 2.days.ago
    @deal.end_date = Date.today + 2
    @deal.active?.should eql(false)
  end

  it "Should be inactive when current date is not in between start date and end date and even deal is approved" do
    @deal.start_date = Date.today - 10
    @deal.end_date = Date.today - 1
    @deal.active?.should eql(false)
  end

  it "Should be past when end date is greater than current date" do
    @deal.end_date = Date.today - 1
    @deal.past?.should eql(true)
  end

  describe "Sold Out" do
    it "Should not be happened if maximum quantity is 0" do
      @deal.max_quantity = 0
      @deal.sold_out?.should eql(false)
    end
  end

  describe "Campaign Name" do
    it "Should return deal title with campaign name concatenated if campaign name exists" do
      @deal.title_with_campaign_name.should eql(@deal.title + ' - ' + @deal.campaign_name)
    end
    it "Should return deal title only if campaign name does not exists" do
      @deal.campaign_name = nil
      @deal.title_with_campaign_name.should eql(@deal.title)
    end
  end

  describe "Running" do
    it "Should be true if it is approved and current date is in valid_from and valid_to date range " do
      @deal.running?.should eql(true)
    end
    it "Should be false if it is approved and current date is not in valid_from and valid_to date range " do
      @deal.valid_to = Date.today - 10
      @deal.valid_from = Date.today - 1
      @deal.running?.should eql(false)
    end
    it "Should be false if it is pending and current date is in valid_from and valid_to date range " do
      @deal.status = Deal::statuses[0]
      @deal.running?.should eql(false)
    end
  end

  describe "Parent" do
    it "Should be if it's parent id is nil" do
      @deal.parent_deal?.should eql(true)
    end
    it "Should not be if it's parent id is not nil" do
      @deal.parent_id = Deal.first.id
      @deal.parent_deal?.should eql(false)
    end
  end

  describe "Discount" do
    it "Should be 100% if actual price is 0" do
      @deal.actual_price = 0
      @deal.discount.should eql(100)
    end
    it "Should be 100% if discounted price is 0" do
      @deal.discounted_price = 0
      @deal.discount.should == 100
    end
    it "Should be less than 100% if actual price is greater than discounted price" do
      @deal.discount.should < 100
    end
  end

  it "Should return discount amount" do
    @deal.discount_amount.should_not be_nil
  end

  describe "Highest Discount" do
    it "Should be 98% as sub_deal2 has highest discount" do
      parent_deal = Deal.last
      sub_deal1 = Factory.create(:deal, :parent_id => parent_deal.id, :discounted_price => 100)
      sub_deal2 = Factory.create(:deal, :parent_id => parent_deal.id, :discounted_price => 10)
      parent_deal.highest_discount.should == 98
    end
    it "Should be 90% as parent deal has highest discount" do
      parent_deal = Factory.create(:deal)
      sub_deal1 = Factory.create(:deal, :parent_id => parent_deal.id, :discounted_price => 400)
      sub_deal2 = Factory.create(:deal, :parent_id => parent_deal.id, :discounted_price => 300)
      parent_deal.highest_discount.should == 90
    end
  end

  describe "Edit accessibility" do
     it "Should have edit access if current user is superadmin whether a deal is pending or approved" do
       deal = Factory.build(:deal)
       user = User.last
       user.roles_mask = 1
       deal.status = Deal::statuses[0].to_s  #Deal is pending
       deal.have_edit_access?(user).should eql(true)
       deal.status = Deal::statuses[1].to_s  #Deal is approved
       deal.have_edit_access?(user).should eql(true)
     end
     it "Should have edit access if deal is pending" do
       deal = Factory.build(:deal)
       user = User.last
       user.roles_mask = 256 # The user is crm
       deal.status = Deal::statuses[0].to_s #Deal is pending
       deal.have_edit_access?(user).should eql(true)
     end
    it "Should not have edit access if deal is approved" do
       deal = Factory.build(:deal)
       user = User.last
       user.roles_mask = 256 # The user is crm
       deal.status = Deal::statuses[1].to_s #Deal is pending
       deal.have_edit_access?(user).should eql(false)
     end
  end


  it "Should give a type depends on it's type id" do
    @deal.type_id = rand(3)
    @deal.type_name.should_not be_nil
  end

end
