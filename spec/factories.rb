require "#{Rails.root}/spec/spec_utility"
include SpecUtility


Factory.define :user do |f|
  f.sequence(:email){|n| "master-tester-#{n}@akhoni.com" }
  f.password                          "top-secrete"
  f.password_confirmation             { |u| u.password }
  f.firstname                         "first-name-#{rand(10)}"
  f.address                           "dhaka-#{rand(10)}"
  f.phone                             MobileTransaction::COUNTRY_CODE + User::PHONE_INITIALS[rand(5)] + random_number(8)
  f.roles_mask                        1
end

Factory.define :location do |f|
  f.name      "City - #{rand(10)}"
  f.url       {|location| location.name.downcase}
end

Factory.define :deal do |f|
  f.sequence(:title){|n| "test-deal-#{n}" }
  f.description             "test deal description"
  f.actual_price            500
  f.discounted_price        50
  f.copyrighter_id          1
  f.status                  'approved'
  f.min_quantity            1
  f.max_quantity            100
  f.max_order_limit         1
  f.merchant_id             1
  f.start_date              2.days.ago
  f.end_date                Date.today + 10
  f.valid_from              {|deal| deal.start_date}
  f.valid_to                {|deal| deal.end_date}
  f.campaign_name           '001'
  f.parent_id               nil
end

Factory.define :product do |f|
  f.name                "test product name"
  f.description         "test product description"
  f.quantity            10
  f.actual_price        500
  f.discounted_price    100
  f.location_id         1
  f.merchant_id         1
  f.start_date          2.days.ago
  f.end_date            Date.today + 10
  f.campaign_name       '001'
end

Factory.define :order do |f|
  f.quantity      1
  f.booking_at    Date.today
  f.user          User.last
end

Factory.define :corporate_discount do |f|
  f.domain      'akhoni.com'
  f.discount     10
  f.item_id      1
  f.item_type    'deal'
end



