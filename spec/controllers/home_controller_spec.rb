require 'spec_helper'

describe HomeController do

  describe "static pages" do
    it "should be successful on about" do
      get 'about'
      response.should be_success
    end

    it "should be successful how" do
      get 'how'
      response.should be_success
    end
  end
end
