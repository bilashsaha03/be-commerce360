require 'spec_helper'

describe BusinessesController do
  include Devise::TestHelpers
  describe "GET new" do

    it "should get 200 status" do
      current_user= User.first
      #puts current_user.email
    sign_in current_user
      get :new
      response.code.should eq("200")
    end
  end
end