require 'spec_helper'

describe Manage::DealsController, "creating a new deal" do
  include Devise::TestHelpers

  before do
    current_user= User.first
    sign_in current_user
    @attributes = {:title => 'test deal', :description => 'test deal',
                   :actual_price => 500, :discounted_price => 50,
                   :merchant_id =>1,
                   :end_date => "2011-04-30", :start_date => "2011-04-23"
    }
    @deal = mock_model(Deal)
    #Deal.expects(:new).with(@attributes).once.and_return(@deal)
    Deal.expects(:new).at_least_once
  end

  it "should redirect to index with a notice on successful save" do
    #@deal.expects(:save).with().once.and_return(true)
    @deal.expects(:save).at_least_once
    post :create, :deal => @attributes
    assigns[:deal].should_not be(@deal)
    assigns[:deal][:title].should equal('test deal')
    flash[:notice].should_not be(nil)
    response.should redirect_to(admin_deals_path)
  end

  it "should re-render new template on failed save" do
    #@deal.expects(:save).with().once.and_return(false)
    @deal.expects(:save).at_least_once
    post :create, :deal => @attributes

    assigns[:deal].should be(@deal)
    flash[:notice].should be(nil)
    response.should be_success
    response.should render_template('new')
  end

end