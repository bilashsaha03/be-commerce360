var locale = 'en'; // Global variable which can be accessed from any where

//** Extending JS String class with to_bddigit() method which can be called from anywhere within the String Object **//

String.prototype.to_bddigit = function () {
    if (locale && locale == 'en') {
        return this;
    }
    translation_map = ['০', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯']
    text = this.split('');
    translated_text = "";
    for (var i = 0; i < this.length; i++) {
        if (isNaN(text[i]) || text[i] == ' ')
            translated_text += text[i]
        else
            translated_text += translation_map[text[i]]
    }
    return translated_text;
}

function initialize_tiny_mce(elements) {
    tinyMCE.init({
        theme:"advanced",
        theme_advanced_buttons1:"bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,|,cut,copy,paste,bullist,numlist,|,undo,redo,|,link,unlink",
        //theme_advanced_buttons2 : "",
        theme_advanced_buttons3:"",
        theme_advanced_buttons4:"",
        theme_advanced_toolbar_location:"top",
        theme_advanced_toolbar_align:"left",
        auto_resize:true,
        mode:"exact",
        elements:elements
    })
}

//** Tests to see whether a phone is given in correct mobile format(i,e 01711222888) or not **//
function isPhoneValid(phone) {
    var isValid = false;
    jQuery.each(["011", "015", "016", "017", "018", "019"], function () {
        if (phone.match("^" + this) != null) {
            isValid = true
            return true;
        }
    });
    return isValid;
}

//** Tests to see whether a phone is given in correct mobile format(i,e 01711222888) or not **//
function isbKash(phone) {
    var isValid = false;
    jQuery.each(["017", "018", "019"], function () {
        if (phone.match("^" + this) != null) {
            isValid = true
            return true;
        }
    });
    return isValid;
}

//** Calculates total payable amounts based on visible price only **//
function calculatePayableAmount(){
    var amount = 0;
    $(".charge_discounts").each(function(){
      if($(this).is(':visible')){
        amount += parseInt($(this).text());
      }
    });
    return amount;
}

function displayCalculatedPayableAmount(){
    var total_calculated_amount = calculatePayableAmount().toFixed(1);
    $("#total_calculated_amount").text(total_calculated_amount);
    $("#total_amount").val(total_calculated_amount);
}

$(document).ready(function () {
    displayCalculatedPayableAmount();
    function popupCenter(url, width, height, name) {
        var left = (screen.width / 2) - (width / 2);
        var top = (screen.height / 2) - (height / 2);
        return window.open(url, name, "menubar=no,toolbar=no,status=no,width=" + width + ",height=" + height + ",toolbar=no,left=" + left + ",top=" + top);
    }

    $("a.popup").click(function (e) {
        popupCenter($(this).attr("href"), $(this).attr("data-width"), $(this).attr("data-height"), "authPopup");
        e.stopPropagation();
        return false;
    });

    /*ORDER PAGE*/
    function calculatePayment(obj) {
        var unit_price = new Number($('#deal_discounted_price').val());
        var quantity = new Number(obj.val());
        var discounted_price_cc = new Number($('#discounted_price_cc').val());
//        if (quantity == 0) {
//            quantity = 1;
//            $('#order_quantity').val(quantity);
//        }
        $('#total_price').html(quantity * unit_price);
        $('#total_price_cc_discounted').html(quantity * discounted_price_cc);
    }

    $('#order_submit').attr('disabled', 'disabled');

    $('#agree').click(function () {
        if (this.checked) {
            $('.btn-submit').removeAttr('disabled');
        } else {
            $('.btn-submit').attr('disabled', 'disabled');
        }
    });


    $('#order_submit').click(function () {
        if ($('#order_quantity').val() != undefined) {
            var quantity = parseInt($('#order_quantity').val());
            if (quantity > 0) {
            } else {
                $('#order_quantity').attr('style', "border:1px solid red");
                $('#error-quantity').attr('style', "color:red;font-size:12px;background-color: #F0F0F0;border: 1px solid red;margin: 10px 10px 10px 10px;padding: 4px 4px 0;cols=40;");
                $('#error-quantity').html('Quantity must be greater than zero');
                return false;
            }
        }
    });


    //** Javascript validation for friends info in order submit**//

    $('#order_submit').click(function () {
        if ($('#friend_info').attr('checked') == 'checked') {
            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            var reg_phone = /^\+?([0-9]|-)+$/;
            var fname = $('#gift_info #friend_firstname');
            var email = $('#gift_info #friend_email');
            var phone = $('#gift_info #friend_phone');
            var address = $('#gift_info #friend_address');
            var mesg = ""
            if (fname.val() == null || fname.val() == "") {
                $('#gift_info #friend_firstname').attr('style', "border:1px solid red");
                mesg = mesg + " > " + fname.attr('blank_msg') + "<br />"
            }
            else {
                $('#gift_info #friend_firstname').removeAttr("style");
            }

            if (email.val() == null || email.val() == "") {
                $('#gift_info #friend_email').attr('style', "border:1px solid red");
                mesg = mesg + " > " + email.attr('blank_msg') + "<br />"
            }
            else if (reg.test(email.val()) == false) {
                $('#gift_info #friend_email').attr('style', "border:1px solid red");
                mesg = mesg + '> Friend\'s Email Address is Invalid.<br/> '
            }
            else {
                $('#gift_info #friend_email').removeAttr("style");
            }

            if (phone.val() == null || phone.val() == "") {
                $('#gift_info #friend_phone').attr('style', "border:1px solid red");
                mesg = mesg + " > " + phone.attr('blank_msg') + "<br />"
            }
            else if (reg_phone.test(phone.val()) == false) {
                $('#gift_info #friend_phone').attr('style', "border:1px solid red");
                mesg = mesg + '> Friend\'s Phone Number is Invalid.<br/> '
            }
            /*else if (phone.val().length != 11) {
                $('#gift_info #friend_phone').attr('style', "border:1px solid red");
                mesg = mesg + '> Friend\'s Phone is the wrong length (should be 11 characters)<br/> '
            }*/
            /*else if (!isPhoneValid(phone.val())) {
                $('#gift_info #friend_phone').attr('style', "border:1px solid red");
                mesg = mesg + '> Friend\'s Phone Number is incorrect format .<br/> '
            }*/
            else {
                $('#gift_info #friend_phone').removeAttr("style");
            }

            if (address.val() == null || address.val() == "") {
                $('#gift_info #friend_address').attr('style', "border:1px solid red;width:600px");
                mesg = mesg + " > " + address.attr('blank_msg') + "<br />"
            }
            else {
                $('#gift_info #friend_address').removeAttr("style");
            }

            if (mesg != '') {
                $('#error').attr('style', "color:red;font-size:12px;background-color: #F0F0F0;border: 1px solid red;margin: 10px 10px 10px 10px;padding: 4px 4px 0;cols=40;");
                $('#error').html(mesg);
                return false;
            }
            else {
                $('#error').html("");
                $('#error').removeAttr("style");
            }
        }
    });

    $('#friend_info').click(function () {
        if (this.checked) {
            $('#gift_info').show();
        } else {
            $('#gift_info').hide();
        }
    });

    if ($('#order_quantity').length == 1) {
        calculatePayment($('#order_quantity'));
    }

    $('#order_quantity').keyup(function () {
        var quantity = $(this).val();
        var valid_chars = "0123456789";
        for (var i = 0; i < quantity.length; i++) {
            if (valid_chars.indexOf(quantity.charAt(i)) == -1) {
                $(this).val('');
                break;
            }
        }
        calculatePayment($(this));
    });

    $('#order_quantity').blur(function () {
        var quantity = $(this).val();
        if (quantity == '') {
            $(this).val(1);
        }
        calculatePayment($(this));
    });

    $('ul.product-size li.available').click(function () {
        var size_id = $(this).attr('size_id');
        var size_name = $(this).html();
        //Remove selection from all items
        $('.available').each(function () {
            $(this).removeClass('selected');
        });
        //Mark as selected current item
        $(this).addClass('selected');
        $('#selected_size_id').val(size_id)
        $('#selected_size_name').html(size_name)
    });

    $('.buy_gilt_item').click(function () {
        var link = $(this).attr('link');
        var size_id = $('#selected_size_id').val();

        if (($("ul.product-size li.selected").size() <= 0) || size_id == '') {
            //$('#selected_size_name').html('<span style="color:red;">Please select size</span>')
            alert($('span#select_size').text());
            return false;
        } else {
            document.location.href = link + '?size_id=' + size_id;
        }
    });

    $('.friend_button').click(function () {
        var link = $(this).attr('link');
        var size_id = $('#selected_size_id').val();

        if (($("ul.product-size li.selected").size() <= 0) || size_id == '') {
            $('#selected_size_name').html('<span style="color:red;">Please select size</span>')
            return false;
        } else {
            document.location.href = (link + '&size_id=' + size_id).replace(' ', '');
        }
    });


    $('#live_chat_link').click(function () {
        var win = window.open('http://www.google.com/talk/service/badge/Start?tk=z01q6amlqi6i373a1al6dsjist3ha4rr0alv0sd49tj3ga6rm2lvrbbtm3675h0jo89ffg2s934a72nqoueq0qulegn3okma91bodglsllbjdljm6knoh55k5erkjpmivfudicqjcs4oietc89h79k7u7bk23klimgca7drjp', 'chatwin', 'width=500,height=500')
        win.focus();
    });


    //Blink input box
    $.fn.blinkme = function (options) {
        var defaults = {
            bordereffect:true,
            bordercolor:'red',
            backgroundeffect:true,
            backgroundcolor:'yellow',
            blinkinterval:400,
            eventstoattach:'blur',
            eventstodetach:'focus'
        };
        options = $.extend(defaults, options);
        return this.each(function () {
            var elem = $(this);
            var intervalobj = null;
            var bordercolor = elem.css("border-color");
            var bgcolor = elem.css("backgroundColor");
            //elem.bind(options.eventstoattach, function () {
            var blinked = false;
            intervalobj = setInterval(function () {
                if (blinked == true) {
                    elem.css({
                        'backgroundColor':bgcolor,
                        'border-color':bordercolor
                    });
                    blinked = false;
                }
                else {
                    if (options.bordereffect == true) {
                        elem.css({
                            'border-color':options.bordercolor
                        });
                    }
                    if (options.backgroundeffect == true) {
                        elem.css({
                            'backgroundColor':options.backgroundcolor
                        });
                    }
                    blinked = true;
                }
            }, options.blinkinterval);
            //});
            elem.bind(options.eventstoattach, function () {
                elem.blinkme();
            });
            elem.bind(options.eventstodetach, function () {
                if (intervalobj) {
                    clearInterval(intervalobj);
                    elem.css({
                        'backgroundColor':bgcolor,
                        'border-color':bordercolor
                    });
                }
            });
        });
    }

    //Show order actions
//    $("tr.order-row").live({
//        mouseover: function() {
//            $('#action_' + $(this).attr('id')).show();
//        },
//        mouseout: function() {
//            $('#action_' + $(this).attr('id')).hide();
//        }
//    });

    //Date filter changability
    $('.date_filter_selector').click(function () {
        elm = $(this).attr('for');
        if (this.checked) {

            $('.' + elm).each(function () {
                $(this).removeAttr('disabled');
            });
        } else {
            $('.' + elm).each(function () {
                $(this).attr('disabled', 'disabled');
            });
        }
    });

    //validation for products
    $("[id*=product_sizes_attributes]").live({
        change:function (event) {
            var flag_exists = false;
            var flag_added = false;
            var matched = 0;
            var current_selection = $("select.[id=" + event.target.id.trim() + "] option:selected").html();

            $('#all_sizes .name').each(function () {
                if (current_selection != null)
                    if (current_selection.trim() === $(this).html().trim()) {
                        flag_exists = true;
                    }
            });
            $("[id*=product_sizes_attributes]").each(function () {
                var matching_selection = $("select.[id=" + $(this).attr('id') + "] option:selected").html();
                if (matching_selection != null)
                    if (current_selection.trim() === matching_selection.trim())
                        matched++;
            });

            if (flag_exists) {
                var answer = confirm("Selected Size already exists.Remove it before adding new one?")
                if (answer) {
                    $('#all_sizes .name').each(function () {
                        if (current_selection != null) {
                            if (current_selection.trim() === $(this).html().trim()) {
                                $('#' + $(this).parent().parent().attr('id').trim() + ' td [type=checkbox]').prop("checked", true);
                            }
                        }
                    });
                }
                else {
                    $(this).val("");
                }
            }
            if (matched > 1 && !flag_exists) {
                alert('Selected Size already added');
                $(this).val("");
            }

        }
    });

    $('.lightbox a').live('mouseover', function () {
        $(this).fancybox({
            width:800,
            height:350,
            opacity:true
        });
    });

    //**  bKash mobile selection **//
    $('#pay_cash_on_coupon_deal_delivery, #pay_cash_on_coupon_product_delivery, #pay_cash_by_credit_debit_card, #pay_by_referral_credit').click(function () {
        $('#mobile_wrapper').css('display', 'none');
    });
    $('#pay_cash_by_mobile').click(function () {
        $('#mobile_wrapper').css('display', 'block');
    });

    //** Credit card charge or discount//
    $("#pay_cash_by_credit_debit_card").click(function(){
      if($("#credit_card_payment")){
          $("#credit_card_payment").show();
          displayCalculatedPayableAmount();
      }

    });

    $("#pay_cash_on_coupon_deal_delivery, #pay_cash_on_coupon_product_delivery, #pay_cash_by_mobile").click(function(){
        $("#credit_card_payment").hide();
        displayCalculatedPayableAmount();
    })

    //** Pay Now **//

    //**Group radio button from different 2 forms**//
    $('#pay_cash_on_coupon_deal_delivery, #pay_cash_on_coupon_product_delivery, #pay_cash_by_credit_debit_card, #pay_cash_by_mobile, #pay_by_referral_credit').click(function () {
        $('.payment_methods').prop('checked', false);
        $(this).prop('checked', true);
    });


    $('#pay_now').click(function () {
        var referral_credit = parseFloat($("#referral_credit").val());
        var payable_order_amount = parseFloat($("#total_amount").val()) + referral_credit;
        var use_referral_credit = $("#checkout_use_referral").is(":checked");
        //** Submits referral form if referral_credit is more than order amount and referral credit checkbox is checked//
        if ($("#checkout_use_referral").is(':checked') && referral_credit >= payable_order_amount) {
            $('#pay_by_referral_credit_form').submit();
            return;
        }
        $('.payment_methods').each(function () {

            if ($(this).attr('checked')) {
                if ($(this).attr('id') == 'pay_cash_by_credit_debit_card') {
                    if (use_referral_credit && referral_credit < payable_order_amount) {
                        $("<input type='hidden' name='use_referral' value='1' >").appendTo($('#cash_or_credit_debit_form'));
                    }
                    $('#cash_or_credit_debit_form').submit();
                }
                else if ($(this).attr('id') == 'pay_cash_on_coupon_deal_delivery' || $(this).attr('id') == 'pay_cash_on_coupon_product_delivery') {
                    var redirect_location = $(this).val();
                    if (use_referral_credit && referral_credit < payable_order_amount)
                        redirect_location = $(this).val() + '?use_referral=1'
                    else
                        redirect_location = $(this).val();

                    window.location = redirect_location;
                    return;
                }

                else if ($(this).attr('id') == 'pay_cash_by_mobile') {

                    var error_message = '';
                    var mobile_field = $('#mobile_number');
                    mobile_field.val(mobile_field.val().replace(/\s/g, "")); // replacing white spaces from mobile number

                    if (mobile_field.val() > '' && isNaN(mobile_field.val())) { // checks whether bkash number is integer or not
                        error_message += '<label>>Your Mobile Number is invalid</label>';
                    }
                    if (mobile_field.val().length == 0) {
                        error_message += '<label>>Enter your bKash Mobile Number</label>';
                    }

                    /*if (!isbKash(mobile_field.val())) {
                        error_message += '<label>>Your Mobile Number should be bKash</label>';
                    }*/

                    if ($('#mobile_transaction_id').val() == '') {
                        error_message += '<label>>Enter Your Transaction ID</label>';
                    }
                    error_message = jQuery.trim(error_message);
                    if (error_message > '') {
                        $('table#mobile_wrapper div#errors').html(error_message);
                        $('table#mobile_wrapper div#errors').attr('style', "color:red;font-size:12px;background-color: #F0F0F0;border: 1px solid red;padding: 4px 4px 0;");
                        return false;
                    }
                    else {
                        $('table#mobile_wrapper div#errors').html("");
                        $('table#mobile_wrapper div#errors').removeAttr("style");
                        if (use_referral_credit && referral_credit < payable_order_amount) {
                            $("<input type='hidden' name='use_referral' value='1' >").appendTo($('#mobile_form'));
                        }
                        $('#mobile_form').submit();
                        return true;
                    }
                }
            }
        })
    });

});


function ResetForm(form_id) {
    var form = $('#' + form_id);
    var elements = ['input', 'select'];
    for (i = 0; i < elements.length; i++) {
        $.each(form.find(elements[i]), function () {
            switch (elements[i]) {
                case 'input':
                    switch ($(this).attr('type')) {
                        case 'text':
                        case 'textarea':
                        case 'hidden':
                        case 'file':
                            $(this).val('');
                            break;
                        case 'checkbox':
                        case 'radio':
                            $(this).attr('checked', false);
                            break;
                    }
                    break;
                case 'select':
                    $(this).val('');
                    break;
            }
        });
    }
}

function initialize_image_slider(element_id) {

    $('#' + element_id + ' img.fast_image').each(function(){
        $(this).attr('src','/images/' + $(this).attr('data_src'));
    })


    $('#' + element_id).nivoSlider({
        effect:'fade', // Specify sets like: 'fold,fade,sliceDown'
        slices:15, // For slice animations
        boxCols:8, // For box animations
        boxRows:4, // For box animations
        animSpeed:500, // Slide transition speed
        pauseTime:5000, // How long each slide will show
        startSlide:0, // Set starting Slide (0 index)
        controlNav:true, // 1,2,3... navigation
        keyboardNav:true, // Use left & right arrows
        pauseOnHover:true // Stop animation while hovering
    });
}

var days = new Array(), hours = new Array(), mins = new Array(), secs = new Array(), tmp_time = new Array();
var message = new Array();

function displayCountdown(deal_id, time_diff, div_id) {
    if (time_diff > 0) {
        secs[deal_id] = time_diff % 60;
        secs[deal_id] = secs[deal_id] < 0 ? 0 : secs[deal_id];

        tmp_time[deal_id] = (time_diff - secs[deal_id]) / 60;
        mins[deal_id] = tmp_time[deal_id] % 60;
        mins[deal_id] = mins[deal_id] < 0 ? 0 : mins[deal_id];

        tmp_time[deal_id] = (tmp_time[deal_id] - mins[deal_id]) / 60;
        hours[deal_id] = tmp_time[deal_id] % 24;
        days[deal_id] = (tmp_time[deal_id] - hours[deal_id]) / 24;

        message[deal_id] = days[deal_id] + (locale && locale == 'en' ? days[deal_id] > 1 ? ' days ' : ' day ' : ' দিন ');
        message[deal_id] += ' ' + ('' + hours[deal_id].length == 1 ? ('0' + hours[deal_id]) : hours[deal_id]);
        message[deal_id] += ':' + ('' + mins[deal_id].length == 1 ? ('0' + mins[deal_id]) : mins[deal_id]);
        message[deal_id] += ':' + (('' + secs[deal_id]).length == 1 ? ('0' + secs[deal_id]) : secs[deal_id]);
        document.getElementById(div_id).innerHTML = message[deal_id].to_bddigit();
        setTimeout('displayCountdown(' + deal_id + ',' + (time_diff - 1) + ',\'' + div_id + '\');', 999);
    }
    else if (time_diff == 0)
        window.location.reload();
}

function check_release(release_time, count_time) {
    if (release_time == count_time) {
        window.location = "http://akhoni.com"
    } else {
        $('#check_time').html(release_time + '::' + count_time);
        setTimeout('check_release(' + release_time + ',' + (count_time + 1) + ');', 999);
    }
}

function sendNotification(url, updator_id) {
    if ($('#' + updator_id).html() < $('#total_users').val()) {
        $.ajax({
            cache:false,
            type:"GET",
            url:url,
            success:function (content) {
                $('#' + updator_id).html(content);
            }
        });
        setTimeout('sendNotification(\'' + url + '\',\'' + updator_id + '\');', 999);
    }
}

$(document).ready(function () {

    /*$(".product_image_hover").hover(
        function () {
            $(this).find('img.product_medium').fadeTo('slow', .6);
            $(this).find('.product_image_hover_wrapper').fadeTo('fast', .8);

        },
        function () {
            $(this).find('img.product_medium').fadeTo('slow', 1);
            $(this).find('.product_image_hover_wrapper').fadeTo('fast', 0);
        }
    );

    $(".travel_image_hover").hover(
        function () {
            $(this).find('img.large').fadeTo('slow', .6);
            $(this).find('.travel_image_hover_wrapper').fadeTo('fast', .8);

        },
        function () {
            $(this).find('img.large').fadeTo('slow', 1);
            $(this).find('.travel_image_hover_wrapper').fadeTo('fast', 0);
        }
    );*/

    $(".product_image_hover, .travel_image_hover").each(function(){
        $(this).find('.product_image_hover_wrapper').fadeTo('fast', .78);
        $(this).find('.travel_image_hover_wrapper').fadeTo('fast', .78);
    })


    $('#corporate_discount_item_type').change(function () {
        if ($(this).val() == 'product') {
            $('#deal_select').hide();
            $('#product_select').show();
        } else if ($(this).val() == 'deal') {
            $('#deal_select').show();
            $('#product_select').hide();
        }
        else {
            $('#deal_select').hide();
            $('#product_select').hide();
        }

    });
    $('#search_item_type_equals').change(function () {
        if ($(this).val() == 'product') {
            $('#deal_search').hide();
            $('#product_search').show();
        } else if ($(this).val() == 'deal') {
            $('#deal_search').show();
            $('#product_search').hide();
        }
        else {
            $('#deal_search').hide();
            $('#product_search').hide();
        }
    });

    $("#checkout_use_referral").click(function () {
        var payable_order_amount = calculatePayableAmount();
        var referral_credit = parseFloat($("#referral_credit").val());
        var flag = true

        //* If user uses referral and referral credit is enough to buy an order then other payment methods will be disabled*//
        if ($(this).is(':checked') && referral_credit >= payable_order_amount) {
            $("#referral_balance").show();
            $("#total_calculated_amount").html(0);
            $('.payment_methods').attr('checked', false).attr('disabled', 'disabled');
            $("#mobile_wrapper").hide();
            $(this).val(1);
            flag = false;
            displayCalculatedPayableAmount();
        }
        else {
            $('.payment_methods').attr('disabled', false);
            $(this).val(0);
            $("#referral_balance").hide();
            displayCalculatedPayableAmount();
        }

        if (flag) {
            //* If user uses referral and referral credit is not enough but have some then user will be shown his payable amount only*//
            if ($(this).is(':checked') && referral_credit < payable_order_amount) {

                $("#referral_balance").show();
                displayCalculatedPayableAmount();
                //* Add use_referral parameter for ssl commerze as user wants to use referral credit*//
                $('.url_for_ssl').each(function () {
                    $(this).val($(this).val().split('&use_referral=1')[0]);
                    $(this).val($(this).val() + '&use_referral=1');
                })
            }
            else {

                $("#referral_balance").hide();
                displayCalculatedPayableAmount();
                //* Remove use_referral parameter for ssl commerze as user does not want to use referral credit*//
                $('.url_for_ssl').each(function () {
                    $(this).val($(this).val().split('&use_referral=1')[0]);
                })
            }
        }

    })

    $("#error_explanation h2, span.discounted-price, span.original-price").each(function () {
        var info = $(this).text();
        info = $.trim(info);
        info = info.to_bddigit();
        $(this).text(info);
    });

    //**Social media hook**//

    $("#google_map").appear(function () {
        var wrapper = $(this);
        $.ajax({
            url:"/social-media-google-map/" + $(this).attr('deal_id'),
            beforeSend:function () {
                wrapper.html("Loading...")
            },
            success:function (response) {
                wrapper.html(response);
                //load_social_media();
            }
        });
    });

    $(".social-media-likes-featured-deal").appear(function () {
        var wrapper = $(this);
        $.ajax({
            url:"/social-media-likes-featured-deal/" + $(this).attr('deal_id') + "?deal_url=" + $(this).attr('deal_url'),
            beforeSend:function () {
                wrapper.html("Loading...")
            },
            success:function (response) {
                wrapper.html(response);
                load_social_media();
            }
        });
    });

    $("#fb_like_box").appear(function () {
        var wrapper = $(this);
        $.ajax({
            url:"/social-media-facebook-like-box",
            beforeSend:function () {
                wrapper.html("Loading...");
            },
            success:function (response) {
                wrapper.html(response);
                load_fb_social_media()
            }
        });
    });

    function load_social_media() {
        load_fb_social_media();

        if ($(".twitter-share-button").length > 0) {
            if (typeof (twttr) != 'undefined') {
                twttr.widgets.load();
            } else {
                $.getScript('http://platform.twitter.com/widgets.js');
            }
        }

        var gbuttons = $(".g-plusone");
        if (gbuttons.length > 0) {
            if (typeof (gapi) != 'undefined') {
                gbuttons.each(function () {
                    gapi.plusone.render($(this).get(0));
                });
            } else {
                $.getScript('https://apis.google.com/js/plusone.js');
            }
        }

    }

    function load_fb_social_media() {
        if (typeof (FB) != 'undefined') {
            FB.init({ status:true, cookie:true, xfbml:true });
        } else {
            $.getScript("https://connect.facebook.net/en_US/all.js#appId=126965780708584&amp;xfbml=1", function () {
                FB.init({ status:true, cookie:true, xfbml:true });
            });
        }
    }

    //**Social media hook**//

    //*Bind click event on product image with product details page redirection *//
    $(".product_image_hover img.product_medium, .product_image_hover img.product_medium_missing").click(function () {

        window.location = $(this).prev().find("a.multiple-product").attr('href');

    });

    //* Submit poll form by clicking on the radio button *//
    $("div#poll input[type=radio]").click(function () {
        $("form#poll_form").submit();
    });
    $("td.active-poll-option").click(function () {
        $(this).prev().find('input').attr('checked', 'checked');
        $("form#poll_form").submit();
    })
    $("td.active-poll-option").css({cursor:'pointer'});

    //* Loads corporate discounts via AJAX on it's wrapper appearing event for both products and deals *//
    $("div.corporate-extra-discount").appear(function () {
        var corp_dis = $(this);

        if (corp_dis.attr('item_type') == 'deal') {
            $.ajax({
                url:"/deal-corporate-discount/" + $(this).attr('item_id') + "/deal?max_length=" + corp_dis.attr('max_length') ,
                success:function (response) {
                    corp_dis.html(response);
                }
            })
        }
        else if (corp_dis.attr('item_type') == 'product') {
            $.ajax({
                url:"/product-corporate-discount/" + $(this).attr('item_id') + "/product?max_length=" + corp_dis.attr('max_length'),
                success:function (response) {
                    corp_dis.html(response);
                }
            });
        }

    });

    $(".shipping_info").hide();
    $("#need_shipping").click(function(){
        if($(this).prop('checked')) {
           $(".shipping_info").show();
        }
        else {
            $(".shipping_info").hide();
        }
    });

    $("#pickup_from_akhoni").click(function(){
        if($(this).prop('checked')) {
            $(".shipping_info").hide();
        }
        else {
           // $(".shipping_info").hide();
        }
    });

    $('#order_submit').click(function(){
       if($("#pickup_from_akhoni").prop('checked')){
           if(confirm("Are you sure to pick up from akhoni office?")){
               return true;
           }
           else{
              $("#need_shipping").attr('checked',true);
              $(".shipping_info").show();
              return false;
           }
       }
       return true;
    });

    $("img.fast_image").appear(function(){
        $(this).attr('src','/images/' + $(this).attr('data_src'));
    })

    $("img.fast_dynamic_image").appear(function(){
        $(this).attr('src', $(this).attr('data_src'));
    })

});