function closeSubscription(container, msg, flag, exp_time, trans_time) {
    $.cookie('is_subscribed', flag, { expires: exp_time });
    $('#response-' + container + '-container').html(msg);
    if (container == 'strip') {
        $("#subscription-strip").fadeOut(trans_time);
        $("#page_footer").css('margin-bottom', '0px');
    } else {
        hideSubscriptionPopup(flag);
    }
    $('#subscription-' + container + '-email').val('');
}

function hideSubscriptionPopup(flag) {
    $.cookie('is_subscribed', flag);
    $.cookie('subscription_popup', 1, { expires: 7 })
    $('#subscription-popup-email').val('');
    $.fancybox.close();
    if (flag != '1')
        showSubscriptionStrip(20000);
}

function showSubscriptionStrip(time) {
    $("#subscription-strip").delay(time).fadeIn();
    $("#page_footer").css('margin-bottom', '50px');
}

function hideSubscriptionStrip() {
    $.cookie('is_subscribed', '0');
    $("#subscription-strip").fadeOut();
    $("#page_footer").css('margin-bottom', '0px');
    $('#subscription-strip-email').val('');
}

function validateEmailAddress(obj, container) {
    var filter = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/;
    var filter_phone = /^\+?([0-9]|-)+$/;
    var email = $('#subscription-' + container + '-email').val();

    if (!filter.test(email) && (!filter_phone.test(email)) ) {
        $('#response-' + container + '-container').html(email);
        return false;
    }
    else {
        obj.closest("form").submit();
        return false;
    }
}

function renderMessage(msg, container) {
    if (msg == 'Subscribed Successfully.') {
        closeSubscription(container, msg, 1, 30, 6000);
    } else if (msg == "Already Subscribed") {
        closeSubscription(container, msg, 1, 30, 6000);
    } else if (msg == "Invalid email or mobile number") {
        $('#response-' + container + '-container').html('Please enter a valid email or mobile number.');
    } else {
        closeSubscription(container, msg, 0, 0, 1000);
    }
}

function subscriptionHandler(form, container) {
    $.ajax({
        type : form.attr('method'),
        url  : form.attr('action'),
        data : form.serialize(),
        cache: false,
        success : function(msg) {
            renderMessage(msg, container)
        }
    });
}

$(document).ready(function () {
    /*if ($.cookie('subscription_popup') != "1") {
        $("a.subscription-popup").fancybox({
            'width': '100%',
            'height': '100%',
            'autoScale': true,
            'transitionIn': 'fade',
            'transitionOut': 'fade',
            'hideOnOverlayClick':false,
            'href': '/subscriptions/new',
            'onClosed': function(){
               hideSubscriptionPopup(0);
            }
        });
        $(".subscription-popup").click();
    }*/

    if (is_subscribed == '1') {
        $("#subscription-strip").hide();
    } else {         // if ($.cookie('subscription_popup') == '1')
        showSubscriptionStrip(30000);
    }

    //STRIP
    $('#subscription-strip-submit').live('click', function() {
        validateEmailAddress($(this), 'strip');
    });
    $('#subscription-strip-email').live('blur', function() {
        validateEmailAddress($(this), 'strip');
    });
    $('.subscription-skip').live('click', function () {
        hideSubscriptionStrip();
    });
    $('#subscription-strip-form').submit(function() {
        subscriptionHandler($(this), 'strip');
        return false;
    });

    //POPUP
    $('#subscription-popup-submit').live('click', function() {
        validateEmailAddress($(this), 'popup');
    });
    $('#subscription-popup-email').live('blur', function() {
        validateEmailAddress($(this), 'popup');
    });
    $('#subscription-popup .hide-strip').live('click', function () {
        hideSubscriptionPopup(0);
    });
    $('#subscription-popup-form').live('submit', function() {
        subscriptionHandler($(this), 'popup');
        return false;
    });
});