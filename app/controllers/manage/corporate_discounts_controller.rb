class Manage::CorporateDiscountsController < ApplicationController
  before_filter :authenticate_user!
  #load_and_authorize_resource

  layout 'admin'

  def index
    search_and_export_corporate_discounts()
    if params[:commit].present? && params[:commit] == 'Export to Excel'
      send_data @corporate_discounts.to_xls(
                    :columns => [:domain, :discount, :deal_or_product_name],
                    :headers => ['Domain', 'Discount', 'Deal/Product Name']),
                :filename => 'corporate_discounts.xls' and return
    end
    @corporate_discounts = @corporate_discounts.paginate(:page => params[:page], :per_page => ITEM_PER_PAGE)

    respond_to do |format|
      format.html # index.html.erb
      format.xml { render :xml => @corporate_discounts }
    end
  end

  def new

    @corporate_discount = CorporateDiscount.new

  end

  def create
    @corporate_discount = CorporateDiscount.new(params[:corporate_discount])
    @corporate_discount.item_id = params[:corporate_discount][:item_type].to_s == 'deal' ? params[:corporate_discount_][:deal_item] : params[:corporate_discount_][:product_item]
    begin
      @corporate_discount.save!
      redirect_to admin_corporate_discounts_path(), :notice => 'Corporate discount was successfully created.'
    rescue Exception => error
      logger.error "Error:: #{error.message}"
      render :new
    end
  end

  def edit
    @corporate_discount = CorporateDiscount.find(params[:id])

  end

  def update
    @corporate_discount = CorporateDiscount.find(params[:id])
     @corporate_discount.item_id = params[:corporate_discount][:item_type].to_s == 'deal' ? params[:corporate_discount_][:deal_item] : params[:corporate_discount_][:product_item]
    begin
      @corporate_discount.update_attributes(params[:corporate_discount])
      redirect_to admin_corporate_discounts_path(), :notice => 'Corporate discount was successfully updated.'
    rescue Exception => error
      logger.error "Error:: #{error.message}"
      render :edit
    end
  end

  def destroy
    begin
      @corporate_discount = CorporateDiscount.find(params[:id])
      @corporate_discount.destroy
      notice = 'Corporate Discount was successfully deleted.'
    rescue Exception => error
      notice = 'Unable to delete Corporate Discount.'
      logger.error "Error:: #{error.message}"
    end
    redirect_to admin_corporate_discounts_path(), :notice => notice
  end
 private
  def search_and_export_corporate_discounts()
    if params[:search].present?

      session[:corporate_discount_search] = params[:search]
    end
    session[:corporate_discount_page] = params[:page] if params[:page].present?

    @search = CorporateDiscount.search((session[:corporate_discount_search] if session[:corporate_discount_search].present?))
    @corporate_discounts = @search.all
  end

end
