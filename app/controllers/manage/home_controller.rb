class Manage::HomeController < ApplicationController
  before_filter :authenticate_user!

  layout 'admin'
  require 'fileutils'

  def index
  end

  def new_background_image
  end

  def save_background_image
    begin
      background_path = "#{SITE_CONFIG['background_image_path']}"
      file = File.join(background_path, 'background.JPG')
      old_file_rename = File.join(background_path, "#{Time.now.to_i}.JPG")
      if File.file?(file)
        FileUtils.cp file, old_file_rename
      end
      FileUtils.cp params[:background_image].tempfile.path, file
      redirect_to "/manage", :notice => 'Background Successfully Uploaded'
    rescue Exception => error
      logger.error "Error:: #{error.message}"
      render :new_background_image, :notice => "#{error.message}"
    end
  end

end
