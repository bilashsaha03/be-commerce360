class Manage::MerchantsController < ApplicationController
  before_filter :authenticate_user!
  after_filter lambda { |controller| controller.update_bangla_for(@merchant) }, :only => [:create, :update]
  cache_sweeper :merchant_sweeper
  #load_and_authorize_resource

  layout 'admin'

  def index
    @merchants = Merchant.paginate(:page => params[:page], :per_page => ITEM_PER_PAGE)

    respond_to do |format|
      format.html # index.html.erb
      format.xml { render :xml => @merchants }
    end
  end

  def new
    @merchant = Merchant.new
  end

  def create
    @merchant = Merchant.new(params[:merchant])

    begin
      prepare_users()
      @merchant.save!
      redirect_to admin_merchants_path(), :notice => 'Merchant was successfully created.'
    rescue Exception => error
      logger.error "Error:: #{error.message}"
      render :new
    end
  end

  def edit
    @merchant = Merchant.find(params[:id])
  end

  def update
    @merchant = Merchant.find(params[:id])

      prepare_users()
      @merchant.update_attributes(params[:merchant])
      #if @merchant.pictures.size > 1
      #  (@merchant.pictures.size - 1).times do |i|
      #    @merchant.pictures[i].destroy()
      #  end
      #end
      redirect_to admin_merchants_path(), :notice => 'Merchant was successfully updated.'

  end

  def destroy
    begin
      @merchant = Merchant.find(params[:id])
      @merchant.destroy
      notice = 'Merchant was successfully deleted.'
    rescue Exception => error
      notice = 'Unable to delete Merchant.'
      logger.error "Error:: #{error.message}"
    end
    redirect_to admin_merchants_path(), :notice => notice
  end

  private
  def prepare_users
    user_list = []
    params[:user_emails].split(',').each do |email|
      user = User.find_by_email(email)
      user_list << user if user.present?
    end
    @merchant.users = user_list
  end
end
