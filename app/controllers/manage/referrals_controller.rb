class Manage::ReferralsController < ApplicationController
  before_filter :authenticate_user!
  layout 'admin'

  def reset_credits
    begin
      ReferralTracking.all.each do |rt|
        if rt.order.present? && rt.event == 'buy' && rt.order.amount <= 0
          credit = LogReferralCredit.find_by_referred_by_id_and_referred_to_id(rt.referral.user.id, rt.visitor_id)
          credit.destroy if credit.present?
        end
      end
    rescue Exception => error
      logger.error "Error:: #{error.message}"
    end
    render :text => 'done'
  end
end
