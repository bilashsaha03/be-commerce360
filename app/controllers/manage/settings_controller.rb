class Manage::SettingsController < ApplicationController
  def index

  end

  def show

  end

  def clear_cache
    Rails.cache.clear
    redirect_to admin_home_index_path(), :notice => "Cache was deleted successfully"
  end

end
