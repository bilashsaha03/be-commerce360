class Manage::PressesController < ApplicationController
  layout 'admin'
  def index
    @presses = Press.paginate :per_page => ITEM_PER_PAGE, :page => params[:page], :order => 'created_at DESC'
  end

  def new
    @press = Press.new
  end

  def edit
    @press = Press.find(params[:id])
  end

  def create
    @press = Press.new(params[:press])
    if @press.save
      redirect_to admin_presses_url, :notice => 'Press release was saved successfully'
    else
      render :action => :new
    end
  end

  def update
    @press = Press.find(params[:id])
    if @press.update_attributes(params[:press])
      redirect_to admin_presses_url, :notice => 'Press release was updated successfully'
    else
      render :action => :edit
    end
  end

  def destroy
    @press = Press.find(params[:id])
    @press.destroy
    redirect_to admin_presses_url, :notice => 'Press release was deleted successfully'
  end

end
