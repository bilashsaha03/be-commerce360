class Manage::DealsController < ApplicationController
  before_filter :authenticate_user!
  after_filter lambda { |controller| controller.update_bangla_for(@deal) }, :only => [:create, :update]
  cache_sweeper :deal_sweeper
  layout 'admin'

  def index
    prepare_search_criteria()
    @search = Deal.where('parent_id is null').order('id desc').search((session[:deal_search] if session[:deal_search].present?))
    @deals = @search.paginate(:page => session[:deal_page], :per_page => ITEM_PER_PAGE)

    respond_to do |format|
      format.html # index.html.erb
      format.xml { render :xml => @deals }
    end
  end

  def show
    @deal = Deal.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml { render :xml => @deal }
    end
  end

  def new
    if params[:parent_id].present?
      deal = Deal.find(params[:parent_id].to_i)
      @deal = deal.clone
      @deal.is_pay_by_card =  true
      @deal.is_enable_mcoupon =  true
      @deal.parent = deal
    else
      @deal = Deal.new
      @deal.status = Deal::statuses[0].to_s
      initialize_deal_attributes()
    end
  end

  def create
    @deal = Deal.new(params[:deal])
    @deal.copyrighter_id = current_user.id

    begin
      @deal.save!
      redirect_after_save('Deal was successfully created.')
    rescue Exception => error
      logger.error "Error:: #{error.message}"
      render :new
    end
  end

  def edit
    @deal = Deal.find(params[:id])
    @deal.parent_id = @deal.parent_id.present? ? @deal.parent_id : params[:parent_id]
  end

  def update
    @deal = Deal.find(params[:id])
    prev_status = @deal.status
    if @deal.update_attributes(params[:deal])
      after_status = @deal.status
      if(current_user.is?(:superadmin) && after_status == 'approved' && prev_status != 'approved')
        MerchantMailer.send_approved_notification(@deal).deliver
      end
      redirect_after_save('Deal was successfully updated.')
    else
      render :edit
    end
  end

  def sort
    @current_deals = Deal.current.parent_deal.order_by_rating()
    @past_deals = Deal.past.parent_deal.order_by_rating()
  end

  def update_order
    Deal.update_all("rating = '9999999'")
    rating = 1;
    params["current_deals"].each do |deal_id|
      Deal.update(deal_id, :rating => rating+1)
      rating += 1
    end

    params["past_deals"].each do |deal_id|
      Deal.update(deal_id, :rating => rating+1)
      rating += 1
    end
  end

  def destroy
    begin
      @deal = Deal.find(params[:id])
      @deal.destroy
      notice = 'Deal was successfully deleted.'
    rescue Exception => error
      notice = 'Unable to delete deal.'
      logger.error "Error:: #{error.message}"
    end
    redirect_to admin_deals_path(), :notice => notice
  end

  def stat_report
    Deal::set_params(params)
    prepare_report_search()
    if params[:commit].present? && params[:commit] == 'Export to Excel'
      send_data @deals.to_xls(
                    :columns => [:title_with_campaign_name, [:merchant => :name],:actual_price,:discounted_price, :order_count_date_wise, :confirmed_count_date_wise, :sold_count_date_wise, :canceled_count_date_wise, :redeemed_count_date_wise],
                    :headers => ['Title', 'Merchant','Original Unit Price' ,'Unit Paid Amount','Orders Count', 'Confirmed Count', 'Sold Count', 'Canceled Count', 'Redeemed Count']),
                :filename => "report_#{@start_date.strftime('%b %d, %Y %H:%I %P')} to #{@end_date.strftime('%b %d, %Y %H:%I %P')}.xls" and return
    end
  end

  private
  def redirect_after_save(message = '')
    if @deal.parent_deal?
      redirect_to admin_deals_path(), :notice => message
    else
      redirect_to edit_admin_deal_path(@deal.parent), :notice => message
    end
  end

  def prepare_report_search
    @start_date = params["start_date(1i)"].present? ? DateTime.civil(params["start_date(1i)"].to_i, params["start_date(2i)"].to_i, params["start_date(3i)"].to_i, params[:"start_date(4i)"].to_i, params["start_date(5i)"].to_i) : Time.now
    @end_date = params["end_date(1i)"].present? ? DateTime.civil(params["end_date(1i)"].to_i, params["end_date(2i)"].to_i, params["end_date(3i)"].to_i, params["end_date(4i)"].to_i, params["end_date(5i)"].to_i) : Time.now
    @deals = Deal.in_date_range(@start_date, @end_date)
  end

  def prepare_search_criteria()
    if params[:search].present?
      session[:deal_search] = params[:search]
      session[:deal_page] = 1
    end
    session[:deal_page] = params[:page] if params[:page].present?
  end

  def initialize_deal_attributes()
    @deal.min_quantity = 1
    @deal.max_quantity = 50
    @deal.max_order_limit = 5
    @deal.max_coupon_quantity = 1
    @deal.location = Location.default
  end

end
