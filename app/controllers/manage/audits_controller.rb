class Manage::AuditsController < ApplicationController
  #before_filter :authenticate_user!, :authorize_admin
  #load_and_authorize_resource
  layout 'admin'

  def index
    prepare_search_criteria()
    @search = Audit.search((session[:audit_search] if session[:audit_search].present?))
    @audits = @search.all

    if !params[:commit].nil? && params[:commit] == 'Export to Excel'
      send_data @audits.to_xls(
                    :columns => [:auditable_type, :action, {:user=> [:name]}, :created_at],
                    :headers => ['Auditable Type', 'Action', 'User Name','Date']),
                :filename => 'audit_histories.xls' and return
    end
    @audits = @audits.paginate(:page => session[:audit_page], :per_page => ITEM_PER_PAGE)

    respond_to do |format|
      format.html # index.html.erb
      format.xml { render :xml => @audits }
    end
  end

  def show
    @audits = Audit.where(:auditable_id => params[:id], :auditable_type=> params[:format])
    render :layout => 'popup'
  end

  private
  def prepare_search_criteria()
    if params[:search].present?
      session[:audit_search] = params[:search]
      session[:audit_page] = 1
    end
    session[:audit_page] = params[:page] if params[:page].present?
  end
end


