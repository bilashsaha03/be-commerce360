class Manage::BusinessesController < ApplicationController
  before_filter :authenticate_user!
  #load_and_authorize_resource

  layout 'admin'

  def index
    @business = Business.paginate(:page => params[:page], :per_page => ITEM_PER_PAGE)

    respond_to do |format|
      format.html # index.html.erb
      format.xml { render :xml => @business }
    end
  end

  def show
    @business = Business.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml { render :xml => @business }
    end
  end
end
