class Manage::PaymentSummariesController < ApplicationController
  before_filter :authenticate_user!
  #load_and_authorize_resource

  layout 'admin'

  def index
    @deal = Deal.find(params[:deal_id] || Deal.parent_deal.last.id)

    if !params[:commit].nil?
      if params[:commit] == 'Export'
        send_data @deal.payment_summaries.to_xls(
                      :columns => [:collector_name, :collection_amount, :due_amount],
                      :headers => ['Collector Name', 'Collection Amount', 'Due Amount']),
                  :filename => "payment_collections_of_deal_#{@deal.id}.xls" and return
      elsif params[:commit] == 'Export All'
        send_data PaymentSummary.order(:deal_id).all.to_xls(
                      :columns => [{:deal => [:title]}, :collector_name, :collection_amount, :due_amount],
                      :headers => ['Deal', 'Collector Name', 'Collection Amount', 'Due Amount']),
                  :filename => "payment_collections.xls" and return
      end
    end

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  def show
    @payment_summary = PaymentSummary.find(params[:id])
    @payment_receives = @payment_summary.payment_receives.order("created_at DESC").paginate(:page => params[:page], :per_page => ITEM_PER_PAGE)
    respond_to do |format|
      format.html # show.html.erb
      format.xml { render :xml => @deal }
    end
  end

  def receive
    begin
      payment_summary = PaymentSummary.find(params[:payment_summary_id])
      due_amount= payment_summary.due_amount - params[:received_amount].to_i
      payment_summary.update_attribute(:due_amount, due_amount)
      payment_receive=PaymentReceive.new
      payment_receive.payment_summary=payment_summary
      payment_receive.received_amount= params[:received_amount]
      payment_receive.receiver_id=current_user.id
      payment_receive.comments = params[:received_comment]
      payment_receive.received_at = params[:received_at]
      payment_receive.save
      result = {:status => 'SUCCESS', :data => render_to_string(partial: 'row_summary', :locals => {:payment_summary => payment_summary})}
    rescue Exception => error
      logger.error "Error:: #{error.message}"
      result = {:status => 'FAILED', :data => "An error occurred"}
    end
    render json: result
  end
end
