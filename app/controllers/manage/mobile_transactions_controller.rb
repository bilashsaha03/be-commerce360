class Manage::MobileTransactionsController < ApplicationController
  before_filter :authenticate_user!
  layout 'admin'

  def index
    @mobile_transactions = MobileTransaction.paginate :per_page => ITEM_PER_PAGE, :page => params[:page], :include => :user
  end

  def show
    @mobile_transaction = MobileTransaction.find(params[:id])
  end

  def new
    @mobile_transaction = MobileTransaction.new
  end

  def edit
    @mobile_transaction = MobileTransaction.find(params[:id])
    @mobile_transaction.mobile_number = @mobile_transaction.mobile_number.split(MobileTransaction::COUNTRY_CODE).last
  end

  def create
    @mobile_transaction = MobileTransaction.new(params[:mobile_transaction])
    @mobile_transaction.user = current_user
    if @mobile_transaction.save
      update_payment(@mobile_transaction)
      redirect_to admin_mobile_transactions_url, :notice => 'Mobile Transaction was successfully created.'
    else
      render :action => :new
    end
  end

  def update
    @mobile_transaction = MobileTransaction.find(params[:id])
    if @mobile_transaction.update_attributes(params[:mobile_transaction])
      update_payment(@mobile_transaction)
      redirect_to admin_mobile_transactions_url, :notice => 'Mobile Transaction was successfully updated.'
    else
      render :action => :edit
    end
  end

  def destroy
    @mobile_transaction = MobileTransaction.find(params[:id])
    @mobile_transaction.destroy
    redirect_to admin_mobile_transactions_url, :notice => 'Mobile Transaction was successfully deleted.'
  end

  private
  def update_payment(mobile_transaction)
    payment = Payment.first(:conditions => ["transaction_id=? AND mobile_number=?", mobile_transaction.transaction_id, mobile_transaction.mobile_number])
    #raise payment.inspect
    if payment.present?
      MobileTransaction::update_payment(payment.order, mobile_transaction, current_user)
      mobile_transaction.update_attribute(:is_used, true)
    end
  end
end
