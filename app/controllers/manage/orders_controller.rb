class Manage::OrdersController < ApplicationController
  before_filter :authenticate_user!
  layout 'admin'

  def index
    ## Orders will be printed according to filter parameters
    if !params[:commit].nil? && params[:commit] == 'Print'
      #@search = Order.joins(:payments).according_sort.search((session[:order_search] if session[:order_search].present?))
      @search = Order.includes(:payments, :user, :deal => [:translations, :merchant], :product => [:translations, :merchant]).select("DISTINCT(payments.order_id), payments.*, orders.*").according_sort.search((session[:order_search] if session[:order_search].present?))
      @orders = @search.all
      #@orders = @orders.uniq
      #render :file => 'app/views/manage/orders/print_user_info.html.erb', :layout => false and return
      render :file => 'app/views/manage/orders/print_user_invoice.html.erb', :layout => false and return
    end
    prepare_search_criteria()
    #@search = Order.joins(:payments).according_sort.search((session[:order_search] if session[:order_search].present?))
    @search = Order.includes(:payments).select("DISTINCT(payments.order_id), payments.*, orders.*").according_sort.search((session[:order_search] if session[:order_search].present?))
    #@orders = @search.all
    #@orders = @orders.uniq

    if !params[:commit].nil? && params[:commit] == 'Export to Excel'
      @orders = @search.all
      send_data @orders.to_xls(
                    :columns => [:id, :security_code_booking, :booking_at, :quantity, {:deal => [:discounted_price]}, :actual_price, :cc_discount, :amount, :payment_type, :notified_at, :delivery_date, :payment_collector_name, :cancelled?,{:user => [:firstname]},{:user => [:lastname]}, {:user => [:email]},{:user => [:phone]},:payable_amount,:shipping_cost,:item_name,:merchant_name],
                    :headers => ['Order No.', 'Booking Code', 'Date', 'Quantity', 'Unit Price', 'Original Value', 'Card Discount', 'Paid Amount', 'Payment Type', 'Payment Date', 'Delivery Date', 'Collected By', 'Cancelled?','User Firstname','User Lastemane','User email','User Phone','Payable Amount','Shipping Cost','Item Name','Merchant Name']),
                :filename => 'report.xls' and return
    end


    @orders = @search.paginate(:page => session[:order_page], :per_page => ITEM_PER_PAGE)
    respond_to do |format|
      format.html # index.html.erb
      format.xml { render :xml => @orders }
    end
  end

  def show
    @order = Order.find(params[:id])
  end

  def confirm
    @order = Order.find(params[:order_id])
    begin
      @order.build_security_information() if @order.security_code_booking.nil?
      if @order.confirmed?
        notice = "Order##{@order.id} already confirmed"
      else
        @order.update_attributes!(:confirmed_at => Time.now, :confirmed_by => current_user.id)
        notice = "Order##{@order.id} confirmed successfully."
      end
    rescue Exception => error
      logger.error "order confirmation error:: #{error.message}"
      notice = "Error: #{error.message}"
    end
    redirect_to :back, :notice => notice
  end

  def paid
    @order = Order.find(params[:order_id])
    message = "Order##{@order.id} is already paid."

    if @order.due_amount > 0
      if !@order.confirmed?
        @order.update_attributes(:confirmed_at => Time.now, :confirmed_by => current_user.id, :is_waiting => false)
      end

      payment = @order.latest_payment
      payment.update_attributes(
          :collector_id => current_user.id,
          :method => 'CASH',
          :paid_at => Time.now,
          :amount => @order.payable_amount,
          :status => 'VALID')
      message = 'Payment information saved successfully'
    end
    redirect_to :back, :notice => message
  end

  def cancel
    @order = Order.find(params[:order_id])
    message = "Unable to cancel the order##{@order.id}."

    if !@order.redeemed?
      begin
        Order.transaction do
          @order.payments << Payment.new(
              :collector_id => current_user.id,
              :method => 'REFUND',
              :paid_at => Time.now,
              :amount => -@order.paid_amount,
              :status => 'VALID')
          @order.canceled_at = Time.now
          @order.canceled_by = current_user.id
          @order.save!
        end
        message = "Order##{@order.id} canceled successfully."
      rescue Exception => error
        logger.error "Error:: #{error.message}"
      end
    end

    redirect_to :back, :notice => message
  end

  def comment
    begin
      order = Order.find(params[:order_id])
      delivery_date = params[:delivery_date_year].to_s
      delivery_date += '-'+ params[:delivery_date_month].to_s
      delivery_date += '-'+ params[:delivery_date_day].to_s
      if order.shipping_detail.present?
        order.shipping_detail.update_attributes({:address => params[:address_value]})
      else
        order.user.update_attributes({:address => params[:address_value]})
      end

      order.comment = params[:comment].present? ? params[:comment] : nil
      order.delivery_date = delivery_date
      if order.save(false) # order is updated with comment as an order may be updated at any time(it may be after it's deal is expired)) by crm/admin
        result = {:status => 'SUCCESS', :data => render_to_string(:partial => 'row_order', :locals => {:order => order})}
      else
        result = {:status => 'FAILED', :data => order.errors.full_messages}
      end
    rescue Exception => error
      logger.error "Error:: #{error.message}"
      result = {:status => 'FAILED', :data => order.errors.full_messages}
    end
    render :json => result
  end

  private
  def prepare_search_criteria()
    if params[:search].present?
      params[:search].delete(:confirmed_by_greater_than_or_equal_to) if params[:search][:confirmed_by_greater_than_or_equal_to].to_i == 0
      params[:search].delete(:is_notified_is_true) if params[:search][:is_notified_is_true].to_i == 0
      params[:search].delete(:canceled_by_greater_than_or_equal_to) if params[:search][:canceled_by_greater_than_or_equal_to].to_i == 0
      params[:search].delete(:canceled_by_less_than) if params[:search][:canceled_by_less_than].to_i == 0
      params[:search].delete(:deal_id_greater_than_or_equal_to) if params[:search][:deal_id_greater_than_or_equal_to].to_i == 0
      params[:search].delete(:deal_id_less_than) if params[:search][:deal_id_less_than].to_i == 0
      params[:search].delete(:product_id_greater_than_or_equal_to) if params[:search][:product_id_greater_than_or_equal_to].to_i == 0
      params[:search].delete(:product_id_less_than) if params[:search][:product_id_less_than].to_i == 0

      if params[:select_delivery_date].nil?
        params[:search].delete(:delivery_date_greater_than_or_equal_to)
        params[:search].delete(:delivery_date_less_than_or_equal_to)
      end

      session[:order_search] = params[:search]
      session[:order_page] = 1
    end

    session[:order_page] = params[:page] if params[:page].present?
  end
end
