class Manage::CouponsController < ApplicationController
  before_filter :authenticate_user!
  layout 'admin'

  def index
    prepare_search_criteria()
    @search = Coupon.search((session[:coupon_search] if session[:coupon_search].present?))

    if !params[:commit].nil? && params[:commit] == 'Export to Excel'
      @coupons = @search.all
      send_data @coupons.to_xls(
                    :columns => [:order_id, :security_code_coupon, :created_at, :quantity, {:order => {:deal => [:discounted_price]}}, :actual_price, :amount, {:order => [:payment_type]}, :redeemed?],
                    :headers => ['Order No.', 'Coupon Code', 'Date', 'Quantity', 'Unit Price', 'Original Value', 'Paid Amount', 'Payment Method', 'Redeemed?']),
                :filename => 'report.xls' and return
    end

    @coupons = @search.all.paginate(:page => session[:coupon_page], :per_page => ITEM_PER_PAGE)

    respond_to do |format|
      format.html # index.html.erb
      format.xml { render :xml => @coupons }
    end
  end

  def resend_sms
    coupon = Coupon.find(params[:coupon_id]) if params[:coupon_id].present?
    phone_number = coupon.order.gift.present? ? coupon.order.gift.phone : coupon.order.user.phone
    response = SmsNotifier.send_coupon_sms(coupon,phone_number)
    message = 'SMS Sent Successfully' if response
    redirect_to :back, :notice => message
  end


  def destroy
    coupon = Coupon.find(params[:id])
    @order = coupon.order
    @notice = 'Unable to delete the coupon.'

    if @order.coupons.sum(:quantity) > coupon.quantity
      Coupon.transaction do
        coupon.destroy
        @order.update_attribute(:quantity, @order.coupons.sum(:quantity))
        @notice = 'Coupon deleted successfully.'
      end
    end

    respond_to do |format|
      format.html
      format.js
    end
  end

  private
  def prepare_search_criteria()
    if params[:search].present?
      params[:search].delete(:redeemed_by_user_id_greater_than_or_equal_to) if params[:search][:redeemed_by_user_id_greater_than_or_equal_to].to_i == 0
      params[:search].delete(:redeemed_by_user_id_is_null) if params[:search][:redeemed_by_user_id_is_null].to_i == 0
      params[:search].delete(:order_confirmed_by_greater_than_or_equal_to) if params[:search][:order_confirmed_by_greater_than_or_equal_to].to_i == 0
      params[:search].delete(:order_is_notified_is_true) if params[:search][:order_is_notified_is_true].to_i == 0

      session[:coupon_search] = params[:search]
      session[:coupon_page] = 1
    end

    session[:coupon_page] = params[:page] if params[:page].present?
  end
end
