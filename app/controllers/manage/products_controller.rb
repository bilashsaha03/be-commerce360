class Manage::ProductsController < ApplicationController
  before_filter :authenticate_user!
  after_filter lambda { |controller| controller.update_bangla_for(@product) }, :only => [:create, :update]
  cache_sweeper :product_sweeper
  #load_and_authorize_resource

  layout 'admin'

  def index
    prepare_search_criteria
    @search = Product.not_deal_specific.order("id DESC").search((session[:product_search] if session[:product_search].present?))
    @products = @search.paginate(:page => params[:page], :per_page => ITEM_PER_PAGE)
    respond_to do |format|
      format.html # index.html.erb
      format.xml { render :xml => @products }
    end
  end

  def new
    @product = Product.new
    @product.status='pending'
    @product.quantity=0
    if params[:deal_id].present?
      prepare_product()
    elsif params[:merchant_id].present?
      @product.merchant_id = params[:merchant_id]
    end
  end

  def create
    @product = Product.new(params[:product])

    begin
      @product.save!
      @product.update_attribute(:quantity, @product.product_sizes.sum(:quantity).to_i) if @product.product_sizes.present?
      redirect_after_save()
    rescue Exception => error
      logger.error "Error:: #{error.message}"
      render :new
    end
  end

  def edit
    @product = Product.find(params[:id])
  end

  def update
    @product = Product.find(params[:id])
    prev_status = @product.status
    begin
      @product.update_attributes!(params[:product])
      @product.update_attribute(:quantity, @product.product_sizes.sum(:quantity).to_i) if @product.product_sizes.present?
      after_status = @product.status
      if(current_user.is?(:superadmin) && after_status == 'approved' && prev_status != 'approved')
        MerchantMailer.send_approved_notification(@product).deliver
      end
      redirect_after_save('Product information updated successfully')
    rescue Exception => error
      logger.error "Error:: #{error.message}"
      render :edit
    end
  end

  def destroy
    begin
      @product = Product.find(params[:id])
      @product.destroy
      notice = 'Product deleted successfully.'
    rescue Exception => error
      notice = "Unable to delete #{@product.name}"
      logger.error "Error:: #{error.message}"
    end
    redirect_to admin_deal_path(@product.deal), :notice => notice
  end

  def sizes
    product_id = params[:id].to_i
    @product = Product.find(params[:id].to_i) if product_id > 0
    category = Category.find(params[:category_id])
    @sizes = category.present? ? category.sizes : Size.all

    render :layout => false
  end


  private
  def redirect_after_save(notice = 'Product information saved successfully.')
    if @product.deal.present?
      redirect_to edit_admin_deal_path(@product.deal), :notice => notice
    elsif @product.merchant.present?
      redirect_to admin_products_path(), :notice => notice
    end
  end

  def prepare_product()
    deal = Deal.find(params[:deal_id])
    @product.deal = deal
    @product.merchant_id = deal.merchant_id
    @product.location = deal.location
    @product.start_date = deal.start_date
    @product.end_date = deal.end_date
    @product.valid_from = deal.valid_from
    @product.valid_to = deal.valid_to
    @product.status = deal.status
    @product.wait_count = deal.wait_count
    @product.wait_message = deal.wait_message
  end

  def prepare_search_criteria()
    if params[:search].present?
      session[:product_search] = params[:search]
      session[:product_page] = 1
    end
    session[:product_page] = params[:page] if params[:page].present?
  end

end