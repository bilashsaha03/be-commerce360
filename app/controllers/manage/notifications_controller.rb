class Manage::NotificationsController < ApplicationController
  before_filter :authenticate_user!
  cache_sweeper :notification_sweeper
  layout 'admin'

  def index
    @notifications = Notification.order("id desc").paginate(:page => params[:page], :per_page => ITEM_PER_PAGE)
    respond_to do |format|
      format.html # index.html.erb
      format.xml { render :xml => @deals }
    end
  end

  def new
    @notification = Notification.new()
  end

  def create
    @notification = Notification.new(params[:notification])
    begin
      @notification.sent_by = current_user.id
      @notification.save!
      unzip_predefined_html()
      redirect_to admin_notifications_path(), :notice => 'Notification Saved successfully.'
    rescue Exception => error
      logger.error "Error:: #{error.message}"
      render :new
    end
  end

  def edit
    @notification = Notification.find(params[:id])
  end

  def update
    @notification = Notification.find(params[:id])
    begin
      @notification.attributes = params[:notification]
      @notification.save!
      unzip_predefined_html()
      redirect_to admin_notifications_path(), :notice => 'Notification updated successfully.'
    rescue Exception => error
      logger.error "Error:: #{error.message}"
      render :edit
    end
  end

  def show
    @notification = Notification.find(params[:id])
  end

  def preview
    @notification = Notification.find(params[:id])
    @deal = @notification.deal
    @user = current_user

    case @notification.template
      when 'newsletter'
        @deals = Deal.live.parent_deal.for(@deal.location.url).not_in([@deal.id]).order("created_at desc")
        @referral = Referral.create_referral(current_user)
        template_name = "notification_mailer/#{@notification.template}_email"
        layout = 'newsletter'
      when 'ceo_notification'
        template_name = "notification_mailer/ceo_email"
        layout = 'system_email'
      when 'pre_designed_html'
        @referral = Referral.create_referral(current_user)
        template_name = "../../#{@notification.designed_html.url}"
        layout = false
      when 'plain'
        template_name = "notification_mailer/routine_email"
        layout = 'system_email'
    end
    render :layout => layout, :template => template_name
  end

  def schedule
    notification = Notification.find(params[:id])
    notice = 'You must have to assign users before schedule the notification.'
    if notification.notifications_users.any?
      notification.update_attribute(:status, 'Scheduled')
      notice = 'Notification scheduled successfully.'
    end
    redirect_to admin_notifications_path(), :notice => notice
  end

  def assign_users
    search_users()
    @notification = Notification.find(params[:id])

    if params[:commit].present?
      if params[:commit] == 'Export to Excel'
        send_data @users.to_xls(
                      :columns => [:id, :name, :gender, :email, :phone, :address, :is_subscribed],
                      :headers => ['ID', 'Name', 'Gender', 'Email', 'Phone', 'Address', 'Is Subscribed']),
                  :filename => 'users.xls' and return
      elsif params[:commit] == 'Assign'
        begin
          @notification.users = @valid_users
          @notification.sent_by = current_user.id
          @notification.save!
          message = 'Users assigned successfully.'
        rescue Exception => error
          logger.error "Error:: #{error.message}"
          message = "Error to assign users - #{error.message}"
        end
        redirect_to assign_users_admin_notification_path(@notification), :notice => message
      end
    end
  end

  def unsubscribe
    require "#{Rails.root}/lib/Unsubscriber"
    conf = SITE_CONFIG["unsubscribe"]
    if params.present?
      conf['username'] = params['u']
      conf['password'] = params['p']
      folder = params['f'].present? ? params['f'] : 'Delivery Status'
      Unsubscriber.new(conf).unsubscribe(folder)
    end
  end

  def logs
    search_notification_log()
    if !params[:commit].nil? && params[:commit] == 'Export to Excel'
      send_data @notification_logs.to_xls(
                    :columns => [:receiver_email, :notification_email, :subject, :ip_address, :sent_at, :bounce_type],
                    :headers => ['Email', 'Source', 'Subject', 'IP', 'Sent At', 'Notes']),
                :filename => 'notification_logs.xls' and return
    end
    @notification_logs = @notification_logs.paginate(:page => session[:notification_log_page], :per_page => ITEM_PER_PAGE)

    respond_to do |format|
      format.html # index.html.erb
      format.xml { render :xml => @notification_logs }
    end
  end

  def send_now
    @notification = Notification.find(params[:id])
    @notification.update_attribute(:status, 'Scheduled')
    @notification.send_now()
    redirect_to admin_notification_path(@notification), :notice => 'Notification sent successfully.'
  end

  def test
    @notification = Notification.find(params[:id])
    MailNotifier.test_notification(@notification, current_user)
    redirect_to admin_notifications_path(), :notice => 'Test notification sent to your email address.'
  end

  private
  def search_users()
    if params[:search].present?
      params[:search].delete(:orders_is_notified_is_true) if params[:search][:orders_is_notified_is_true].to_i == 0
      params[:search].delete(:orders_confirmed_at_is_not_null) if params[:search][:orders_confirmed_at_is_not_null].to_i == 0
      params[:search].delete(:orders_coupons_redeemed_by_user_id_is_null) if params[:search][:orders_coupons_redeemed_by_user_id_is_null].to_i == 0
    end

    @search = User.search((params[:search] if params[:search].present?))
    @valid_users =  @search.where("is_subscribed = 1 and is_verified = 1 and bounce_count = 0 and complain_count = 0")
    @users = @search.all
    @stats = {
        :total_matched => @search.count(),
        :unsubscribe_count => @search.where(:is_subscribed => false).count(),
        :invalid_count => @search.where(:is_verified => false).count(),
        :bounce_count => @search.where("bounce_count > 0 or complain_count > 0 ").count(),
        :valid => @valid_users.count()
    }
  end

  def search_notification_log()
    if params[:search].present?
      session[:notification_log_search] = params[:search]
      session[:notification_log_page] = 1
    end

    session[:notification_log_page] = params[:page] if params[:page].present?
    @search = LogEmailNotification.search((session[:notification_log_search] if session[:notification_log_search].present?))
    @notification_logs = @search.all
  end

  def unzip_predefined_html()
    if @notification.designed_html_file_name.present?
      Zipper::decompress(@notification.designed_html_file_name, @notification.designed_html.path)
    end
  end
end

