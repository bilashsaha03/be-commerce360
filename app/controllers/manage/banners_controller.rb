class Manage::BannersController < ApplicationController
  layout 'admin'
  cache_sweeper :banner_sweeper
  def index
    @banners = Banner.order("rank ASC").paginate :per_page => ITEM_PER_PAGE, :page => params[:page], :order => 'created_at DESC'
  end

  def new
    @banner = Banner.new
  end

  def edit
    @banner = Banner.find(params[:id])
  end

  def create
    @banner = Banner.new(params[:banner])
    if @banner.save
      redirect_to admin_banners_url, :notice => 'Banner was saved successfully'
    else
      render :action => :new
    end
  end

  def update
    @banner = Banner.find(params[:id])
    if @banner.update_attributes(params[:banner])
      redirect_to admin_banners_url, :notice => 'Banner was updated successfully'
    else
      render :action => :edit
    end
  end

  def destroy
    @banner = Banner.find(params[:id])
    @banner.destroy
    redirect_to admin_banners_url, :notice => 'Banner was deleted successfully'
  end

end
