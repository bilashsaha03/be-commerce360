class Manage::UsersController < ApplicationController
  before_filter :authenticate_user!
  layout 'admin'

  def index
    search_and_export_users()
    if params[:commit].present? && params[:commit] == 'Export to Excel'
      send_data @users.to_xls(
                    :columns => [:id, :name, :gender, :email, :phone, :address, :is_subscribed],
                    :headers => ['ID', 'Name', 'Gender', 'Email', 'Phone', 'Address', 'Is Subscribed']),
                :filename => 'users.xls' and return
    end
    @users = @users.paginate(:page => session[:user_page], :per_page => ITEM_PER_PAGE)

    respond_to do |format|
      format.html # index.html.erb
      format.xml { render :xml => @users }
    end
  end

  def edit
    @user = User.find(params[:id])
    @user.phone = @user.phone_without_country_code
  end

  def update
    @user = User.find(params[:id])
      if params[:user][:is_active].to_i == 0
        params[:user][:suspended_at] = Time.now
      end
      @user.roles = params[:user][:roles]
      #params[:user][:phone] = MobileTransaction::COUNTRY_CODE + params[:user][:phone]
      if @user.update_attributes(params[:user])
        @user.referral_credit = params[:user][:referral_credit].to_i
        @user.save
        redirect_to admin_users_path(), :notice=>'User is Successfully Updated'
      else
        render :action => :edit
      end
  end

  def show
    @user = User.find(params[:id])
  end

  def suspend
    @user = User.find(params[:id])
    @user.update_attributes(:suspended_at => Time.now, :is_active => false)
    redirect_to admin_users_path(), :notice=>"#{@user.name} has been suspended."
  end

  def activate
    @user = User.find(params[:id])
    @user.update_attribute(:is_active, true)
    redirect_to admin_users_path(), :notice=>"#{@user.name} has been activated."
  end

  def verify
    update_verified_information() if params[:verify].present?
    search_and_export_users()
    if params[:commit].present? && params[:commit] == 'Export to Excel'
      send_data @users.to_xls(
                    :columns => [:id, :name, :gender, :email, :phone, :address, :is_subscribed],
                    :headers => ['ID', 'Name', 'Gender', 'Email', 'Phone', 'Address', 'Is Subscribed']),
                :filename => 'users.xls' and return
    end
    @users = @users.paginate(:page => session[:user_page], :per_page => ITEM_PER_PAGE)

    respond_to do |format|
      format.html # index.html.erb
      format.xml { render :xml => @users }
    end
  end

  def login_as

  end

  def login
    email = params[:user][:email]
    @user = User.where("email=? AND (roles_mask=32 OR roles_mask=NULL OR roles_mask=0)", email).first
    if @user.present?
      sign_in(:user, @user)
      flash[:notice] = "You are logged in with #{email}"
      redirect_to root_url and return
    else
      flash[:notice] = "#{email} was not found"
      render "login_as"
    end
  end

  private
  def update_verified_information()
    male_ids = []
    female_ids = []
    subscribe_ids = []
    unsubscribe_ids = []
    verified_ids = []
    unverified_ids = []

    params[:gender].each do |g|
      if g[1] == 'M'
        male_ids << g[0]
      elsif g[1] == 'F'
        female_ids << g[0]
      end
    end
    params[:is_subscribed].each do |s|
      if s[1].to_i == 1
        subscribe_ids << s[0]
      else
        unsubscribe_ids << s[0]
      end
    end
    params[:is_verified].each do |v|
      if v[1].to_i == 1
        verified_ids << v[0]
      else
        unverified_ids << v[0]
      end
    end

    User.where("id in (#{male_ids.join(',')})").update_all(:gender => 'M') if male_ids.size > 0
    User.where("id in (#{female_ids.join(',')})").update_all(:gender => 'F') if female_ids.size > 0
    User.where("id in (#{subscribe_ids.join(',')})").update_all(:is_subscribed => true) if subscribe_ids.size > 0
    User.where("id in (#{unsubscribe_ids.join(',')})").update_all(:is_subscribed => false) if unsubscribe_ids.size > 0
    User.where("id in (#{verified_ids.join(',')})").update_all(:is_verified => true) if verified_ids.size > 0
    User.where("id in (#{unverified_ids.join(',')})").update_all(:is_verified => false) if unverified_ids.size > 0
  end

  def search_and_export_users()
    if params[:search].present?
      params[:search].delete(:orders_is_notified_is_true) if params[:search][:orders_is_notified_is_true].to_i == 0
      params[:search].delete(:orders_confirmed_at_is_not_null) if params[:search][:orders_confirmed_at_is_not_null].to_i == 0
      params[:search].delete(:orders_coupons_redeemed_by_user_id_is_null) if params[:search][:orders_coupons_redeemed_by_user_id_is_null].to_i == 0
      session[:user_search] = params[:search]
    end
    session[:user_page] = params[:page] if params[:page].present?

    @search = User.search((session[:user_search] if session[:user_search].present?))
    @users = @search.all
    @users = @users.uniq
  end
end