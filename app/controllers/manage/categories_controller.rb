class Manage::CategoriesController < ApplicationController
  after_filter lambda { |controller| controller.update_bangla_for(@category) }, :only => [:create, :update]

  layout 'admin'
  def index
    @categories = Category.paginate :per_page => ITEM_PER_PAGE, :page => params[:page]
  end

  def new
    @category = Category.new
  end

  def edit
    @category = Category.find(params[:id])
  end

  def create
    @category = Category.new(params[:category])
    if @category.save
      redirect_to admin_categories_url, :notice => 'category was saved successfully'
    else
      render :action => :new
    end
  end

  def update
    @category = Category.find(params[:id])
    if @category.update_attributes(params[:category])
      redirect_to admin_categories_url, :notice => 'category was updated successfully'
    else
      render :action => :edit
    end
  end

  def destroy
    @category = Category.find(params[:id])
    @category.destroy
    redirect_to admin_categories_url, :notice => 'category was deleted successfully'
  end


end
