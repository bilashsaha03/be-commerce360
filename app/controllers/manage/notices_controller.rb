class Manage::NoticesController < ApplicationController
  before_filter :authenticate_user!
  cache_sweeper :notice_sweeper
  #load_and_authorize_resource

  layout 'admin'

  def index
    @notices = Notice.viewable
    @notices = @notices.all.paginate(:page => params[:page], :per_page => ITEM_PER_PAGE)
    respond_to do |format|
      format.html # index.html.erb
      format.xml { render :xml => @notices }
    end
  end

  def new
    @notice= Notice.new
  end

  def create
    @notice = Notice.new(params[:notice])
    begin
      @notice.created_by = current_user.id
      @notice.save!
      redirect_to admin_notices_path(), :notice => 'Notice saved successfully.'
    rescue Exception => error
      logger.error "Error:: #{error.message}"
      render :new
    end
  end

  def edit
    @notice = Notice.find(params[:id])
  end

  def update
    @notice = Notice.find(params[:id])
    @notice.updated_by= current_user.id
    if @notice.update_attributes(params[:notice])
      redirect_to admin_notices_path(), :notice => 'Notice was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    begin
      @notice = Notice.find(params[:id])
      @notice.update_attribute(:is_deleted, true)
      notice = 'Notice was successfully deleted.'
    rescue Exception => error
      notice = 'Unable to delete Notice.'
      logger.error "Error:: #{error.message}"
    end
    redirect_to admin_notices_path(), :notice => notice
  end


  def show
    @notice = Notice.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml { render :xml => @notice }
    end
  end

end


