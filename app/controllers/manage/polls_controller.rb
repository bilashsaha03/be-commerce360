class Manage::PollsController < ApplicationController

  layout 'admin'
  def index
    @polls = Poll.paginate :per_page => ITEM_PER_PAGE, :page => params[:page], :order => 'created_at DESC'
  end

  def new
    @poll = Poll.new
  end

  def edit
    @poll = Poll.find(params[:id])
  end

  def create
    @poll = Poll.new(params[:poll])
    if @poll.save
      redirect_to admin_polls_url, :notice => 'Poll was saved successfully'
    else
      render :action => :new
    end
  end

  def update
    @poll = Poll.find(params[:id])
    if @poll.update_attributes(params[:poll])
      redirect_to admin_polls_url, :notice => 'Poll was updated successfully'
    else
      render :action => :edit
    end
  end

  def destroy
    @poll = Poll.find(params[:id])
    @poll.destroy
    redirect_to admin_polls_url, :notice => 'Poll was deleted successfully'
  end


end
