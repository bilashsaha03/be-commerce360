class Manage::PaymentsController < ApplicationController
  before_filter :authenticate_user!
  layout 'admin'

  def index
    prepare_search_criteria()
    @search = Payment.latest_first().search((session[:payment_search] if session[:payment_search].present?))

    if !params[:commit].nil? && params[:commit] == 'Export to Excel'
      @payments = @search.all
      send_data @payments.to_xls(
                    :columns => [:id, :order_id, :collector_name, :amount_without_referral, {:order => [:referral_credit]}, :paid_at, :method, :status],
                    :headers => ['Payment ID', 'Order ID', 'Collector', 'Amount','Referral Credit', 'Date', 'Method', 'Status']),
                :filename => 'payments.xls' and return
    end

    @payments = @search.all
    @payments = @payments.paginate(:page => session[:payment_page], :per_page => ITEM_PER_PAGE)

    respond_to do |format|
      format.html # index.html.erb
      format.xml { render :xml => @payments }
    end
  end

  private
  def prepare_search_criteria()
    if params[:search].present?
      session[:payment_search] = params[:search]
      session[:payment_page] = 1
    end

    session[:payment_page] = params[:page] if params[:page].present?
  end
end
