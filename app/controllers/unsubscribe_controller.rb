class UnsubscribeController < ApplicationController
  def new
    page_not_found(User.new) if params[:id].nil? || params[:email].nil?
    @user = User.find_by_id_and_email(params[:id], params[:email])
  end

  def create
    begin
      @user = User.find_by_id_and_email(params[:id], params[:email])
      @user.update_attribute(:is_subscribed, 0)
      redirect_to '/', :notice => 'Unsubscribed successfully'
    rescue Exception => error
      logger.error "Error:: #{error.message}"
      #page_not_found(User.new)
    end
  end
end
