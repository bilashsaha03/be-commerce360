class BusinessesController < ApplicationController
  def new
    @business = Business.new
  end

  def create
    @business = Business.new(params[:business])
    user_ip = request.remote_ip
    businesses = Business.from(user_ip)
    if (!businesses.nil? && businesses.size>=SITE_CONFIG['get_featured']['request_count'])
      redirect_to '/getfeatured', :alert=>"You have exceeded the limit for 'Feature Your Business' request"
      return ''
    else
      begin
        @business.ip_address = user_ip
        @business.save!
        redirect_to '/getfeatured/thanks'
      rescue Exception => error
        logger.error "Error:: #{error.message}"
        render :new
      end
    end
  end

  def thanks
    page_not_found(Business.new) if request.referrer.nil? || request.referrer == '/'
  end
end
