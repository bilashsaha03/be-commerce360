class DealsController < ApplicationController
  include ActionView::Helpers::TextHelper

  def index
    #populate_deals()
    #redirect_to past_location_deals_path(params[:location_id]) and return if @deals.nil? || @deals.size == 0
    #redirect_to location_deal_path(params[:location_id], @deals.first) and return if @deals.size == 1

    respond_to do |format|
      format.html # index.html.erb
      format.xml { render :xml => @deals }
    end
  end

  def index_for_facebook
    populate_deals()
    render :layout => 'popup'
  end

  def show
    @deal = Deal.find(params[:id], :include => [:merchant])
    if (@deal.pending? && (!current_user.present? || current_user.member?))
      page_not_found(@deal)
    else
      redirect_to @deal.parent and return if @deal.parent_id.present?
      redirect_to @deal, :status => :moved_permanently and return unless @deal.friendly_id_status.best?
      populate_side_deals([@deal])

      respond_to do |format|
        format.html # show.html.erb
        format.xml { render :xml => @deal }
      end
    end
  end

  def sub_deals
    @deal = Deal.find(params[:id], :include => [:sub_deals])
    if (@deal.pending? && (!current_user.present? || !current_user.admin?))
      page_not_found(@deal)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.xml { render :xml => @deal.sub_deals }
      end
    end
  end

  def past
    @deals = Deal.past.parent_deal.for(params[:location_id]).order_by_rating.paginate(:page => params[:page], :per_page => ITEM_PER_PAGE)

    respond_to do |format|
      format.html # index.html.erb
      format.xml { render :xml => @deals }
    end
  end

  def expire
    Deal.all.each do |deal|
      expire_page :action => :show, :id => deal.slug.name, :location => params[:location_id]
    end

    redirect_to location_deals_path(session[:location_id])
  end

  def google_map
    @deal = Deal.find(params[:deal_id])
    render :layout => false
  end

  def deal_corporate_discount
    if current_user.nil?
      render :text => '', :layout => false
      return
    end
    domain = current_user.email.split('@').last
    deal = Deal.find(params[:deal_id])
    corporate_discount = current_user.corporate_discount_of(deal)
    if corporate_discount.nil?
      render :text => '', :layout => false
      return
    end
    discount_label = corporate_discount.send("discount_label_#{I18n.locale}")
    if discount_label.present?
      label = discount_label
    else
      label = "#{corporate_discount.discount.to_i} tk extra discount for #{domain.split('.').first} users"
    end
    truncated_label =  params[:max_length].to_i > 0 ? truncate(label, :length => params[:max_length].to_i) : label
    render :text => "<label class='extra_discount_label' title='#{label}'>#{truncated_label}</label>", :layout => false
  end

  private
  def populate_deals()
    if params[:category_id].present? && params[:category_id].to_i > 0
      @deals = Deal.live.parent_deal.in_categories([params[:category_id].to_i]).for(params[:location_id]).order_by_rating()
    else
      #@deals = Deal.live.parent_deal.featured.in_categories([0]).for(params[:location_id]).order_by_rating()
    end
  end

  def populate_side_deals(deals)
    ids = deals.map { |deal| deal.id }
    @left_widget = Hash.new
    @left_widget[:title] = t("MORE_DEALS")
    @left_widget[:deals] = Deal.live.parent_deal.for(params[:location_id]).not_in(ids).order_by_rating
  end
end
