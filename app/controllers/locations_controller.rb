class LocationsController < ApplicationController
  def index
    redirect_to location_deals_path(session[:location_id]) and return
  end
end
