class Merchant::ProductsController < ApplicationController
  before_filter :authenticate_user!
  after_filter lambda { |controller| controller.update_bangla_for(@product) }, :only => [:create, :update]

  layout 'merchant'

  def index
    @products = Array.new
    Product.not_deal_specific.search(params[:search]).each do |product|
      @products << product if product.merchants_product?(current_user)
    end
    @products = @products.paginate(:page => params[:page], :per_page => ITEM_PER_PAGE)

    respond_to do |format|
      format.html # index.html.erb
      format.xml { render :xml => @products }
    end
  end

  def new
    @product = Product.new
    @product.status='pending'
    @product.quantity=0
  end

  def create
    @product = Product.new(params[:product])
    @product.status='pending'
    check_merchant_authorization
    begin
      @product.save!
       @product.update_attribute(:quantity, @product.product_sizes.sum(:quantity).to_i) if @product.product_sizes.present?
      redirect_to merchant_products_path(), :notice => 'Product was successfully created.'
    rescue Exception => error
      logger.error "Error:: #{error.message}"
      render :new
    end
  end

  def edit
    @product = Product.find(params[:id])
    if @product.status == 'approved'
      redirect_to root_url, :notice => 'You have not permission to do that' and return
    end
    check_merchant_authorization
  end

  def update
    @product = Product.find(params[:id])
    check_merchant_authorization
    begin
      @product.update_attributes!(params[:product])
       @product.update_attribute(:quantity, @product.product_sizes.sum(:quantity).to_i) if @product.product_sizes.present?
      redirect_to merchant_products_path(), :notice => 'Product information updated successfully'
    rescue Exception => error
      logger.error "Error:: #{error.message}"
      render :edit
    end
  end

  def sizes
    product_id = params[:id].to_i
    @product = Product.find(params[:id].to_i) if product_id > 0
    category = Category.find(params[:category_id])
    @sizes = category.present? ? category.sizes : Size.all

    render :layout => false
  end

  def send_approval_notification
    product_id = params[:product_id].to_i
    product = Product.find(product_id)
    if !product.merchants_product?(current_user)
       page_not_found(product)
    end

    MerchantMailer::send_approval_notification(product,current_user).deliver!
    redirect_to merchant_products_url, :notice => "An approval email notification has been sent to admin!"
  end

  protected
  def check_merchant_authorization
    merchant_id = @product.merchant.id
    merchant = Merchant.find(merchant_id)
    if !merchant.manager?(current_user)
      redirect_to root_url, :notice => 'You have not permission to do that' and return
    end
  end

end