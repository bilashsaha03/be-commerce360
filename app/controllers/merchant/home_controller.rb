class Merchant::HomeController < ApplicationController
  before_filter :authenticate_user!

  layout 'merchant'

  def index
  end

end
