class Merchant::ReportController < ApplicationController
  before_filter :authenticate_user!, :authorize_merchant
  MESSAGE_UNAUTHORIZED = "Not authorized!"
  layout 'merchant'

  def index
    @merchants = current_user.merchants

    if @merchants.size == 0
       redirect_to root_url
      return
    elsif @merchants.size == 1
      redirect_to merchant_coupons_url(:id => @merchants.first)
      return
    end

    respond_to do |format|
      format.html # index.html.erb
      format.xml { render :xml => @merchants }
    end
  end


  def merchants_orders
      prepare_search_criteria()
      ids = current_user.merchants.collect{|p| p.id}
      deal_ids = Deal.for_merchant(ids).collect{|p| p.id}
      product_ids = Product.for_merchant(ids).collect{|p| p.id}
      @search = Order.joins(:payments).according_sort.specific_orders(deal_ids,product_ids).search((session[:order_search] if session[:order_search].present?))
      @orders = @search.all
      @orders = @orders.uniq
      @orders = @orders.paginate(:page => session[:order_page], :per_page => ITEM_PER_PAGE)
      render :file => '/merchant/report/merchants_orders.html.erb' and return
  end

  private
      def prepare_search_criteria()
        if params[:search].present?
          params[:search].delete(:confirmed_by_greater_than_or_equal_to) if params[:search][:confirmed_by_greater_than_or_equal_to].to_i == 0
          params[:search].delete(:is_notified_is_true) if params[:search][:is_notified_is_true].to_i == 0
          params[:search].delete(:canceled_by_greater_than_or_equal_to) if params[:search][:canceled_by_greater_than_or_equal_to].to_i == 0
          params[:search].delete(:canceled_by_less_than) if params[:search][:canceled_by_less_than].to_i == 0
          params[:search].delete(:deal_id_greater_than_or_equal_to) if params[:search][:deal_id_greater_than_or_equal_to].to_i == 0
          params[:search].delete(:deal_id_less_than) if params[:search][:deal_id_less_than].to_i == 0
          params[:search].delete(:product_id_greater_than_or_equal_to) if params[:search][:product_id_greater_than_or_equal_to].to_i == 0
          params[:search].delete(:product_id_less_than) if params[:search][:product_id_less_than].to_i == 0

          if params[:select_delivery_date].nil?
            params[:search].delete(:delivery_date_greater_than_or_equal_to)
            params[:search].delete(:delivery_date_less_than_or_equal_to)
          end

          session[:order_search] = params[:search]
          session[:order_page] = 1
        end

        session[:order_page] = params[:page] if params[:page].present?
      end


end
