class Merchant::CouponsController < ApplicationController
  before_filter :authenticate_user!, :authorize_merchant
  layout 'merchant'

  def index
    merchant_id = params[:id].to_i
    @merchant = Merchant.find_by_id(merchant_id)
    if @merchant.nil? || !@merchant.users.include?(current_user)
      #raise CanCan::AccessDenied.new(MESSAGE_UNAUTHORIZED)
      redirect_to root_url
      return
    end

    # A merchant will see blank coupon page until he gives a valid coupon code
    session[:coupon_search] = {"order_deal_id_or_order_deal_parent_id_equals"=>"", "order_product_id_equals"=>"", "security_code_coupon_contains"=>"xxxxxxxx"}

    if params[:search].present?
      session[:coupon_search] = params[:search]
      session[:coupon_page] = 1
    end
    session[:coupon_page] = params[:page] if params[:page].present?

    @deals = @merchant.deals.parent_deal
    @products = @merchant.products
    redirect_to :controller => 'merchant/home', :action => :index and return if (@deals.size + @products.size) == 0

    conditions = prepare_conditions
    @search = Coupon.for_all(conditions).confirmed.not_canceled.search((session[:coupon_search] if session[:coupon_search].present?))
    @coupons = @search.all.paginate(:page => session[:coupon_page], :per_page => 100)

    respond_to do |format|
      format.html # index.html.erb
      format.xml { render :xml => @coupons }
    end
  end

  def verify
    result = Hash.new
    coupon = Coupon.find_by_id(params[:coupon_id].to_i)
    verify_code = params[:verification_code].to_i

    if coupon.nil?
      result = {:status => 'FAILED', :data => 'Order not found'}
    elsif !coupon.redeemed_ability?(current_user)
      result = {:status => 'UNAUTHORIZED', :data => "You don't have permission to access the data"}
    elsif coupon.log_failed_redeems.length > Coupon::MAX_REDEEM_ATTEMPT
      result = {:status => 'FAILED', :data => 'The order is locked due to repeated redeem attempts'}
    elsif coupon.security_code_verification.present? && coupon.security_code_verification != verify_code
      if coupon.log_failed_redeems.length == Coupon::MAX_REDEEM_ATTEMPT
        result = {:status => 'SUCCESS', :data => render_to_string(:partial => 'row_order', :locals => {:coupon => coupon})}
      else
        result = {:status => 'FAILED', :data => "Verification code mismatched"}
      end
      LogFailedRedeem.create(:user => current_user, :coupon => coupon)
    else
      coupon.redeemed_at = Time.now
      coupon.redeemed_by_user = current_user
      coupon.save
      result = {:status => 'SUCCESS', :data => render_to_string(:partial => 'row_order', :locals => {:coupon => coupon})}
    end

    render :json => result
  end

  def redeemed
    @merchant = Merchant.find_by_id(params[:id].to_i)
    @deals = @merchant.deals.parent_deal
    @products = @merchant.products
    session[:coupon_search] = nil
    if params[:search].present?
      session[:coupon_search] = params[:search]
      session[:coupon_page] = 1
    end
    session[:coupon_page] = params[:page] if params[:page].present?

    conditions = prepare_conditions
    @search = Coupon.for_all(conditions).redeemed_coupons.search((session[:coupon_search] if session[:coupon_search].present?))
    @coupons = @search.all.paginate(:page => session[:coupon_page], :per_page => 100)
  end


  private

  def prepare_conditions
    ids = {}
    ids[:deal] = @deals.map { |deal| deal.id }
    ids[:product] = @products.map { |product| product.id }
    conditions = []
    conditions << "deals.id in (#{ids[:deal].join(', ')}) or deals.parent_id in (#{ids[:deal].join(', ')}) " if ids[:deal].any?
    conditions << "products.id in (#{ids[:product].join(', ')})" if ids[:product].any?
    return conditions
  end

end
