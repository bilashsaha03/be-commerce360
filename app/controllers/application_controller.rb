class ApplicationController < ActionController::Base
  helper :layout
  before_filter :set_location, :set_locale, :check_for_reference
  protect_from_forgery
  #before_filter :authenticate_for_staging_server, :if => lambda { !Rails.env.production? }
  before_filter :force_logout, :if => lambda { session[:force_logout].blank? }

  before_filter :redirect_to_english, :if => lambda { I18n.locale == :bn && !params[:controller].to_s.match(/manage/).nil? }
  #before_filter :redirect_to_https, :if => lambda { request.protocol == 'http://' && (current_user && params[:controller].to_s.match(/manage/).nil? && current_user.roles_mask != 32 && current_user.roles_mask > 0) }
  before_filter :redirect_to_https, :if => lambda { ( !params[:controller].to_s.match(/merchant/).nil? || !params[:controller].to_s.match(/manage/).nil?) && request.protocol == 'http://' && params[:controller].to_s != "merchants" }
  #before_filter :redirect_to_http, :if => lambda {params[:controller].to_s.match(/manage/).nil? && request.protocol == 'https://'}

  #########################################
  # DECLARE METHOS AS A HELPER METHOD     #
  #########################################
  helper_method :deletable?, :exportable?, :reverse_language, :get_language_switch_url

  enable_authorization do |exception|
    redirect_to root_url, :alert => exception.message
  end

  #def default_url_options(options={})
  #   { :locale => I18n.locale }
  #end

  def set_location
    session[:location_id] = params[:location_id] || Location.default.url
  end

  def page_not_found(object)
    logger.error "Unauthorized access! - Order: #{object.inspect} User: #{current_user.inspect}"
    redirect_to("/PageNotFound") and return
    return
  end

  def authorize_admin
    unless current_user.admin?
      page_not_found(current_user)
    end
  end

  def authorize_merchant
    unless current_user.merchant?
      page_not_found(current_user)
    end
  end

  def authorize_owner(order)
    unless order.user_id == current_user.id
      page_not_found(order)
    end
  end

  def authorize_coupon(order)
    unless ((order.user_id == current_user.id && order.paid?) ||
        (current_user.admin? && order.confirmed?) ||
        (order.gift.present? && params.present? && order.gift.verification_code == params['gcode'].to_s))
      page_not_found(order)
    end
  end

  def validate_active_deal(deal)
    unless deal.active?
      page_not_found(deal)
    end
  end

  def validate_active_product(product)
    unless product.active?
      page_not_found(product)
    end
  end

  def check_for_reference
    if user_signed_in? && session[:referral_tracking_id]
      ReferralTracking.update(session[:referral_tracking_id], {:visitor_id => current_user.id})
      session[:referral_tracking_id] = nil
    elsif params[:ref] && referral = Referral.find_by_code(params[:ref])
      session[:referral_id] = referral.id
      referral_tracking = referral.track(request, 'visit', current_user)
      session[:referral_tracking_id] = referral_tracking.id
    end
  end


  def update_bangla_for(item)
    if item && item.errors && item.errors.full_messages.size == 0
      original_locale = I18n.locale
      I18n.locale = reverse_language
      params_object = 'bangla_' + item.class.to_s.downcase
      item.update_attributes(params[params_object.to_sym])
      I18n.locale = original_locale
      begin
        item.cached_slug = item.send(item.friendly_id_config.method.to_sym)
        item.save
      rescue Exception => error
        logger.error "Error:: #{item.class} was not declared as friendly id"
      end
    end
  end

  def reverse_language
    if I18n.locale == :en
      :bn
    else
      :en
    end
  end

  def get_language_switch_url
    locale = I18n.locale == :en ? 'bn' : 'en'
    query_strings = request.env['QUERY_STRING'].split('&').delete_if { |q| q.match(/locale/) }.join('&')
    query_strings = query_strings.length > 0 ? '&' + query_strings : ''
    language_switch_url = request.env['PATH_INFO'] + '?' + "locale=#{locale}" + query_strings
  end

  def deletable?(action='destroy', controller='')
    return can?(action, controller) && !Rails.env.production?
  end

  def exportable?(item)
    return false if current_user.nil?
    if item == :user
      return current_user.is?(:analyst)
    elsif item == :order
      return current_user.is?(:accountant) || current_user.is?(:analyst)
    elsif item == :coupon
      return current_user.is?(:analyst)
    end
    return false
  end

  private
  def authenticate_for_staging_server
    authenticate_or_request_with_http_basic do |user_name, password|
      password == HTTP_AUTH_USERS[user_name] or login_by_email(user_name, password) == true
    end
  end

  def force_logout
    if current_user && !current_user.active?
      session[:force_logout] = 1
      redirect_to destroy_user_session_path and return
    end
  end

  #
  # Sets I18n.locale based on params first then session and default is i18n's default locale
  #
  def set_locale
    localization = params[:locale].to_s
    if I18n.available_locales.include?(localization.to_sym)
      I18n.locale = localization
      session[:locale] = localization
    elsif session[:locale]
      I18n.locale = session[:locale]
    else
      I18n.locale = I18n.default_locale
    end
  end

  #
  # Redirects to admin panel home page and switch to english if admin user is on bangla mode
  # as we are not giving support for admin user to browse admin panel in bangla mode
  #
  def redirect_to_english
    I18n.locale = :en
    session[:locale] = I18n.locale
    redirect_to admin_home_index_url and return
  end

  def redirect_to_https
    if Rails.env.production?
      redirect_to :protocol => "https://"
    end
    return
  end

  def redirect_to_http
    if Rails.env.production?
      redirect_to :protocol => "http://"
    end
  end

  def login_by_email(user_name, password)
    is_logged_in = false
    if (user_name.match(/.+\b@akhoni.com$\b/i))
      require 'net/imap'
      require 'openssl'
      client = Net::IMAP.new(SITE_CONFIG["unsubscribe"]['host'], SITE_CONFIG["unsubscribe"]['port'], true, nil, false)
      begin
        client.login(user_name, password)
        client.logout
        is_logged_in = true
      rescue Exception => error
        logger.error "Error to login staging server :: #{error.message}"
      end
      client.disconnect
    else
      logger.error "Unknown domain to try login :: #{user_name}"
    end
    return is_logged_in
  end

end