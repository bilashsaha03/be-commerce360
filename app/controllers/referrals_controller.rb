class ReferralsController < ApplicationController
  before_filter :authenticate_user!

  def index
    @deals = Deal.live.parent_deal.featured.for(session[:location_id])
    referral = Referral.create_referral(current_user)
    @referral_link = "#{root_url}?ref=#{referral.code}"
  end

  def history
    @referral_events = LogReferralCredit.for_user(current_user.id)
    @total_purchased = Payment.for(current_user.id).by('REFERRAL-CREDIT').sum(:amount).to_f
    @partial_purchased = Payment.for(current_user.id).includes(:order).sum(:referral_credit)
    @total_purchased = @total_purchased + @partial_purchased.to_f
  end
end
