class ProductsController < ApplicationController
  include ActionView::Helpers::TextHelper
  def index
    respond_to do |format|
      format.html # index.html.erb
      format.xml { render :xml => @products }
    end
  end

  def show
    @product = Product.find(params[:id], :include => [:product_sizes, :pictures, :translations])
    if (@product.pending? && (!current_user.present? || current_user.member?))
      page_not_found(@product)
    else
      respond_to do |format|
        format.html # show.html.erb
        format.xml { render :xml => @product }
      end
    end
  end

  def product_corporate_discount
    if current_user.nil?
      render :text => '', :layout => false
      return
    end
    domain = current_user.email.split('@').last
    product = Product.find(params[:product_id])
    corporate_discount = current_user.corporate_discount_of(product)
    if corporate_discount.nil?
      render :text => '', :layout => false
      return
    end
    discount_label = corporate_discount.send("discount_label_#{I18n.locale}")
    if discount_label.present?
      label = discount_label
    else
      label = "#{corporate_discount.discount.to_i} tk extra discount for #{domain.split('.').first} users"
    end
    truncated_label =  params[:max_length].to_i > 0 ? truncate(label, :length => params[:max_length].to_i) : label
    render :text => "<label class='extra_discount_label' title='#{label}'>#{truncated_label}</label>", :layout => false
  end
end
