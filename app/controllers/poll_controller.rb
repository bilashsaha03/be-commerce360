class PollController < ApplicationController
  before_filter :authenticate_user!


  def vote
    poll_vote = PollOption::create_vote(params, current_user)
    message = poll_vote ? "You have successfully casted your vote." : "You have already casted your vote."
    redirect_to digital_mind_url, :notice => message
  end
end
