class SubscriptionsController < ApplicationController
  layout nil

  def new
  end

  def create
    email = params[:email]
    phone = params[:email]
    is_verified = true
    @success = 'Subscribed Successfully.'

    if (params[:email].match(/\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i).present?)
      phone = ""
      user = User.find_by_email(params[:email])
    elsif (params[:email].match(/^\+?([0-9]|-)+$/).present?)
      email = "#{phone}@subscription.akhoni.com"
      user = User.find_by_phone(params[:email])
      is_verified = false
    else
      @success = 'Invalid email or mobile number'
    end

    begin
      if user.present?
        if user.is_subscribed?
          @success = 'Already Subscribed'
        else
          user.update_attribute(:is_subscribed, true)
        end
      else
        User.create(
            {
                :email => email,
                :phone => phone,
                :is_subscribed => true,
                :is_registered => false,
                :is_verified => is_verified,
                :password => (1..8).map { |i| ('A'..'Z').to_a[rand(26)] }.join,
                :location_id => Location.default.id
            }
        )
      end
    rescue Exception => error
      @success = 'Unable to process subscription, please try again later.'
      logger.error "Error:: #{error.message}"
    end
  end
end
