class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController

  before_filter :set_auth

  def facebook
    # You need to implement the method below in your model
    @user = User.find_for_facebook_oauth(env["omniauth.auth"], current_user)

    if @user.persisted?
      flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => "Facebook"
      sign_in_and_redirect @user, :event => :authentication
    else
      session["devise.facebook_data"] = env["omniauth.auth"]
      redirect_to new_user_registration_url
    end
  end

#  def facebook
#
#    if current_user
#      if current_user.set_token_from_hash(facebook_authorization_hash)
#        flash[:notice] = "Authentication successful"
#      else
#        flash[:alert] = "This Facebook account is already attached to a user account!"
#      end
#      redirect_to edit_user_registration_path + '#tabs-3'
#    else
#
#      authorization = Authorization.find_by_provider_and_uid(@auth['provider'], @auth['uid'])
#
#      if authorization
#        authorization.user.set_token_from_hash(facebook_authorization_hash)
#        flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => @auth['provider']
#        sign_in_and_redirect(:user, authorization.user)
#      else
#        session['devise.omniauth_info'] = facebook_authorization_hash.merge(facebook_user_hash)
#        redirect_to new_user_registration_url
#      end
#    end
#  end
#
#  private
#
  def facebook_authorization_hash
    {
      :provider    => @auth['provider'],
      :uid         => @auth['uid'],
      :nickname    => @auth['user_info']['nickname'],
      :url         => @auth['user_info']['urls']['Facebook'],
      :credentials => @auth['credentials']
    }
  end

  def facebook_user_hash
    {
      :name        => @auth['user_info']['name'],
      :email       => @auth['extra']['user_hash']['email'],
      :gender      => case @auth['extra']['user_hash']['gender']
        when 'male'
          'M'
        when 'female'
          'F'
        else
          'N'
        end
    }
  end

  def set_auth
    @auth = env['omniauth.auth']
  end
end
