class HomeController < ApplicationController
  before_filter :set_blank_layout, :only => [:credit_card_purchase, :cash_purchase, :site_notice, :bkash_instruction, :coupon_on_your_mobile]

  def index
  end

  def digital_mind
     @polls = Poll.live.active
  end

  def about
    about__page_locale = params[:locale].present? ? params[:locale] : I18n.locale
    render 'about_' + about__page_locale.to_s
  end

  def how
    how_akhoni_works_page_locale = params[:locale].present? ? params[:locale] : I18n.locale
    render 'how_' + how_akhoni_works_page_locale.to_s
  end

  def terms
     terms__page_locale = params[:locale].present? ? params[:locale] : I18n.locale
    render 'terms_' + terms__page_locale.to_s
  end

  def privacy
  end

  def page_not_found
  end

  def contact
    contact__page_locale = params[:locale].present? ? params[:locale] : I18n.locale
    render 'contact_' + contact__page_locale.to_s
  end

  def credit_card_purchase
  end

  def cash_purchase
  end

  def site_notice
  end

  def jobs
    contact__page_locale = params[:locale].present? ? params[:locale] : I18n.locale
    render 'jobs_' + contact__page_locale.to_s
  end

  def press
    @presses = Press.latest_first.paginate :per_page => ITEM_PER_PAGE, :page => params[:page]
  end
  def bkash_instruction
  end

  def coupon_on_your_mobile

  end

  private
  def set_blank_layout
    render :layout => false
  end
end
