class RegistrationsController < Devise::RegistrationsController

  def create
    build_resource # Here's where the autofill magic happens

    # if already subscribed, change their old email
    # TODO: Its a quick hack. need to change
    old_user = User.find_by_email(resource.email)

    if old_user && !old_user.is_registered?
      old_user.update_attributes(:email => "bak_#{SecureRandom.random_number(1000)}_#{old_user.email}", :is_subscribed => false)
    end
    #raise resource.inspect

    # TODO: Its a quick hack. need to change
    resource.roles_mask = 32  # Setting default role for member
    resource.referral_credit = 0  #Setting referral_credit to 0 role for member
    resource.location_id = 1  #default location
    resource.phone = resource.set_phone
    if resource.save
      # 1. set in session
      session[:new_signed_up] = 'true'

      # 2. update referral
      if session[:referral_tracking_id]
        ReferralTracking.update_visitor(resource, session[:referral_tracking_id], 'signup')
        session[:referral_tracking_id] = nil
      end

      if resource.active_for_authentication?
        set_flash_message :notice, :signed_up if is_navigational_format?
        sign_in(resource_name, resource)
        respond_with resource, :location => redirect_location(resource_name, resource)
      else
        set_flash_message :notice, :inactive_signed_up, :reason => resource.inactive_message.to_s if is_navigational_format?
        expire_session_data_after_sign_in!
        respond_with resource, :location => after_inactive_sign_up_path_for(resource)
      end
    else
      #resource.phone = resource.phone.gsub(/^88/, '')
      clean_up_passwords(resource)
      respond_with_navigational(resource) { render_with_scope :new }
    end
  end


  def update
    params[resource_name].delete(:password) if params[resource_name][:password].blank?
    params[resource_name].delete(:roles) if params[resource_name][:roles].present?  # Deletes roles if a hacker tries to inject roles html
    params[resource_name].delete(:referral_credit) if params[resource_name][:referral_credit].present?
    params[resource_name].delete(:password_confirmation) if params[resource_name][:password_confirmation].blank?
    #params[resource_name][:phone] = '88' + params[resource_name][:phone] if params[resource_name][:phone].present?
    if resource.update_attributes(params[resource_name])
      set_flash_message :notice, :updated if is_navigational_format?
      sign_in resource_name, resource, :bypass => true
      respond_with resource, :location => after_update_path_for(resource)
    else
      clean_up_passwords(resource)
       render :file => '/devise/registrations/edit.html.erb'
    end
  end
end