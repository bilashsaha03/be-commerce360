class OrdersController < ApplicationController
  include OrdersHelper
  before_filter :authenticate_user!

  def new
    @order = Order.new
    @order.deal = Deal.find(params[:deal_id]) if params[:deal_id].present?
    @order.product = Product.find(params[:product_id].to_i) if params[:product_id].present?
    page_not_found(@order.product) if @order.product && !@order.product.enabled?
    @order.size = Size.find(params[:size_id].to_i) if params[:size_id].present?
    @order.user = current_user
    if @order.deal.present?
      validate_active_deal(@order.deal)
    else
      validate_active_product(@order.product)
    end
  end

  def create
    #if params[:user][:phone].present? && params[:user][:phone].match("^88").nil?
    #  params[:user][:phone] = MobileTransaction::COUNTRY_CODE + params[:user][:phone]
    #end
    @order = Order.new(params[:order])
    @order.deal = Deal.find(params[:deal_id]) if params[:deal_id].present?
    @order.product = Product.find(params[:product_id].to_i) if params[:product_id].present?
    @order.size = Size.find(params[:size_id].to_i) if params[:size_id].present?
    @order.user = current_user
    @order.is_waiting= !@order.regular_order?


    if @order.deal.present?
      validate_active_deal(@order.deal)
    else
      validate_active_product(@order.product)
    end

    @order.booking_at = Time.now
    @order.user = current_user

      current_user.update_attributes(params[:user])
      @order.user.update_attributes(params[:user])
      corporate_discount = current_user.corporate_discount(current_user, @order)
      prepare_gift()

    begin
      if params[:promotional_code].blank? && @order.save
        ChargeDiscount.create(:amount => (-corporate_discount.discount), :order_id => @order.id, :charge_discount_type => ChargeDiscount::TYPES[:CORPORATE_DISCOUNT]) if corporate_discount
        @order.construct_shipping_cost(params[:shipping])
        redirect_to checkout_path(@order) and return
      elsif params[:promotional_code].present? && promotional_code_matched? && @order.save
        ChargeDiscount.create(:amount => (-@order.orderable.promotional_discount), :order_id => @order.id, :charge_discount_type => ChargeDiscount::TYPES[:PROMOTIONAL_DISCOUNT])
        ChargeDiscount.create(:amount => (-corporate_discount.discount), :order_id => @order.id, :charge_discount_type => ChargeDiscount::TYPES[:CORPORATE_DISCOUNT]) if corporate_discount
        @order.construct_shipping_cost(params[:shipping])
        redirect_to checkout_path(@order) and return
      else
        @order.errors.add(:promotional_code, "is invalid") if (params[:promotional_code].present? && !promotional_code_matched?)
        logger.error "Error::#{@order.errors.full_messages + @user.errors.full_messages}"
        render :new
      end
    rescue Exception => error
      logger.error "Error::#{error.message}"
      render :new
    end

  end

  def edit
    @order = Order.find(params[:id])
    authorize_owner(@order)
    if @order.deal.present?
      validate_active_deal(@order.deal)
    else
      validate_active_product(@order.product)
    end
  end

  def update
    @order = Order.find(params[:id])
    authorize_owner(@order)
    if @order.deal.present?
      validate_active_deal(@order.deal)
    else
      validate_active_product(@order.product)
    end

    begin
      Order.transaction do
        current_user.update_attributes(params[:user])
        @order.user.update_attributes!(params[:user])
        @order.update_attributes!(params[:order])
      end
      redirect_to checkout_path(@order)
    rescue Exception => error
      logger.error "Error:: #{error.message}"
      render :edit
    end
  end

  def checkout
    @order = Order.find(params[:order_id])
    @order.latest_payment
    authorize_owner(@order)
    if @order.deal.present?
      validate_active_deal(@order.deal)
    else
      validate_active_product(@order.product)
    end
  end

  def show
    @order = Order.find(params[:id])
    authorize_owner(@order)
  end

  def book
    session[:payment_method] = Payment::methods[0][:name]
    @order = Order.find(params[:order_id])
    authorize_owner(@order)
    if @order.deal.present?
      validate_active_deal(@order.deal)
    else
      validate_active_product(@order.product)
    end

    update_order_by_referral(@order)

    if @order.deal.present?
      redirect_to location_deal_order_path(@order.deal.location, @order.deal, @order)
    else
      redirect_to location_product_order_path(@order.product.location, @order.product, @order)
    end

  end

  def coupon
    @order = Order.find(params[:order_id])
    authorize_coupon(@order)

    begin
      if @order.coupons.size == 0
        @order.build_security_information() if @order.security_code_booking.nil?
        @order.update_attributes(:confirmed_at => Time.now, :confirmed_by => current_user.id)
      end
    rescue Exception => error
      logger.error "code generation Error:: #{error.message}"
    end

    if @order.deal.present?
      redirect_to location_deal_order_print_coupon_path(@order.deal.location, @order.deal, @order, {:id => @order.coupons.first.id, :gcode => params['gcode'].to_s}) and return if @order.coupons.size == 1
    else
      redirect_to location_product_order_print_coupon_path(@order.product.location, @order.product, @order, {:id => @order.coupons.first.id, :gcode => params['gcode'].to_s}) and return if @order.coupons.size == 1
    end
  end

  def print_coupon
    @coupon = Coupon.find(params[:id])
    @order = @coupon.order
    authorize_coupon(@order)
    render :layout => false if params[:print].present?
  end

  def waiver
    order = Order.find(params[:order_id])
    #CHECKED FREE DEAL & ORDER AMUNT & REFERRER
    if order.deal.nil? ||
        request.referrer.nil? || request.referrer == '/' ||
        !(order.deal.free_coupon? || order.deal.free_gift?)
      page_not_found(order)
    end
    #Chedked owner of the order
    authorize_owner(order)
    #Verified live deal/product
    if order.deal.present?
      validate_active_deal(order.deal)
    else
      validate_active_product(order.product)
    end

    begin
      # Removed payment updation as user not paying at our end
      order.update_attributes!(:confirmed_at => Time.now, :confirmed_by => current_user.id)
      send_free_coupon_email(order)
      if order.deal.present? && order.deal.free_coupon?
        order.coupons.each do |coupon|
          SmsNotifier.send_coupon_sms(coupon, order.user.phone)
        end
      end
    rescue Exception => error
      logger.error "Waiver Error:: #{error.message}"
    end

    redirect_to location_deal_path(order.deal.location, order.deal) + "/free_coupon"
  end

  def free_coupon
    @deal = Deal.find(params[:deal_id])
    page_not_found(@deal) if request.referrer.nil? || request.referrer == '/'
  end

  private

  def promotional_code_matched?
   return false  if !@order.orderable.promotional_code.present?
   return (@order.orderable.promotional_code.to_s == params[:promotional_code].to_s)
  end
  def checkout_path(order)
    if order.deal.present?
      if order.deal.free_coupon? || order.deal.free_gift?
        return location_deal_order_waiver_path(order.deal.location, order.deal, order)
      else
        return location_deal_order_checkout_path(order.deal.location, order.deal, order)
      end
    else
      return location_product_order_checkout_path(order.product.location, order.product, order)
    end
  end

  def prepare_gift()
    if params[:friend_info].to_i == 1
      user = User.find_by_email(params[:friend][:email]) if params[:friend][:email].present?
      if params[:friend][:phone].present? && params[:friend][:phone].match("^88").nil?
        params[:friend][:phone] = MobileTransaction::COUNTRY_CODE + params[:friend][:phone]
      end
      @order.gift = Gift.new(params[:friend])
      @order.gift.verification_code = Time.now().to_i
      @order.gift.receiver = user if user.present?
    end
  end

  def send_free_coupon_email(order)
    if order.deal.present? && order.deal.free_coupon?
      OrderMailer.coupon_email(order).deliver
    end
  end
end
