class PaymentsController < ApplicationController
  include OrdersHelper
  before_filter :authenticate_user!

  def success
    update_payment('INVALID')

    begin
      response = PaymentVerifier.request :checkValidation, {:symbol=> @payment.validation_token}

      if response[:check_validation_response][:return] == "VALID"
        @payment.order.update_attribute(:cc_discount, @payment.order.cc_discount_amount)
        @payment.order.update_attributes(:confirmed_at => Time.now, :confirmed_by => current_user.id, :is_notified => true)
        @payment.update_attribute(:status, "VALID")

        ###################################################################
        # User is using referral credit with credit or debit card
        # And use is using his gained discounts and charges
        # - sign is for producing original discounted amount :-)
        update_order_by_referral(@payment.order)
        #@payment.amount =  @payment.order.payable_amount
        #@payment.save
        ###################################################################

        if @payment.order.due_amount <= 0
          redirect_to_coupon_path(@payment.order)
        else
          redirect_to_payment_incomplete_path(@payment)
        end
      else
        redirect_to_payment_invalid_path(@payment)
      end
    rescue Exception => error
      logger.error "Error:: #{error.message}"
      if @payment.order.deal.present?
        redirect_to location_deal_order_payment_error_path(@payment.order.deal.location, @payment.order.deal, @payment.order, @payment)
      else
        redirect_to location_product_order_payment_error_path(@payment.order.product.location, @payment.order.product, @payment.order, @payment)
      end
    end
  end

  def failed
    update_payment()
    if @payment.order.deal.present?
      redirect_to location_deal_order_checkout_path(@payment.order.deal.location, @payment.order.deal, @payment.order),
                  :notice => 'Unable to process the transaction, please try again or contact with administrator.'
    else
      redirect_to location_product_order_checkout_path(@payment.order.product.location, @payment.order.product, @payment.order),
                  :notice => 'Unable to process the transaction, please try again or contact with administrator.'
    end

  end

  def canceled
    update_payment('CANCELED')
    if @payment.order.deal.present?
      redirect_to location_deal_path(@payment.order.deal.location, @payment.order.deal)
    else
      redirect_to location_product_path(@payment.order.product.location, @payment.order.product)
    end
  end

  def incomplete
  end

  def error
  end

  def invalid
  end

  def by_mobile
    order = Order.find(params[:order_id])
    page_not_found(order) if request.referrer.nil? || request.referrer == '/'

    mobile_number = (MobileTransaction::USER_DEFAULT_CODE + params[:mobile][:number]).to_s
    mobile_transaction = MobileTransaction.first(:conditions => ["transaction_id=? AND mobile_number=?", params[:mobile][:transaction_id], mobile_number])
    used_mobile_transaction = Payment.first(:conditions => ["transaction_id=? AND mobile_number=?", params[:mobile][:transaction_id], mobile_number])

    if used_mobile_transaction
      set_used_mobile_transaction_flash_message(mobile_number)
      redirect_to :controller => :orders, :action => :checkout, :order_id => order, :use_referral => params[:use_referral] and return
    end

    begin
      if mobile_transaction.present?
        if mobile_transaction.used?
          set_used_mobile_transaction_flash_message(mobile_number)
          redirect_to :controller => :orders, :action => :checkout, :order_id => order, :use_referral => params[:use_referral] and return
        else
          mobile_transaction.update_attribute(:is_used, true)
          update_order_by_referral(order)
          MobileTransaction::update_payment(order, mobile_transaction, current_user)
          if order.due_amount <= 0
            redirect_to_coupon_path(order)
          else
            redirect_to_payment_incomplete_path(order.latest_payment) and return
          end
        end
      else
        update_order_by_referral(order)
        order.latest_payment.update_attributes(:mobile_number => mobile_number,
                                               :method => 'bKash',
                                               :transaction_id => params[:mobile][:transaction_id])
        redirect_to :controller => :orders, :action => :show, :id => order, :use_referral => params[:use_referral]
      end
    rescue Exception => error
      logger.error "Error:: #{error.message}"
      redirect_to_payment_invalid_path(order.latest_payment)
    end
  end

  def referral
    order = Order.find(params[:order_id])
    page_not_found(order) if request.referrer.nil? || request.referrer == '/'

    if order.payable_amount.to_f <= 0
      redirect_to :back, :notice => 'You can pay the order by referral credit.'
      return
    end
    if order.payable_amount.to_f > 0 && order.user.referral_balance.to_f < order.payable_amount.to_f
      redirect_to :back, :notice => 'You have not enough referral credit to buy'
      return
    end

    begin
      Order.transaction do
        order.update_attributes(:confirmed_at => Time.now, :confirmed_by => current_user.id)
        order.latest_payment.update_attributes(
            :method => 'REFERRAL-CREDIT',
            :paid_at => Time.now,
            :amount => order.payable_amount.to_f,
            :status => 'VALID'
        )
      end
      redirect_to_coupon_path(order)
    rescue Exception => error
      logger.error "Error:: #{error.message}"
      redirect_to :back, :notice => 'Unable to process the payment, please try again.'
    end
  end

  private
  def set_used_mobile_transaction_flash_message(mobile_number)
    flash[:number] = mobile_number.split(MobileTransaction::USER_DEFAULT_CODE).last
    flash[:transaction_id] = params[:mobile][:transaction_id]
    flash[:mobile_pay_error] = t("TRANSACTION_ID_ALREADY_BEEN_USED")
  end

  def update_payment(status = 'FAILED')
    @payment = Payment.find(params[:payment_id])
    authorize_owner(@payment.order)
    if @payment.order.deal.present?
      validate_active_deal(@payment.order.deal)
    else
      validate_active_product(@payment.order.product)
    end

    begin
      @payment.update_attributes(
          :validation_token => params[:val_id],
          :method => params[:card_type],
          :paid_at => Time.now,
          :amount => params[:amount].to_f,
          :status => status
      )

    rescue Exception => error
      logger.error "Error:: #{error.message}"
    end
  end

  def redirect_to_payment_invalid_path(payment)
    if payment.order.deal.present?
      redirect_to location_deal_order_payment_invalid_path(payment.order.deal.location, payment.order.deal, payment.order, payment)
    else
      redirect_to location_product_order_payment_invalid_path(payment.order.product.location, payment.order.product, payment.order, payment)
    end
  end

  def redirect_to_payment_incomplete_path(payment)
    if payment.order.deal.present?
      redirect_to location_deal_order_payment_incomplete_path(payment.order.deal.location, payment.order.deal, payment.order, payment)
    else
      redirect_to location_product_order_payment_incomplete_path(payment.order.product.location, payment.order.product, payment.order, payment)
    end
  end

  def redirect_to_coupon_path(order)
    if order.deal.present?
      redirect_to location_deal_order_coupon_path(order.deal.location, order.deal, order) and return
    else
      redirect_to location_product_order_coupon_path(order.product.location, order.product, order) and return
    end
  end

end