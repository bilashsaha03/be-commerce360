class MerchantsController < ApplicationController
  def show
    @merchant = Merchant.find_by_url_name(params[:merchant_name])
    redirect_to root_url if !@merchant.has_store_enabled?
  end

  def index
    @merchants = Merchant.enabled
  end
end
