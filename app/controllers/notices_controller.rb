class NoticesController < ApplicationController

  layout false

  def show
    @notice = Notice.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.xml { render :xml => @notice }
    end
  end

end


