class MerchantSweeper < ActionController::Caching::Sweeper
  observe Merchant

  def after_create(merchant)
    expire_cache_for(merchant)
  end

  def after_update(merchant)
    expire_cache_for(merchant)
  end

  def after_destroy(merchant)
    expire_cache_for(merchant)
  end

  private
  def expire_cache_for(merchant)
    Rails.cache.clear
  end

end