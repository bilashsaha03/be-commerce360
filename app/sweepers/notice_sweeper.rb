class NoticeSweeper < ActionController::Caching::Sweeper
  observe Notice

  def after_create(notice)
    expire_cache_for(notice)
  end

  def after_update(notice)
    expire_cache_for(notice)
  end

  def after_destroy(notice)
    expire_cache_for(notice)
  end

  private
  def expire_cache_for(notice)
    expire_fragment("notices-#{I18n.locale}")
  end

end