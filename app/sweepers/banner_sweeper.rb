class BannerSweeper < ActionController::Caching::Sweeper
  observe Banner

  def after_create(banner)
    expire_cache_for(banner)
  end

  def after_update(banner)
    expire_cache_for(banner)
  end

  def after_destroy(banner)
    expire_cache_for(banner)
  end

  private
  def expire_cache_for(banner)
    expire_fragment('left-banners')
    expire_fragment('top_banner')
  end

end