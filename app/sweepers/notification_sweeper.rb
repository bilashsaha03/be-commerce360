class NotificationSweeper < ActionController::Caching::Sweeper
  observe Notification

  def after_create(notification)
    expire_cache_for(notification)
  end

  def after_update(notification)
    expire_cache_for(notification)
  end

  def after_destroy(notification)
    expire_cache_for(notification)
  end

  private
  def expire_cache_for(notification)
    expire_fragment("latest_newsletter")
  end

end