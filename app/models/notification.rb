class Notification < ActiveRecord::Base
  acts_as_audited
  belongs_to :deal
  belongs_to :sender, :class_name => 'User', :foreign_key => 'sent_by'
  has_many :notifications_users
  has_many :users, :through => :notifications_users
  has_many :logs, :class_name => 'LogEmailNotification', :foreign_key => :notification_id, :dependent => :nullify
  has_attached_file :designed_html,
                    :url => "public/newsletters/:id/:basename/index.html",
                    :path => ":rails_root/public/newsletters/:id/:basename.:extension"
  validates_attachment_content_type :designed_html, :content_type => ['application/zip', 'application/octet-stream'], :message => 'is not a zip file'
  validates_presence_of :subject, :from_name, :template, :content
  scope :pending, lambda { where(" status = 'Scheduled' ") }

  def sent?
    return self.status == 'Sent'
  end

  def scheduled?
    return self.status == 'Scheduled'
  end

  def draft?
    return self.status == 'Draft'
  end

  #def send_now
  #  pending_users = self.notifications_users.pending
  #  if pending_users.any?
  #    pending_users.each do |notification_user|
  #      notification_user.notify
  #    end
  #  end
  #end

  def send_now
    pending_users = self.notifications_users.pending
    if pending_users.any?
      groups = pending_users.in_groups(4, false)
      groups.each do |group|
        ActiveRecord::Base.clear_all_connections!
        fork do
          ActiveRecord::Base.establish_connection
          group.each do |notification_user|
            notification_user.notify
          end
          exit
        end
      end
      ActiveRecord::Base.clear_all_connections!
      ActiveRecord::Base.establish_connection
    end
  end

  def self.unsubscribe(conf)
    Unsubscriber.new(conf).unsubscribe()
  end
end
