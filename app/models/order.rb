class Order < ActiveRecord::Base
  attr_protected :referral_credit
  attr_accessor :payable_amount,:shipping_cost,:item_name,:merchant_name
  acts_as_audited
  belongs_to :user
  belongs_to :deal
  belongs_to :product
  belongs_to :size
  belongs_to :confirmed_user, :class_name => 'User', :foreign_key => 'confirmed_by'
  belongs_to :canceled_user, :class_name => 'User', :foreign_key => 'canceled_by'

  has_many :payments, :dependent => :destroy
  has_many :coupons, :dependent => :destroy
  has_many :charge_discounts, :dependent => :destroy

  has_one :gift
  has_one :shipping_detail

  validates_presence_of :user_id, :quantity, :booking_at
  validates_numericality_of :quantity, :only_integer => true, :greater_than_or_equal_to => 1
  attr_accessor :amount, :payment_type, :payment_collector
  scope :for_size, lambda { |size_id| where(" orders.size_id = ? ", size_id) }
  scope :according_sort, order("comment asc, booking_at desc")

  #HOOKS Start
  validate :validate_user, :validate_deal, :validate_product, :on => :create
  validate :validate_order
  after_create :build_security_information, :construct_payment, :send_notification, :track_referral_order
  after_save :generate_coupons, :if => :confirmed_by_changed?
  after_save :track_referral_buy, :if => :is_notified_changed?
  scope :specific_orders, lambda { |deal_ids , product_ids| where(" orders.deal_id in (?) or orders.product_id in (?)" ,deal_ids,product_ids ) }
  #HOOKS END

  def notified?
    return self.is_notified == true
  end

  def cancelled?
    return self.canceled_by != 0
  end

  def confirmed?
    return self.confirmed_by != 0
  end

  def paid?
    return self.due_amount <= 0 && self.notified?
  end

  def redeemed?
    self.coupons.each do |coupon|
      return true if coupon.redeemed?
    end
    return false
  end

  def waiting?
    return is_waiting
  end

  def regular_order?
    if self.deal.present? && self.deal.gilt?
      if self.product.present?
        if self.size.present?
          orders_by_size = self.product.orders.for_size(self.size_id).sum(:quantity).to_i
          return not_in_waiting_list?(orders_by_size, self.product.product_sizes.find_by_size_id(self.size_id).quantity) #covered
        end
        return not_in_waiting_list?(self.product.order_count, self.product.quantity)
      end
      return not_in_waiting_list?(self.deal.order_count, self.deal.max_quantity) #covored

    elsif !self.deal.present? && self.product.present?
      if self.size.present?
        orders_by_size = self.product.orders.for_size(self.size_id).sum(:quantity).to_i
        return not_in_waiting_list?(orders_by_size, self.product.product_sizes.find_by_size_id(self.size_id).quantity) #covered
      end
      return not_in_waiting_list?(self.product.order_count, self.product.quantity)
    else
      return true
    end
  end

  def not_in_waiting_list? (order_count, max_quantity)
    return ((order_count + self.quantity) <= max_quantity) || (order_count < max_quantity && (order_count + self.quantity > max_quantity))
  end

  def skip_notification?
    return self.orderable.skip_notification
  end

  def orderable
    return self.product.present? ? self.product : self.deal
  end

  def latest_payment
    last_payment = self.payments.last
    if last_payment.present? && (self.paid? || last_payment.initialized?)
      last_payment
    else
      construct_payment()
    end
  end

  def amount
    return 0 if self.deal.present? && self.deal.free_gift?
    return (self.quantity * self.unit_price)
  end

  def original_discounted_amount
    return 0 if self.deal.present? && self.deal.free_gift?
    return (self.quantity * self.orderable.discounted_price(true))
  end

  def payable_amount
    return 0 if self.deal.present? && self.deal.free_gift?
    shipping_cost = self.shipping_detail ? self.shipping_detail.amount : 0
    cc_discount_or_charge = self.cc_discount == 0 ? 0 : self.cc_discount_amount
    return self.quantity * (self.unit_price + self.charge_discounts.sum(:amount)) + shipping_cost - cc_discount_or_charge - self.referral_credit
  end

  def shipping_cost
    self.shipping_detail ? self.shipping_detail.amount : 0
  end

  def total_charge_discounts
    return self.quantity * self.charge_discounts.sum(:amount)
  end

  def merchant
       return self.deal.merchant if self.deal.present?
       return self.product.merchant if self.product.present?
  end

  def merchant_name
    return self.merchant.name if self.merchant
  end

  def amount_for_cc
    return 0 if self.deal.present? && self.deal.free_gift?
    return (self.quantity * self.unit_price_for_cc)
  end

  def actual_price
    return self.quantity * self.product.actual_price if self.product.present?
    return self.quantity * self.deal.actual_price
  end

  def paid_amount
    return self.payments.where(:status => 'VALID').sum(:amount)
  end

  def due_amount
    return (self.amount + self.shipping_cost) - (self.paid_amount + self.referral_credit + self.cc_discount) + (self.quantity * self.charge_discounts.sum(:amount))
  end

  def cc_discount_amount
    return self.quantity * self.orderable.discount_cc_payment
  end

  def payment_type
    return self.paid? ? self.latest_payment.method : ''
  end

  def payment_collector_name
    return '' if !self.paid?
    return User.collector_name(self.latest_payment.collector)
  end

  def generate_coupons()
    begin
      coupons = []
      if self.deal.present? && self.deal.max_coupon_quantity.nil? || self.product.present?
        coupons << build_coupon(self.quantity)
      else
        0.step(self.quantity, self.deal.max_coupon_quantity) do |quantity|
          coupons << build_coupon(self.deal.max_coupon_quantity) if quantity > 0
        end
        quantity = self.quantity % self.deal.max_coupon_quantity
        coupons << build_coupon(quantity) if quantity > 0
      end
      self.coupons = coupons
    rescue Exception => error
      logger.error "Coupon code generation error:: #{error.message} - #{error.backtrace}"
    end
  end

  def build_security_information()
    begin
      self.booking_at = Time.now()
      self.security_code_order = SecureRandom.hex(10)

      self.security_code_booking= SecurityCode.getNewCode(
          :entity => 'Booking',
          :entity_id => self.id,
          :valid_upto => self.deal.present? ? self.deal.valid_to.to_s : self.product.valid_to.to_s
      )
      self.save!
    rescue Exception => error
      logger.error "Security code generation error:: #{error.message} - #{error.backtrace}"
    end
  end

  def send_notification
    return if self.skip_notification?

    MerchantMailer.send_order_notification(self).deliver
    if self.deal.present? && self.deal.free_gift?
      OrderMailer.free_gift_email(self).deliver
    elsif !(self.deal.present? && self.deal.free_coupon?)
      OrderMailer.booking_email(self).deliver
    end
  end

  def construct_payment()
    self.payments << Payment.new(
        :status => 'INITIALIZE',
        :transaction_id => (Digest::SHA1.hexdigest Time.now.to_s)
    )
    #TODO: must identify the reason to come in this method during payment processing
    return self.payments.last
  end

  def item_name
    return self.product.name if self.product.present?
    return self.deal.parent.present? ? self.deal.parent.title : self.deal.title
  end

  def item_details
    return self.product.description if self.product.present?
    return self.deal.parent.present? ? self.deal.title : self.deal.description
  end

  def quantity_limit
    return self.product.present? ? 100 : self.deal.max_order_limit
  end

  def unit_price
    return self.product.present? ? self.product.discounted_price : self.deal.discounted_price
  end

  def unit_price_for_cc
    return (self.unit_price - self.orderable.discount_cc_payment).to_i
  end

  #
  # Return boolean depending on order's is gilt or product or deal and their payment method  is declared as card or bKash
  #
  def online_payable?
    return ((self.deal.present? && self.product.present? && self.product.is_pay_by_card?) ||
        (self.deal.present? && self.deal.is_pay_by_card?) ||
        (self.product.present? && self.product.is_pay_by_card?)
    )
  end

  def construct_shipping_cost(shipping)
    if(shipping && shipping[:detail].to_i == 1)
      return ShippingDetail.create(:order_id => self.id, :amount => ShippingDetail::calculate_shipping_cost(self,shipping),
                                   :address => shipping[:address], :choice => shipping[:choice])
    end
  end

  def has_shipping?
    return (self.orderable.min_shipping_cost_dhaka > 0 || self.orderable.min_shipping_cost_bd > 0 || self.orderable.min_shipping_cost_world > 0)
    return false
  end

  private
  def build_coupon(quantity)
    return Coupon.new(
        {
            :security_code_coupon => SecurityCode.getNewCode(
                :entity => 'Coupon',
                :entity_id => self.id,
                :valid_upto => self.deal.present? ? self.deal.valid_to.to_s : self.product.valid_to.to_s),
            :quantity => quantity,
            :security_code_verification => SecureRandom.random_number(1000).to_s
        })
  end

  def order_quantity
    order_limit = self.deal.max_order_limit.to_i
    return true if order_limit == 0
    if order_limit == 1 && self.quantity > 1
      self.errors.add(:base, I18n.t("MUST_BE_ONE_FOR_THIS_DEAL"))
      return false
    elsif self.quantity.to_i > order_limit
      self.errors.add(:base, I18n.t("MUST_BE_BETWEEN_FOR_THIS_DEAL", :order_limit => order_limit.to_bddigit))
      return false
    end
    return true
  end

  def user_order_limit
    if self.deal.free_coupon? || self.deal.gilt? || self.deal.free_gift?
      total_quantity = Order.where("id != ? and deal_id = ? and user_id = ?", self.id.to_i, self.deal_id, self.user_id).sum(:quantity)
      remaining = self.deal.max_order_limit - total_quantity

      if total_quantity >= self.deal.max_order_limit
        self.errors.add(:base, I18n.t("ALREADY_YOU_HAVE_ACCEEDED_THECOUPON_LIMIT"))
        return false
      elsif remaining == 0
        self.errors.add(:base, I18n.t("ALREADY_YOU_HAVE_REACHED_THE_COUPON_LIMIT"))
        return false
      elsif self.quantity > remaining
        self.errors.add(:base, I18n.t("YOU_CAN_ORDER_MORE_COUPONS_FOR_THIS_DEAL", :remaining => remaining.to_bddigit))
        return false
      end
    end
    return true
  end

  def validate_deal
    if self.deal.present? && !self.deal.active?
      self.errors.add(:base, "You are trying to order a invalid deal.")
      return false
    end

    if self.deal.present? && self.deal.sold_out?
      self.errors.add(:base, "Sorry, all the coupons are sold out.")
      return false
    end

    return true
  end

  def validate_product
    if !self.deal.present?
      if self.product.present? && !self.product.active?
        self.errors.add(:base, "You are trying to order a invalid product.")
        return false
      end

      if self.product.present? && self.product.sold_out?
        self.errors.add(:base, "Sorry, all the coupons are sold out.")
        return false
      end
    end
    return true
  end


  def validate_order
    #max order limit for a order
    if self.deal.present? && !self.product.present?
      order_limit = self.deal.max_order_limit.to_i + self.deal.wait_count.to_i
      if order_limit == 1 && self.quantity > 1
        self.errors.add(:base, I18n.t("MUST_BE_ONE_FOR_THIS_DEAL"))
        return false
      elsif self.quantity.to_i > order_limit
        self.errors.add(:base, I18n.t("MUST_BE_BETWEEN_FOR_THIS_DEAL", :order_limit => order_limit.to_bddigit))
        return false
      end

      #user order limit
      if self.deal.free_coupon? || self.deal.gilt? || self.deal.free_gift?
        total_quantity = Order.where("id != ? and deal_id = ? and user_id = ?", self.id.to_i, self.deal_id, self.user_id).sum(:quantity)
        remaining = self.deal.max_order_limit + self.deal.wait_count.to_i - total_quantity

        if total_quantity >= self.deal.max_order_limit+ self.deal.wait_count.to_i
          self.errors.add(:base, I18n.t("ALREADY_YOU_HAVE_ACCEEDED_THECOUPON_LIMIT"))
          return false
        elsif remaining == 0
          self.errors.add(:base, I18n.t("ALREADY_YOU_HAVE_REACHED_THE_COUPON_LIMIT"))
          return false
        elsif self.quantity > remaining
          self.errors.add(:base, I18n.t("YOU_CAN_ORDER_MORE_COUPONS_FOR_THIS_DEAL", :remaining => remaining.to_bddigit))
          return false
        end
      end
    elsif self.product.present?
      total_quantity = Order.where("id != ? and product_id = ? and user_id = ?", self.id.to_i, self.product_id, self.user_id).sum(:quantity)
      remaining = self.product.quantity + self.product.wait_count.to_i - total_quantity
      if self.quantity > remaining
        self.errors.add(:base, I18n.t("YOU_CAN_ORDER_MORE_COUPONS_FOR_THIS_PRODUCT", :remaining => remaining.to_bddigit))
        return false
      end
    else
      self.errors.add(:base, "You must have to select a deal or product to buy")
      return false
    end

    return true
  end

  def validate_user
    self.user.errors.add(:firstname, :blank) if self.user.firstname.blank?
    self.user.errors.add(:address, :blank) if self.user.address.blank?
    self.user.errors.add(:phone, :blank) if self.user.phone.blank?
    if self.user.errors.count > 0
      self.errors.add(:base, I18n.t("INVALID_USER_INFO"))
      return false
    end
    return true
  end

  def track_referral_order
    tracking = ReferralTracking.find_by_visitor_id_and_event(self.user.id, 'signup')
    tracking.update_attributes({:event => 'book', :order_id => self.id}) if tracking.present?
  end

  def track_referral_buy
    tracking = ReferralTracking.find_by_visitor_id_and_event(self.user.id, 'book')
    tracking.update_attributes({:event => 'buy', :order_id => self.id}) if tracking.present?
  end
end
