class ReferralTracking < ActiveRecord::Base
  belongs_to :referral
  belongs_to :order

  #HOOKS Start
  after_update :add_credit_for_referral, :if => :credit_event?
  after_create :update_visit_count
  #HOOKS END

  def credit_event?
    return (self.event_changed? && self.event == 'buy' &&
        (self.order.present? && self.order.paid? && self.order.amount > 200))
  end

  def self.update_visitor(visitor, tracking_id, event)
    referral_tracking = self.find(tracking_id)
    referral_tracking.update_attributes(:visitor_id => visitor.id, :event => event) if referral_tracking
  end

  private
  def add_credit_for_referral
    current_user = User.find(self.visitor_id)
    # We will not provide credit more than once for a specific user self.visitor_id
    if LogReferralCredit.find_by_referred_to_id_and_event(self.visitor_id, self.event).nil?
      LogReferralCredit.create(
          :referred_by => self.referral.user,
          :referred_to => current_user,
          :event => self.event.clone,
          :first_visit => self.created_at < current_user.created_at ? self.created_at : current_user.created_at,
          :amount => Referral::CREDIT[self.event.to_sym]
      )
    end
  end

  def update_visit_count
    self.referral.update_attribute(:visit_count, (self.referral.visit_count.to_i + 1))
  end
end
