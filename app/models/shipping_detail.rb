class ShippingDetail < ActiveRecord::Base
  SHIPPING_CHOICES = {:dhaka => 'inside_dhaka', :bd => 'outside_dhaka', :world => 'outside_bd'}
  belongs_to :order

  #
  # Calculates shipping cost based on item's quantity,weight
  # Returns min cost if weight and min weight is 0
  #
  def self.calculate_shipping_cost(order,shipping)
     choice = shipping[:choice]
     if order.orderable.weight == 0.0 && order.orderable.min_shipping_weight == 0.0
       return order.orderable.min_shipping_cost_dhaka if choice == SHIPPING_CHOICES[:dhaka]
       return order.orderable.min_shipping_cost_bd if choice == SHIPPING_CHOICES[:bd]
       return order.orderable.min_shipping_cost_world if choice == SHIPPING_CHOICES[:world]
     end
     quantity = order.quantity
     weight = order.orderable.weight
     total_weight = (weight * quantity).round(2)
     min_shipping_weight = order.orderable.min_shipping_weight

     min_shipping_cost = 0
     min_shipping_cost = order.orderable.min_shipping_cost_dhaka if choice == SHIPPING_CHOICES[:dhaka]
     min_shipping_cost = order.orderable.min_shipping_cost_bd if choice == SHIPPING_CHOICES[:bd]
     min_shipping_cost = order.orderable.min_shipping_cost_world if choice == SHIPPING_CHOICES[:world]
     resulted_cost = (total_weight/min_shipping_weight).ceil * min_shipping_cost
     return resulted_cost
  end
end
