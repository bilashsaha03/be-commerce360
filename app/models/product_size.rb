class ProductSize < ActiveRecord::Base
  belongs_to :product
  belongs_to :size

  def enabled?
    return self.is_active
  end

end
