class Banner < ActiveRecord::Base
  POSITIONS = ["LEFT","TOP"]
  has_attached_file :picture

  scope :with_position, lambda{|position| where("position=?",position)}
  scope :active, lambda{where("is_active=?",true)}
end
