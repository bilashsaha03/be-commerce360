class Ability
  include CanCan::Ability

  def initialize(user)
    common_access()

    if user
      if user.is?(:member)
        can :access, :payments
      end

      if user.is?(:accountant)
        accountant_access()
      end

      if user.is?(:analyst)
        analyst_access()
      end

      if user.is?(:crm)
        crm_access()
      end

      if user.is?(:marketing_manager)
        marketing_manager_access()
      end

      if user.is?(:merchant)
        merchant_access()
      end

      if user.is?(:collector)
        collector_access()
      end

      if user.is?(:copyrighter)
        copyrighter_access()
      end

      if user.is?(:dealadmin)
        dealadmin_access()
      end

      if user.is?(:superadmin)
        can :access, :all
      end
    end
  end

  private
  def common_access
    can :access, :locations
    can :access, :deals
    can :access, :orders
    can :access, :referrals
    can :access, :"devise/sessions"
    can :new, :"devise/passwords"
    can :create, :"devise/passwords"
    can :edit, :"devise/passwords"
    can :update, :"devise/passwords"
    can :access, :"devise/registrations"
    can :facebook, :"users/omniauth_callbacks"
    can :access, :home
    can :access, :registrations
    can :access, :subscriptions
    can :access, :unsubscribe
    can :access, :businesses
    can :access, :notices
    can [:index, :show, :product_corporate_discount], :products
    can :access, :social_media
    can :access, :poll
    can [:index,:show], :merchants
  end

  def accountant_access
    can [:index, :new_background_image, :save_background_image], :"manage/home"
    can :index, :"manage/payments"
    can [:index, :show, :receive], :"manage/payment_summaries"
    can :show, :"manage/deals"
    can :access, :"manage/coupons"
    can [:index, :show], :"manage/orders"
    can :show, :"manage/audits"
  end

  def analyst_access
    can [:index, :new_background_image, :save_background_image], :"manage/home"
    can [:index, :show, :sort, :update_order, :stat_report], :"manage/deals"
    can [:index, :show], :"manage/orders"
    can :index, :"manage/merchants"
    can :index, :"manage/coupons"
    can :index, :"manage/payments"
    can :index, :"manage/payment_summaries"
    can :logs, :"manage/notifications"
    can :index, :"manage/notices"
    can [:index, :show], :"manage/audits"
    can [:index, :show], :"manage/users"
    can :access, :"manage/referrals"
  end

  def crm_access
    can [:index, :new_background_image, :save_background_image], :"manage/home"
    can [:index, :create, :update, :show], :"manage/merchants"
    can [:index, :show], :"manage/businesses"
    can :show, :"manage/audits"
    can [:index, :create, :update], :"manage/deals"
    can [:index, :create, :update], :"manage/products"
    can :clear_cache, :"manage/settings"
    can [:login_as, :login], :"manage/users"
  end

  def marketing_manager_access
    can [:index, :new_background_image, :save_background_image], :"manage/home"
    can [:index, :show], :"manage/merchants"
    can [:sort, :update_order], :"manage/deals"
    can [:index, :create, :update, :show], :"manage/notices"
    can [:index, :verify, :show], :"manage/users"
    can :show, :"manage/audits"
    can :access, :"manage/notifications"
    can :access, :"manage/referrals"
    can :access, :"manage/presses"
    can :access, :"manage/polls"
  end

  def merchant_access
    can [:index,:merchants_orders ], :"merchant/report"
    can :access, :"merchant/home"
    can :access, :"merchant/coupons"
    can :access, :"merchant/products"
  end

  def collector_access
    can :access, :"manage/orders"
    can :access, :"manage/coupons"
    can :index, :"manage/home"
    can :show, :"manage/deals"
    can :show, :"manage/audits"
    can [:index, :update, :create], :"manage/mobile_transactions"
    can :clear_cache, :"manage/settings"
  end

  def copyrighter_access
    can :index, :"manage/home"
    can [:index, :create, :update, :show], :"manage/merchants"
    can [:index, :update, :create, :show], :"manage/deals"
    can :show, :"manage/audits"
  end

  def dealadmin_access
    can [:index, :new_background_image, :save_background_image], :"manage/home"
    can :access, :"manage/deals"
    can [:index, :create, :update, :show], :"manage/merchants"
    can [:index, :create, :update, :show], :"manage/notices"
    can :show, :"manage/audits"
    can [:index, :create, :update, :destroy], :"manage/products"
    can :access, :"manage/notifications"
    can :clear_cache, :"manage/settings"
    can :access, :"manage/corporate_discounts"
    can [:index, :create, :update, :show], :"manage/categories"
  end
end

