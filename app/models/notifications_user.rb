class NotificationsUser < ActiveRecord::Base
  belongs_to :user
  belongs_to :notification

  scope :of, lambda { |id| where(" notification_id = #{id}") }
  scope :pending, lambda { where("notifications_users.is_sent = 0 ") }
  scope :sent, lambda { where("notifications_users.is_sent = 1 ") }
  scope :order_by_desc, order(" notifications_users.id desc ")

  def notify
    MailNotifier.notify(self)
  end
end
