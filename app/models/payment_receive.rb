class PaymentReceive < ActiveRecord::Base
  acts_as_audited
  belongs_to :payment_summary, :class_name => 'PaymentSummary', :foreign_key => 'summary_id'
  belongs_to :user, :class_name => 'User', :foreign_key => 'receiver_id'
end
