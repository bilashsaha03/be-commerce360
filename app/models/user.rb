class User < ActiveRecord::Base
  ROLES = %w[superadmin dealadmin copyrighter collector merchant member accountant analyst crm marketing_manager]
  PHONE_INITIALS = %w[011 015 016 017 018 019]

  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable, :lockable and :timeoutable
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :omniauthable, :validatable

  has_many :copyrighted_deals, :class_name => 'Deal', :foreign_key => 'copyrighter_id', :dependent => :nullify
  has_many :managed_deals, :class_name => 'Deal', :foreign_key => 'manager_id', :dependent => :nullify
  has_many :managed_products, :class_name => 'Product', :foreign_key => 'manager_id', :dependent => :nullify

  has_many :orders, :dependent => :destroy
  has_many :confirmed_orders, :class_name => 'Order', :foreign_key => 'confirmed_by', :dependent => :nullify
  has_many :canceled_orders, :class_name => 'Order', :foreign_key => 'canceled_by', :dependent => :nullify
  has_many :collected_payments, :class_name => 'Payment', :foreign_key => 'collector_id', :dependent => :nullify
  has_many :redeemed_coupons, :class_name => 'Coupon', :foreign_key => 'redeemed_by_user_id', :dependent => :nullify
  has_many :payment_summaries, :class_name => 'PaymentSummary', :foreign_key => 'collector_id', :dependent => :nullify
  has_many :payment_receives, :class_name => 'PaymentReceive', :foreign_key => 'receiver_id', :dependent => :nullify

  has_many :authorizations, :dependent => :destroy
  has_many :users_merchants
  has_many :merchants, :through => :users_merchants

  has_many :created_notices, :class_name => 'Notice', :foreign_key => 'created_by', :dependent => :nullify
  has_many :updated_notices, :class_name => 'Notice', :foreign_key => 'updated_by', :dependent => :nullify

  has_many :gifts
  has_many :referrals
  has_many :referral_events, :class_name => 'LogReferralCredit', :foreign_key => :referred_by_id, :dependent => :nullify
  belongs_to :subscribed_location, :class_name => 'Location', :foreign_key => :location_id

  has_many :notifications_users
  has_many :notifications, :through => :notifications_users
  has_many :sent_notifications, :class_name => 'Notification', :foreign_key => 'sent_by', :dependent => :nullify
  has_many :email_notifications, :class_name => 'LogEmailNotification', :foreign_key => :receiver_id, :dependent => :nullify

  acts_as_audited :except => [:password]
  attr_accessor :login
  attr_accessible :login, :firstname, :lastname, :name, :email, :phone,
                  :address, :password, :password_confirmation, :remember_me,
                  :gender, :is_subscribed, :is_active, :suspended_reason, :suspended_at, :roles,
                  :bounce_count, :complain_count, :is_verified
  attr_protected :referral_credit

  #VALIDATION START
  validates_uniqueness_of :email
  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create
  validates_uniqueness_of :phone, :allow_nil => true, :allow_blank => true
  #validate :check_phone_format
  #validates_length_of :phone, :is => 13, :allow_nil => false, :message => I18n.t("PHONE_IS_WRONG_LENGTH")
  validates_length_of :password, :in => 6..20, :on => :create

  #SCOPE START
  scope :subscribers, where(:is_subscribed => true)
  scope :notifiable, where("is_subscribed = 1 and is_verified = 1 and bounce_count = 0 and complain_count = 0")
  scope :admins, where("roles_mask & 0xf")
  scope :not_members, where("roles_mask <> 32")
  scope :with_role, lambda { |role| {:conditions => "roles_mask & #{2**ROLES.index(role.to_s)} > 0"} }

  #HOOKS START
  after_create :send_welcome_email
  after_update :update_email_verification, :if => :email_changed?

  def is?(role)
    return true if self.super_admin?
    return roles.include?(role.to_s)
  end

  def admin?
    !(self.roles & %w[superadmin dealadmin copyrighter collector]).empty?
  end

  def analyst?
    !(self.roles & %w[analyst]).empty?
  end

  def active?
    return self.is_active == true
  end

  def merchant?
    !(self.roles & %w[merchant]).empty?
  end

  def super_admin?
    !(self.roles & %w[superadmin]).empty?
  end

  def member?
    return (!self.roles_mask || self.roles_mask == 32)
  end

  def notifiable?
    return false if !self.is_verified? || (self.bounce_count + self.complain_count) > 0
  end

  # Methods for role management
  def roles=(roles)
    self.roles_mask = User.roles_mask_for_roles(roles)
  end

  def roles
    ROLES.reject do |r|
      ((roles_mask || 0) & 2**ROLES.index(r)).zero?
    end
  end

  # Returns phone number without country code(i,e 01711xxxxxx)
  def phone_without_country_code
    return self.phone
    return self.phone.to_s[2..20] if self.phone.to_s[0, 2] == MobileTransaction::COUNTRY_CODE
    return self.phone
  end

  # Sets phone number with country code (i,e 8801711xxxxxx)
  def set_phone
    return self.phone
    if self.phone.present? && self.phone.to_s.match(/^88/).nil?
      self.phone = MobileTransaction::COUNTRY_CODE + self.phone.to_s
    end
    return self.phone
  end

  def referral_balance
    current_user = User.find_by_id(self.id)
    return 0.0 if current_user.nil?
    current_user.referral_credit
  end

  def name
    return "#{self.firstname} #{self.lastname}".strip
  end

  def name_or_email
    return self.name.blank? ? self.email : self.name
  end

  # set firstname and lastname from fullname
  # @param [String] full name of the user
  def name=(full_name)
    self.firstname, *rest = full_name.split
    self.lastname = rest.join(" ")
  end

  # Set token from hash returned by facebook
  # @param [String] serialized hash returned from facebook
  def set_token_from_hash(hash)
    token = self.authorizations.find_or_initialize_by_provider(hash[:provider])
    token.update_attributes(
        :uid => hash[:uid],
        :nickname => hash[:nickname],
        :url => hash[:url],
        :credentials => hash[:credentials]
    )
  end

  def role_symbols
    roles.map(&:to_sym)
  end

  def valid_phone?
    # TO DO: can be check by start with in validation
    phone_is_valid = false
    phone = self.phone.to_s
    User::PHONE_INITIALS.each do |phone_init|
      if (phone.match("^#{phone_init}") && phone.length == 11) || (phone.match("^88#{phone_init}") && phone.length == 13)
        phone_is_valid = true
        break
      end
    end
    return phone_is_valid
  end

  def self.find_for_facebook_oauth(access_token, signed_in_resource=nil)
    data = access_token['extra']['user_hash']
    if user = User.find_by_email(data["email"])
      return user
    else
      user = User.new(:roles_mask => 32, :email => data["email"], :password => Devise.friendly_token[8, 20], :firstname => data['first_name'],
                      :lastname => data['last_name'], :gender => data['gender'] == 'male' ? 'M' : 'F')
      user.save(false)
      return user
    end
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["user_hash"]
        user.email = data["email"]
      end
    end
  end

  def self.collector_name(user = nil)
    return '' if user.nil?
    return user.email[0..(user.email.index('@')-1)].gsub('.', ' ').humanize
  end

  def self.roles_mask_for_roles(roles)
    (roles & ROLES).map { |r| 2**ROLES.index(r) }.sum
  end

  def corporate_discount(user, order)
    return nil if user.nil?
    type = order.orderable.class.to_s.downcase
    type_id = order.orderable.id
    return CorporateDiscount.first(:conditions => ["domain = ? AND item_type=? AND item_id=?", user.email.split('@').last, type, type_id])
  end

  def corporate_discount_of(item)
    CorporateDiscount.first(:conditions => ["domain = ? AND item_type=? AND item_id=?", self.email.split('@').last, item.class.to_s.downcase, item.id])
  end

  private

  def send_welcome_email
    UserMailer.welcome_email(self).deliver() if self.is_registered == true
  end

  def update_email_verification()
    User.update(self.id, {
        :bounce_count => LogEmailNotification.where(:receiver_email => self.email, :is_bounced => true).count(),
        :complain_count => LogEmailNotification.where(:receiver_email => self.email, :is_complained => true).count(),
        :is_verified => 1
    })
  end

  protected
  def self.find_for_database_authentication(conditions)
    login = conditions.delete(:login)
    where(conditions).where(["phone = :value OR email = :value", {:value => login}]).first
  end

  def check_phone_format
    if self.phone.present?
      if self.phone.match(/^\+?([0-9]|-)+$/).nil? || !self.valid_phone?
        errors.add(:base, I18n.t("PHONE_IS_INVALID"))
      end
    end
  end
end
