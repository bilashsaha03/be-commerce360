class UserPoll < ActiveRecord::Base
  validates_uniqueness_of :user_id, :scope => :poll_id
end
