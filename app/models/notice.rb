class Notice < ActiveRecord::Base
  acts_as_audited
  belongs_to :creator, :class_name => 'User', :foreign_key => 'created_by'
  belongs_to :updater, :class_name => 'User', :foreign_key => 'updated_by'
  has_many :pictures, :dependent => :destroy
  accepts_nested_attributes_for :pictures, :allow_destroy => true

  scope :live, lambda { where("? between notices.start_date_time and notices.end_date_time and notices.published = ? and notices.is_deleted = ?", DateTime.now, true, false).order("created_at DESC") }
  scope :viewable, lambda { where("notices.is_deleted = ?", false).order("created_at DESC") }


end
