class LogEmailNotification < ActiveRecord::Base
  belongs_to :receiver, :class_name => 'User', :foreign_key => :receiver_id
  belongs_to :notification

  #HOOKS START#
  after_save :update_user_info
  #HOOK END#

  def self.bounce_types
    return ['Complain', 'Delivery Failure', 'Unsubscribe', 'Verification Failure']
  end

  def unverified?
    return self.bounce_type == 'Verification Failure'
  end

  protected
  def update_user_info
    self.receiver.update_attributes(
        {
            :bounce_count => (self.is_bounced? ? self.receiver.bounce_count + 1 : self.receiver.bounce_count),
            :complain_count => (self.is_complained? ? self.receiver.complain_count + 1 : self.receiver.complain_count),
            :is_subscribed => (self.is_unsubscribed? ? false : self.receiver.is_subscribed),
            :is_verified => (self.unverified? ? false : self.receiver.is_verified)
        }
    ) if self.receiver.present?
  end
end