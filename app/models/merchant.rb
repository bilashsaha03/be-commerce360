class Merchant < ActiveRecord::Base
  acts_as_audited
  has_many :deals
  has_many :parent_deals, :class_name => 'Deal', :conditions => {:parent_id => nil}
  has_many :users_merchants
  has_many :users, :through => :users_merchants
  #has_many :pictures
  has_attached_file :logo, :styles => { :thumb => "120x60>" }
  has_attached_file :banner, :styles => {:big => "680x200>" }

  has_many :products

  #accepts_nested_attributes_for :pictures, :allow_destroy => true

  attr_accessor :user_emails
  validates_presence_of :name, :address

  translates :name, :address

  scope :featured, lambda{where("is_featured=?",true)}
  scope :enabled, lambda{where("has_store_enabled=?",true)}


  def manager? user
    !user.nil? && user.instance_of?(User) && self.users.include?(user)
  end

  def approved_products
     return Product.live.where("merchant_id = ? " ,self.id)
  end


  def user_emails
    emails = []
    self.users.each do |user|
      emails << user.email if user.email.present?
    end
    return emails.join(',')
  end
end
