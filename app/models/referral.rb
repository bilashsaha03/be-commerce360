class Referral < ActiveRecord::Base
  CODE_DIGIT = 14
  SIXTY_TWO = ('0'..'9').to_a + ('a'..'z').to_a + ('A'..'Z').to_a
  CREDIT = {:signup => 0.0, :book => 0.0, :buy => 50.0}

  belongs_to :user
  belongs_to :referable, :polymorphic => true
  has_many :referral_trackings

  def self.create_referral(user)
    return Referral.find_by_user_id(user.id) || create(:user => user, :code => get_unique_code())
  end

  def self.mediums
    [:newsletter, :email, :facebook, :twitter]
  end

  def track(request, event = 'visit', current_user = nil)
    return self.referral_trackings.create(
        {
            :visit_time => Time.now,
            :visitor_ip => request.ip,
            :visitor_id => (current_user.nil? ? nil : current_user.id),
            :user_agent => request.user_agent,
            :event => event
        }
    )
  end

  private
  def self.get_unique_code
    rnd = ''
    begin
      rnd = generate_random()
    end while self.find_by_code(rnd)
    return rnd
  end

  def self.generate_random
    SecureRandom.random_number(10**CODE_DIGIT).b(10).to_a(SIXTY_TWO).join
  end

end
