class SmsNotification < ActiveRecord::Base
  belongs_to :coupon

  def self.save_response(response, message, coupon = nil)
    begin
      if response.present?
        @send_notification = self.new
        @send_notification.message_id = response[:send_text_message_response][:send_text_message_result][:message_id]
        @send_notification.status = response[:send_text_message_response][:send_text_message_result][:status]
        @send_notification.status_text = response[:send_text_message_response][:send_text_message_result][:status_text]
        @send_notification.error_code = response[:send_text_message_response][:send_text_message_result][:error_code]
        @send_notification.error_text = response[:send_text_message_response][:send_text_message_result][:error_text]
        @send_notification.message = message
        @send_notification.coupon_id = coupon.id if coupon.present?
        @send_notification.is_sent = true
        return @send_notification.save!
      end
    rescue Exception => error
      logger.error "Error:: #{error.message}"
    end
    return false
  end

end
