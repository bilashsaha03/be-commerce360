class Business < ActiveRecord::Base
  validates :name, :category, :email, :phone, :city, :contact_person_name, :presence => true
  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create
  validates_format_of :phone, :with => /^\+?([0-9]|-)+$/, :in => 11..25

  scope :from, lambda { |user_ip| where("businesses.ip_address = ? and TIME_TO_SEC(TIMEDIFF(now(), created_at)) <= #{SITE_CONFIG['get_featured']['time_diffrence']}", user_ip) }

  #HOOKS START#
  after_create :send_notification
  #HOOK END#


  private
  def send_notification()
    begin
      BusinessMailer.get_featured(self).deliver
      BusinessMailer.get_featured_sales_notification(self).deliver
    rescue Exception => error
      logger.error "Email sending error:: #{error.message}"
    end
  end
end
