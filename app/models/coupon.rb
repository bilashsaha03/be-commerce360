class Coupon < ActiveRecord::Base
  acts_as_audited
  belongs_to :order, :include => :user
  belongs_to :redeemed_by_user, :class_name => 'User', :foreign_key => 'redeemed_by_user_id'
  has_many :log_failed_redeems, :dependent => :destroy
  has_many :sms_notifications, :dependent => :destroy

  MAX_REDEEM_ATTEMPT = 5

  scope :for, lambda { |*ids|
    {
        :joins => {:order => [:user, :deal]},
        :conditions => "(deals.id in (#{ids.join(', ')}) or deals.parent_id in (#{ids.join(', ')}) )",
        :order => 'users.firstname asc, coupons.created_at asc'
    }
  }
  scope :for_all, lambda { |*conditions|
    {
        :include => {:order => [:user, :deal, :product]},
        :conditions => " #{'  '+conditions.join(' or ') if conditions.any?} ",
        :order => 'users.firstname asc, coupons.created_at asc'
    }
  }

  scope :confirmed, where("orders.confirmed_by is TRUE")
  scope :paid, where("orders.is_notified IS TRUE ")
  scope :not_canceled, where("orders.canceled_by IS FALSE")
  scope :matched_code, lambda { |coupon_code| where(" coupons.security_code_coupon like '%#{coupon_code}%' ") }
  scope :redeemed_coupons, where("redeemed_at IS NOT NULL AND redeemed_by_user_id IS NOT NULL")

  def redeemed?
    return self.redeemed_by_user_id.present? ? true : false
  end

  def redeemable?
    return true if self.order.deal.present? && self.order.deal.running?
    return true if self.order.product.present? && self.order.product.running?
    return false
  end

  def redeemed_ability?(user)
    return true if self.order.deal.present? && self.order.deal.merchant.users.include?(user)
    return true if self.order.product.present? && self.order.product.merchant.users.include?(user)
    return false
  end

  def show_logo?
    return self.order.deal.parent.present? ? self.order.deal.parent.is_show_logo : self.order.deal.is_show_logo
  end

  def show_value?
    return self.order.deal.parent.present? ? self.order.deal.parent.is_show_value : self.order.deal.is_show_value
  end

  def gift?
    return self.order.gift.present?
  end

  def show_paid_amount?
    return self.order.deal.parent.present? ? self.order.deal.parent.is_show_paid_amount : self.order.deal.is_show_paid_amount
  end

  def amount
    return 0 if self.order.deal.present? && (self.order.deal.free_coupon? || self.order.deal.free_gift?)
    return self.quantity * self.order.product.discounted_price if self.order.product.present?
    return self.quantity * self.order.deal.discounted_price
  end

  def actual_price
    return self.quantity * self.order.product.actual_price if self.order.product.present?
    return self.quantity * self.order.deal.actual_price
  end

  def user_name
    return "#{self.order.gift.firstname} #{self.order.gift.lastname}" if self.order.gift.present?
    return self.order.user.name
  end

  def coupon_code
    return self.security_code_coupon + (self.security_code_verification.nil? ? '' : "-#{self.security_code_verification}")
  end

  def item_name
    return self.order.item_name
  end

  def valid_to
    return self.order.orderable.valid_to
  end

  def merchant_name
    return self.order.orderable.merchant.name
  end

  def coupon_header
    return self.order.deal.parent.present? ? self.order.deal.parent.coupon_header : self.order.deal.coupon_header
  end


  def merchant_payable_amount
    if self.order.orderable.advance_payable?
      return (self.quantity * self.order.orderable.discounted_price_for_advance_payment) - (self.quantity * self.order.orderable.discounted_price)
    else
      return self.order.due_amount
    end
  end

end
