class Payment < ActiveRecord::Base
  acts_as_audited
  belongs_to :order
  belongs_to :collector, :class_name => 'User', :foreign_key => 'collector_id'
  attr_accessor :collector_name

  scope :latest_first, order(" payments.id desc ")
  scope :not_initialized, order(" payments.status != 'INITIALIZE' ")
  scope :for, lambda { |user_id| {:joins => [:order], :conditions => ["orders.user_id = ? and payments.status = 'VALID'", user_id]} }
  scope :by, lambda { |pay_method| {:joins => [:order], :conditions => ["payments.method = ?", pay_method]} }


  #HOOKS START#
  after_save :send_notification, :update_summary_table
  after_save :update_referral_balance, :if => :referral?
  #HOOK END#

  def initialized?
    return self.status == 'INITIALIZE'
  end

  def cash?
    return self.method == 'CASH'
  end

  def mwallet?
    return self.method == 'bKash'
  end

  def referral?
    return self.method == 'REFERRAL-CREDIT'
  end

  def update_summary_table()
    return if !self.collector.present?

    begin
      collector_id = self.collector_id
      deal_id = self.order.deal.parent.present? ? self.order.deal.parent_id : self.order.deal_id
      payment_summary = PaymentSummary.find_by_collector_id_and_deal_id(collector_id, deal_id)

      if payment_summary.nil?
        payment_summary = PaymentSummary.new(
            {
                :collector_id => collector_id,
                :deal_id => deal_id,
                :collection_amount => 0
            }
        )
      end

      payment_summary.collection_amount = 0 if payment_summary.collection_amount.blank?
      payment_summary.due_amount = 0 if payment_summary.due_amount.blank?
      payment_summary.collection_amount = payment_summary.collection_amount + self.amount - self.order.referral_credit
      payment_summary.due_amount = payment_summary.due_amount + self.amount - self.order.referral_credit
      payment_summary.save
    rescue Exception => error
      logger.error "Error:: #{error.message}"
    end
  end

  def collector_name
    return User.collector_name(collector)
  end

  def self.statuses
    return [:INITIALIZE, :VALID, :INVALID, :FAILED, :CANCELED]
  end

  #
  # Returns (amount - referral credit) of a payment
  #
  def amount_without_referral
    return self.amount if self.order.nil?
     return (self.amount - self.order.referral_credit)
  end

  def self.methods
    return [
        {:id => 'CASH', :name => 'CASH'},
        {:id => 'NEXUS', :name => 'NEXUS'},
        {:id => 'NEXUS-Dutch Bangla', :name => 'NEXUS-Dutch Bangla'},
        {:id => 'MASTER-Dutch Bangla', :name => 'MASTER-Dutch Bangla'},
        {:id => 'VISA-Brac bank', :name => 'VISA-Brac bank'},
        {:id => 'bKash', :name => 'bKash'},
        {:id => 'REFERRAL-CREDIT', :name => 'REFERRAL-CREDIT'}]
  end

  protected
  def send_notification
    if self.status == 'VALID'
      if self.method == 'REFUND'
        self.order.update_attributes(:notified_at => nil, :is_notified => false)
        OrderMailer.cancel_email(self.order, {:name => self.order.user.name, :email => self.order.user.email}).deliver if !self.order.skip_notification?
      else
        SmsNotifier.send_mwallet_verification_sms(self) if self.mwallet?
        if !self.order.skip_notification? && ((self.order.deal.present? && !self.order.deal.free_coupon? && !self.order.deal.free_gift?) || self.order.product.present?)
          OrderMailer.money_receipt_email(self).deliver
        end

        if self.order.due_amount <= 0
          #Update paid status
          self.order.update_attributes(:notified_at => Time.now, :is_notified => true)
          if !self.order.skip_notification?
            #Send Coupon and Money Receipt
            if self.order.gift.present?
              OrderMailer.gift_receiver_email(self).deliver
              OrderMailer.gift_sender_email(self).deliver
              phone_number = self.order.gift.phone
            else
              if (self.order.deal.present? && !self.order.deal.free_gift?) || self.order.product.present?
                OrderMailer.coupon_email(self.order).deliver
              end
              phone_number = self.order.user.phone
            end
            #Send M-Coupon
            if (self.order.orderable.is_enable_mcoupon?) && (self.order.payment_type != 'CASH') && (self.order.deal.present? && !self.order.deal.free_gift?)
              self.order.coupons.each do |coupon|
                SmsNotifier.send_coupon_sms(coupon, phone_number)
              end
            end
          end
        elsif self.order.paid_amount > 0 && !self.order.skip_notification?
          OrderMailer.incomplete_email(self.order).deliver
        end
      end
    end
  end

  def update_referral_balance
    self.order.user.update_attribute(:referral_credit, self.order.user.referral_credit - self.amount)
  end
end
