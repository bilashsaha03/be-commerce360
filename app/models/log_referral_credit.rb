class LogReferralCredit < ActiveRecord::Base
  belongs_to :referred_by, :class_name => 'User', :foreign_key => :referred_by_id
  belongs_to :referred_to, :class_name => 'User', :foreign_key => :referred_to_id

  after_create :update_referral_credit, :update_success_count
  after_destroy :reset_referral_credit
  scope :for_user, lambda { |user_id| where("amount > 0 and referred_by_id = ? ", user_id) }

  protected
  def update_referral_credit
    self.referred_by.update_attribute(:referral_credit, (self.referred_by.referral_credit.to_f + self.amount.to_f))
  end

  def update_success_count
    referral = Referral.find_by_user_id(self.referred_by_id)
    referral.update_attribute(:success_count, (referral.success_count.to_i + 1))
    UserMailer.referral_credit_email(self).deliver
  end

  def reset_referral_credit
    referral_credit = self.referred_by.referral_credit.to_f - self.amount.to_f
    self.referred_by.update_attribute(:referral_credit, (referral_credit < 0 ? 0 : referral_credit))
    referral = Referral.find_by_user_id(self.referred_by_id)
    success_count = referral.success_count.to_i - 1
    referral.update_attribute(:success_count, (success_count < 0 ? 0 : success_count))
  end
end
