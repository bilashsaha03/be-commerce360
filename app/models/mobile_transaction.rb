class MobileTransaction < ActiveRecord::Base
  COUNTRY_CODE = '88'
  USER_DEFAULT_CODE = '88'
  VALID_TOTAL_DIGIT = 11
  BKASH_INITIALS = ["017", "018", "019"]

  belongs_to :user

  validates :mobile_number, :presence => true, :length => {:minimum => VALID_TOTAL_DIGIT, :maximum => VALID_TOTAL_DIGIT}
  validates_presence_of :amount
  validates_numericality_of :amount, :greater_than => 0
  validates :transaction_id, :presence => true
  validates_uniqueness_of :transaction_id

  validate :check_mobile_number_format
  before_save :append_country_code

  def used?
    return is_used?
  end

  def self.update_payment(order, transaction, user)
    return if transaction.amount <= 0
    order.update_attributes(:confirmed_at => Time.now, :confirmed_by => 1) if order.coupons.empty?
    order.payments.last.update_attributes(:amount => transaction.amount,
                                           :method => 'bKash',
                                           :paid_at => Time.now,
                                           :updated_at => Time.now,
                                           :transaction_id => transaction.transaction_id,
                                           :validation_token => transaction.id,
                                           :status => Payment::statuses[1].to_s)
  end

  def self.bkash_phone?(phone)
    phone = phone.to_s
    phone_is_valid = false
    MobileTransaction::BKASH_INITIALS.collect{|phone_init|
      if phone.match("^#{phone_init}")
        phone_is_valid = true
        break
      end
    }
    return phone_is_valid
  end

  private
  def check_mobile_number_format
      errors.add_to_base('bKash Mobile number is invalid') if !MobileTransaction::bkash_phone?(self.mobile_number)
  end

  def append_country_code
    self.mobile_number = COUNTRY_CODE + self.mobile_number  if self.mobile_number.match(/^88/).nil?
  end
end
