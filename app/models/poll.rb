class Poll < ActiveRecord::Base

  scope :live, lambda { where("? >= start_date AND ? <= end_date", Time.now, Time.now) }
  scope :active, lambda { where("is_active=?", true) }

  has_many :poll_options, :order => "vote_count DESC", :dependent => :destroy

  accepts_nested_attributes_for :poll_options, :allow_destroy => true, :reject_if => proc { |attributes| attributes['option'].blank? }

  #
  # Checks whether an user has given his vote to a particular poll or not
  #
  def vote_casted?(user)
    return false if user.nil?
    user_poll = UserPoll.first(:conditions => ["user_id=? AND poll_id=?",user.id, self.id])
    return true if user_poll.present?
    return false
  end

end
