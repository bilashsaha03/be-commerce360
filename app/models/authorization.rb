class Authorization < ActiveRecord::Base
  belongs_to :user

  serialize :credentials

  validates_uniqueness_of :uid, :scope => :provider

  scope :twitter, where(:provider => 'twitter')
  scope :facebook, where(:provider => 'facebook')
end
