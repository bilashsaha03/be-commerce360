class Gift < ActiveRecord::Base
  belongs_to :receiver, :class_name => 'User', :foreign_key => 'user_id'
  belongs_to :order
  validates_presence_of :email, :firstname, :phone, :address

  def name
    return "#{self.firstname} #{self.lastname}"
  end
end
