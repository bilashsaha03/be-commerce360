class CorporateDiscount < ActiveRecord::Base

  acts_as_audited
  validates :domain, :presence => true
  validates :discount, :presence => true
  validates_numericality_of :discount, :greater_than => 0
  validates_uniqueness_of :domain,  :scope => [:item_type, :item_id]



  def deal_or_product_name
    if self.item_type=="deal"
      deal = Deal.find(self.item_id)
      return deal.title
    elsif self.item_type=="product"
      product = Product.find(self.item_id)
      return product.name
    end
  end
end
