class SecurityCode < ActiveRecord::Base
  has_many :security_code_assignments

  after_create :log_assignment


  def self.getNewCode(options = {})
    code = ''
    row = nil
    loop do
      puts "Log: Generating new security code"
      number = SecureRandom.random_number(100000000).to_s
      puts "Log: Generated coupon code - #{number}"
      row = find_by_code(number)
      if row.nil?
        puts "Log: No previous entry with the security code"
        code = number
        row = new({
                   :code => number,
                   :entity => options[:entity],
                   :entity_id => options[:entity_id],
               })
        break
      elsif row.valid_upto < Time.now
        puts "Log: Security code already exists. Reassigning..."
        code = row.code
        row.entity    = options[:entity]
        row.entity_id = options[:entity_id]
        break
      else
        puts "Log: Security code already exists"
      end
    end
    row.valid_upto = options[:valid_upto].nil? ? 6.months.from_now : DateTime.parse(options[:valid_upto])
    row.issued_at= Time.now
    row.save
    return code
  end

  def log_assignment
    SecurityCodeAssignment.create(
        :entity         => self.entity,
        :entity_id      => self.entity_id,
        :valid_upto     => self.valid_upto,
        :security_code  => self
    )
  end
end

