class Category < ActiveRecord::Base
  validates_presence_of :name
  validates_uniqueness_of :name
  translates :name, :description
  has_many :products
  has_many :sizes

  scope :active_categories, where(:is_active => true)

end
