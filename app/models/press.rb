class Press < ActiveRecord::Base
  has_attached_file :logo, :styles => { :medium => "130x55>", :thumb => "80x30>" }
  scope :latest_first, order(" published_at DESC")
end
