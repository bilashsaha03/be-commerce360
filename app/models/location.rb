class Location < ActiveRecord::Base
  has_many :users
  has_many :deals
  has_many :products

  validates_uniqueness_of :name, :url
  validates_presence_of :name, :url

  has_friendly_id :url, :use_slug => true

  def self.default
    Location.where(:is_default => true).first
  end

  def to_param
    self.friendly_id
  end
end
