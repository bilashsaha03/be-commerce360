class Deal < ActiveRecord::Base
  CATEGORIES = {'Deal' => '0', 'Travel' => '1', 'BLink priyojon' => '2'}
  TYPES = {'Paid' => 1, 'Gilt' => 2, 'Free Coupon' => 3, 'Free Gift' => 4}
  DEFAULT_WAITING_MESSAGE = "Oops! You are currently in the waiting queue. Please have patience with us as we check for stock availability. If there's a way we can serve you, we will get back to you shortly."
  @@params = nil
  belongs_to :merchant
  belongs_to :location
  belongs_to :parent, :class_name => "Deal"
  belongs_to :copyrighter, :class_name => 'User', :foreign_key => 'copyrighter_id'
  belongs_to :manager, :class_name => 'User', :foreign_key => 'manager_id'

  has_many :orders
  has_many :pictures
  has_many :products
  has_many :notifications
  has_many :payment_summaries, :class_name => 'PaymentSummary', :foreign_key => 'deal_id', :dependent => :nullify
  has_many :sub_deals, :class_name => "Deal", :foreign_key => "parent_id", :dependent => :nullify
  has_many :referrals, :as => :referable
  has_friendly_id :friendly_title, :use_slug => true, :scope => :location_id
  translates :title, :description, :background, :highlights, :conditions, :price_label, :pay_label, :save_label, :discount_label

  accepts_nested_attributes_for :pictures, :allow_destroy => true
  acts_as_audited

  validates_presence_of :title, :description, :merchant_id, :start_date, :end_date, :status,
                        :min_quantity, :max_quantity, :max_order_limit
  validates :actual_price, :discounted_price, :presence => true, :numericality => true
  validates_numericality_of :min_quantity, :max_quantity, :max_order_limit, :only_integer => true
  validates_numericality_of :max_coupon_quantity, :allow_nil => true, :greater_than => 0

  attr_accessor :order_count_report, :confirmed_count_report, :sold_count_report, :canceled_count_report, :redeemed_count_report

  scope :current, lambda { where("deals.end_date >= ? ", DateTime.now) }
  scope :live, lambda { where("? between deals.start_date and deals.end_date and status = 'approved'", DateTime.now) }
  scope :past, lambda { where("? > deals.end_date and status = 'approved'", DateTime.now) }
  scope :for, lambda { |location_name| {:joins => [:location], :conditions => {:locations => {:url => location_name}}} }
  scope :parent_deal, where("deals.parent_id is null")
  scope :featured, where(:is_featured => true)
  scope :rated, lambda { where("deals.rating is not null") }
  scope :not_in, lambda { |*ids| where(" deals.id not in (#{ids.join(', ')}) ") }
  scope :order_by_rating, order(" deals.rating asc ")
  scope :in_date_range, lambda { |start_date, end_date| where("deals.parent_id IS NULL AND deals.start_date between ? AND ? OR deals.end_date  between ? AND ? ", start_date, end_date, start_date, end_date) }
  scope :in_categories, lambda { |*cids| where(" deals.category_id in (#{cids.join(', ')}) ") }
  scope :child_collection, lambda { |deal_id| where("deals.parent_id = ? ", deal_id) }
  scope :for_merchant, lambda { |ids| where(" deals.merchant_id in (?) ", ids) }
  scope :latest_drop_down, lambda { where("deals.updated_at >= ?", (DateTime.now - 200)) }

  #HOOKS Start
  after_create :update_rating
  after_save :update_associated_items
  #HOOKS END

  def discounted_price(altered=false)
    return self[:discounted_price] if !altered
    if altered && self[:advance_payable]
      return self[:discounted_price_for_advance_payment]
    end
    return self[:discounted_price]
  end

  def friendly_title
    return self.title.to_s + "-"+ self.id.to_s
  end

  def self.set_params(params)
    @@params = params
  end

  def self.get_params
    @@params
  end

  def approved?
    return self.status == 'approved'
  end

  def pending?
    return (self.status.blank? || self.status == 'pending')
  end

  def active?
    return DateTime.now >= self.start_date && DateTime.now <= self.end_date && self.approved?
  end

  def past?
    return DateTime.now >= self.end_date && self.approved?
  end

  def sold_out?
    if self.products.any?
      self.products.each do |product|
        return false if !product.sold_out?
      end
      return true
    else
      return false if self.max_quantity <= 0
      return self.sold_count >= (self.max_quantity + self.wait_count)
    end
  end

  def booked?
    if self.products.any?
      self.products.each do |product|
        return false if !product.booked?
      end
      return true
    else
      return false if self.max_quantity <= 0
      return self.order_count >= (self.max_quantity + self.wait_count)
    end
  end

  def booked_without_wait_count?
    if self.products.any?
      self.products.each do |product|
        return false if !product.booked_without_wait_count?
      end
      return true
    else
      return false if self.max_quantity <= 0
      return self.order_count >= self.max_quantity
    end
  end

  # Returns deal title with campaign name concatenated
  def title_with_campaign_name
    return self.title.to_s + ' - ' + self.campaign_name.to_s if self.campaign_name.present?
    return self.title
  end

  def free?
    return self.type_id == 3 || self.type_id == 4
  end

  def free_coupon?
    return self.type_id == 3
  end

  def free_gift?
    return self.type_id == 4
  end

  def gilt?
    return self.type_id == 2
  end

  def to_param
    self.friendly_id
  end

  def running?
    return DateTime.now >= self.start_date && DateTime.now <= self.valid_to && self.approved?
  end

  def parent_deal?
    self.parent_id.nil?
  end

  def self.statuses
    return [:pending, :approved, :cancelled]
  end

  def booking_count
    self.orders.sum(:quantity).to_i
  end

  def sold_count
    self.orders.where(:is_notified => true).sum(:quantity).to_i
  end

  def order_count
    self.orders.sum(:quantity).to_i
  end

  def price_information(advance_payable=false)
    selected_deal = self
    selected_deal = self.sub_deals.first if self.sub_deals.any?
    selected_deal = self.products.first if self.gilt? && self.products.any?

    if advance_payable && selected_deal.advance_payable?
      discount = selected_deal.actual_price == 0 ? 100 : (100*(selected_deal.actual_price - selected_deal.discounted_price_for_advance_payment)/selected_deal.actual_price).round
      discounted_amount = selected_deal.actual_price - selected_deal.discounted_price_for_advance_payment
      price_info = {
          :original => selected_deal.actual_price, :pay => selected_deal.discounted_price_for_advance_payment,
          :discount => discount, :save => selected_deal.actual_price - selected_deal.discounted_price_for_advance_payment,
          :original_min => selected_deal.actual_price, :original_max => selected_deal.actual_price,
          :discount_min => discount,
          :pay_max => selected_deal.discounted_price_for_advance_payment, :pay_min => selected_deal.discounted_price_for_advance_payment,
          :save_max => discounted_amount, :save_min => discounted_amount
      }
    else
      price_info = {
          :original => selected_deal.actual_price, :pay => selected_deal.discounted_price,
          :discount => selected_deal.discount, :save => selected_deal.discount_amount,
          :original_min => selected_deal.actual_price, :original_max => selected_deal.actual_price,
          :discount_min => selected_deal.discount,
          :pay_max => selected_deal.discounted_price, :pay_min => selected_deal.discounted_price,
          :save_max => selected_deal.discount_amount, :save_min => selected_deal.discount_amount
      }
    end


    if self.gilt? && self.products.any?
      self.products.each do |product|
        if (advance_payable && product.advance_payable?)
          price_info = assign_price_data_for_advance_pay(price_info, product)
        else
          price_info = assign_price_data(price_info, product)
        end
      end
    elsif self.sub_deals.any?
      self.sub_deals.each do |deal|
        if (advance_payable && deal.advance_payable?)
          price_info = assign_price_data_for_advance_pay(price_info, deal)
        else
          price_info = assign_price_data(price_info, deal)
        end
      end
    end

    return price_info
  end

  def discount
    return 100 if self.actual_price == 0
    (100*(self.actual_price - self.discounted_price)/self.actual_price).round
  end

  def discount_for_advance_payment
    return 100 if self.actual_price == 0
    (100*(self.actual_price - self.discounted_price_for_advance_payment)/self.actual_price).round
  end

  def highest_discount
    highest_discount = self.discount
    if self.sub_deals.any?
      self.sub_deals.each do |deal|
        discount = deal.discount
        highest_discount = discount if highest_discount < discount
      end
    end
    return highest_discount
  end

  def discount_amount
    return (self.actual_price - self.discounted_price).to_i
  end

  def start_date_report
    return Deal::get_params["start_date(1i)"].present? ? DateTime.civil(Deal::get_params["start_date(1i)"].to_i, Deal::get_params["start_date(2i)"].to_i, Deal::get_params["start_date(3i)"].to_i, Deal::get_params[:"start_date(4i)"].to_i, Deal::get_params["start_date(5i)"].to_i) : DateTime.now
  end

  def end_date_report
    return Deal::get_params["end_date(1i)"].present? ? DateTime.civil(Deal::get_params["end_date(1i)"].to_i, Deal::get_params["end_date(2i)"].to_i, Deal::get_params["end_date(3i)"].to_i, Deal::get_params[:"end_date(4i)"].to_i, Deal::get_params["end_date(5i)"].to_i) : DateTime.now
  end

  def highest_discount_amount
    amount = self.discount_amount
    if self.sub_deals.any?
      self.sub_deals.each do |deal|
        discount = deal.discount_amount
        amount = discount if amount < discount
      end
    end
    return amount
  end

  def order_count_date_wise
    self.order_count_report = 0
    if !self.parent_deal? || (self.parent_deal? && !self.sub_deals.any?)
      self.order_count_report = self.orders.where("orders.booking_at between ? and ? ", start_date_report, end_date_report).sum(:quantity).to_i
    else
      self.sub_deals.each do |sub_deal|
        self.order_count_report += sub_deal.orders.where("orders.booking_at between ? and ? ", start_date_report, end_date_report).sum(:quantity).to_i
      end
    end
    puts self.order_count_report
    return self.order_count_report
  end

  def sold_count_date_wise
    sold_count_report = 0
    if !self.parent_deal? || (self.parent_deal? && !self.sub_deals.any?)
      sold_count_report = self.orders.where("orders.is_notified = 1 and orders.notified_at between ? and ? ", start_date_report, end_date_report).sum(:quantity).to_i
    else
      self.sub_deals.each do |sub_deal|
        sold_count_report += sub_deal.orders.where("orders.is_notified = 1 and orders.notified_at between ? and ? ", start_date_report, end_date_report).sum(:quantity).to_i
      end
    end
    return sold_count_report
  end

  def confirmed_count_date_wise
    confirmed_count_report = 0
    if !self.parent_deal? || (self.parent_deal? && !self.sub_deals.any?)
      confirmed_count_report = self.orders.where("orders.confirmed_at between ? and ? ", start_date_report, end_date_report).sum(:quantity).to_i
    else
      self.sub_deals.each do |sub_deal|
        confirmed_count_report += sub_deal.orders.where("orders.confirmed_at between ? and ? ", start_date_report, end_date_report).sum(:quantity).to_i
      end
    end
    return confirmed_count_report
  end

  def canceled_count_date_wise
    canceled_count_report = 0
    if !self.parent_deal? || (self.parent_deal? && !self.sub_deals.any?)
      canceled_count_report = self.orders.where("orders.canceled_at between ? and ? ", start_date_report, end_date_report).sum(:quantity).to_i
    else
      self.sub_deals.each do |sub_deal|
        canceled_count_report += sub_deal.orders.where("orders.canceled_at between ? and ? ", start_date_report, end_date_report).sum(:quantity).to_i
      end
    end
    return canceled_count_report
  end

  def redeemed_count_date_wise
    redeemed_count_report = 0
    if !self.parent_deal? || (self.parent_deal? && !self.sub_deals.any?)
      self.orders.each do |order|
        redeemed_count_report+= order.coupons.where("coupons.redeemed_by_user_id IS NOT NULL and coupons.created_at between ? and ? ", start_date_report, end_date_report).sum(:quantity).to_i
      end
    else
      self.sub_deals.each do |subdeal|
        subdeal.orders.each do |order|
          redeemed_count_report += order.coupons.where("coupons.redeemed_by_user_id IS NOT NULL and coupons.created_at between ? and ? ", start_date_report, end_date_report).sum(:quantity).to_i
        end
      end
    end
    return redeemed_count_report
  end

  def type_name
    return Deal::TYPES.key(self.type_id.to_i)
  end

  #
  # Returns true if current user is super admin(he can update a deal for all time)
  # Returns true if deal is pending
  # Returns false if deal is approved
  #
  def have_edit_access?(user)
    return (user.is?(:superadmin)||user.is?(:dealadmin) || self.pending?)
  end

  private
  def update_rating
    self.update_attribute(:rating, 0)
    Deal.update_all("rating = rating+1")
  end

  def update_associated_items()
    if self.gilt? && self.products.count()
      update_products()
    elsif self.sub_deals.any?
      update_sub_deals()
    end
  end

  def update_products()
    begin
      self.products.each do |product|
        product.update_attributes(
            :merchant_id => self.merchant_id,
            :start_date => self.start_date,
            :end_date => self.end_date,
            :status => self.status,
            :valid_from => self.valid_from,
            :valid_to => self.valid_to,
            :location_id => self.location_id
        )
      end
    rescue Exception => error
      logger.error "Error:: #{error.message}"
    end
  end

  def update_sub_deals()
    self.sub_deals.each do |sub_deal|
      sub_deal.update_attributes(
          :merchant_id => self.merchant_id,
          :start_date => self.start_date,
          :end_date => self.end_date,
          :status => self.status,
          :valid_from => self.valid_from,
          :valid_to => self.valid_to,
          :location_id => self.location_id,
          :highlights => self.highlights,
          :conditions => self.conditions,
          :is_featured => self.is_featured,
          :summary => self.summary,
          :background => self.background,
          :is_close => self.is_close,
          :type_id => self.type_id,
          :skip_notification => self.skip_notification,
          :is_show_logo => self.is_show_logo,
          :coupon_header => self.coupon_header,
          :is_show_value => self.is_show_value,
          :is_show_paid_amount => self.is_show_paid_amount,
          :social_media_sharing_content => self.social_media_sharing_content
      )
    end
  end

  def assign_price_data(price_info, item)
    if item.actual_price > price_info[:original_max]
      price_info[:original_max] = item.actual_price
    elsif item.discounted_price < price_info[:original_min]
      price_info[:original_min] = item.actual_price
    end

    if item.discounted_price > price_info[:pay_max]
      price_info[:pay_max] = item.discounted_price
    elsif item.discounted_price < price_info[:pay_min]
      price_info[:pay_min] = item.discounted_price
    end

    if item.discount < price_info[:discount_min]
      price_info[:discount_min] = item.discount
    end

    if item.discount_amount > price_info[:save_max]
      price_info[:save_max] = item.discount_amount
    elsif item.discount_amount < price_info[:save_min]
      price_info[:save_min] = item.discount_amount
    end

    if (item.discount > price_info[:discount] ||
        (item.discount == price_info[:discount] &&
            item.discounted_price > price_info[:pay]))
      price_info[:original] = item.actual_price
      price_info[:pay] = item.discounted_price
      price_info[:discount] = item.discount
      price_info[:save] = item.discount_amount
    end
    return price_info
  end

  def assign_price_data_for_advance_pay(price_info, item)

    discount = item.actual_price == 0 ? 100 : (100*(item.actual_price - item.discounted_price_for_advance_payment)/item.actual_price).round
    discount_amount = (item.actual_price - item.discounted_price_for_advance_payment).to_i

    if item.actual_price > price_info[:original_max]
      price_info[:original_max] = item.actual_price
    elsif item.discounted_price_for_advance_payment < price_info[:original_min]
      price_info[:original_min] = item.actual_price
    end

    if item.discounted_price_for_advance_payment > price_info[:pay_max]
      price_info[:pay_max] = item.discounted_price_for_advance_payment
    elsif item.discounted_price_for_advance_payment < price_info[:pay_min]
      price_info[:pay_min] = item.discounted_price_for_advance_payment
    end

    if discount < price_info[:discount_min]
      price_info[:discount_min] = discount
    end

    if discount_amount > price_info[:save_max]
      price_info[:save_max] = discount_amount
    elsif discount_amount < price_info[:save_min]
      price_info[:save_min] = discount_amount
    end

    if (discount > price_info[:discount] ||
        (discount == price_info[:discount] &&
            item.discounted_price_for_advance_payment > price_info[:pay]))
      price_info[:original] = item.actual_price
      price_info[:pay] = item.discounted_price_for_advance_payment
      price_info[:discount] = discount
      price_info[:save] = discount_amount
    end
    return price_info
  end

end
