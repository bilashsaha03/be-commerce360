class Product < ActiveRecord::Base
  belongs_to :deal
  belongs_to :category
  belongs_to :merchant
  belongs_to :location
  belongs_to :manager, :class_name => 'User', :foreign_key => 'manager_id'

  has_many :orders
  has_many :pictures
  has_many :product_sizes
  has_many :sizes, :through => :product_sizes

  validates_presence_of :name, :description, :start_date, :end_date, :location_id
  validates :quantity, :actual_price, :discounted_price, :presence => true, :numericality => true

  translates :name, :description, :highlights, :conditions
  acts_as_audited
  accepts_nested_attributes_for :pictures, :allow_destroy => true
  accepts_nested_attributes_for :product_sizes, :allow_destroy => true, :reject_if => proc { |attributes| attributes['size_id'].blank? || attributes['quantity'].blank? || attributes['quantity'].to_i==0 }

  scope :current, lambda { where("products.end_date >= ? ", DateTime.now) }
  scope :live, lambda { where("? between products.start_date and products.end_date and status = 'approved'", DateTime.now) }
  scope :for, lambda { |location_name| {:joins => [:location], :conditions => {:locations => {:url => location_name}}} }
  scope :not_deal_specific, lambda { where("products.deal_id = 0") }
  scope :for_category, lambda { |cat_id| where("products.category_id = ?" , cat_id) }
  scope :featured, lambda { where("products.is_featured = ? ", true) }
  scope :order_by_rating, order(" products.created_at desc ")
  scope :order_by_category, order(" products.category_id ASC ")
  scope :for_merchant, lambda { |ids| where(" products.merchant_id in (?) ",ids) }
  scope :latest_drop_down, lambda { where("products.updated_at >= ?" , (DateTime.now - 200)) }

  def approved?
    return self.status == 'approved'
  end

  def pending?
    return self.status == 'pending' || self.status.blank?
  end

  def active?
    return DateTime.now >= self.start_date && DateTime.now <= self.end_date && self.approved?
  end

  def enabled?
    return self.is_active
  end

  def past?
    return DateTime.now >= self.end_date && self.approved?
  end

  def sold_out?
    return true if self.quantity <= 0
    if self.product_sizes.present?
      self.product_sizes.each do |product_size|
        return false if self.sold_count(product_size.size_id) < (product_size.quantity + self.wait_count.to_i)
      end
      return true
    end
    return (self.sold_count >= self.quantity + self.wait_count) ? true : false
  end

  def sold_count (size_id=0)
    if (size_id != 0)
      return self.orders.where("is_notified = 1 AND size_id = #{size_id}").sum(:quantity).to_i
    end
    return self.orders.where(:is_notified => true).sum(:quantity).to_i
  end

  def order_count
    self.orders.sum(:quantity).to_i
  end



  def booked?
    return true if self.quantity <= 0
    if self.product_sizes.present?
      self.product_sizes.each do |size|
        return false if self.available?(size.size_id)
      end
      return true
    end
    return (self.booking_count >= self.quantity + self.wait_count) ? true : false
  end



  def booked_without_wait_count?
    return true if self.quantity <= 0
    if self.product_sizes.present?
      self.product_sizes.each do |size|
        return false if self.available_without_wait_count?(size.size_id)
      end
      return true
    end
    return (self.booking_count >= self.quantity) ? true : false
  end

  # Returns product name with campaign name concatenated
  def title_with_campaign_name
    return self.name.to_s + ' - ' + self.campaign_name.to_s if self.campaign_name.present?
    return self.name
  end

  def running?
    return DateTime.now >= self.start_date && DateTime.now <= self.valid_to && self.approved?
  end

  def available?(size_id)
    if self.product_sizes.find_by_size_id(size_id).present?
      return self.product_sizes.find_by_size_id(size_id).quantity.to_i + self.wait_count.to_i > (self.orders.for_size(size_id).sum(:quantity).to_i)
    end
    return false
  end

  def available_without_wait_count?(size_id)
    if self.product_sizes.find_by_size_id(size_id).present?
      return self.product_sizes.find_by_size_id(size_id).quantity > (self.orders.for_size(size_id).sum(:quantity).to_i)
    end
    return false
  end


  def show_buy_button?
    if self.sizes.present?
      self.sizes.each do |size|
        available = self.available?(size.id);
      end
    else
      available = self.quantity > (self.orders.sum(:quantity).to_i)
    end
  end

  def booking_count
    self.orders.sum(:quantity).to_i
  end


  def merchants_product? (current_user)
    return self.merchant.users.include?(current_user)
  end

  def discount
    return 100 if self.actual_price == 0
    (100*(self.actual_price - self.discounted_price)/self.actual_price).round
  end

  def discount_for_advance
    return 100 if self.actual_price == 0
    (100*(self.actual_price - self.discounted_price_for_advance_payment)/self.actual_price).round
  end

  def discount_amount
    return (self.actual_price - self.discounted_price).to_i
  end

  def self.statuses
    return [:pending, :approved, :cancelled]
  end

  #
  # Returns true if current user is super admin(he can update a deal for all time)
  # Returns true if deal is pending
  # Returns false if deal is approved
  #
  def have_edit_access?(user)
     return true
     return (user.is?(:superadmin)||user.is?(:dealadmin) || self.pending?)
  end

end
