class PaymentSummary < ActiveRecord::Base
  acts_as_audited
  belongs_to :collector, :class_name => 'User', :foreign_key => 'collector_id'
  belongs_to :deal, :class_name => 'Deal', :foreign_key => 'deal_id'
  has_many :payment_receives, :class_name => 'PaymentReceive', :foreign_key => 'summary_id', :dependent => :nullify

  def collector_name()
    return User.collector_name(self.collector)
  end

  def self.auto_generate
    collected_amount = 0
    @deals = Deal.parent_deal
    @users = User.where("roles_mask >= ?", 8)
    @users.each do |user|
      @deals.each do |deal|
        payment_summary = PaymentSummary.new
        payment_summary.collector= user
        payment_summary.deal = deal
        collected_amount=0
        payment_summary.collection_amount = 0
        payment_summary.due_amount = 0
        user.collected_payments.each do |collected_payment|
          if (deal == collected_payment.order.deal)
            collected_amount = collected_amount + collected_payment.amount
          end
          payment_summary.collection_amount = collected_amount
          payment_summary.due_amount= collected_amount
        end

        if (deal.sub_deals.size > 0)
          deal.sub_deals.each do |sub|
            user.collected_payments.each do |payment|
              if (sub == payment.order.deal)
                collected_amount = collected_amount + payment.amount
              end
              payment_summary.collection_amount = collected_amount
              payment_summary.due_amount= collected_amount
            end
          end
        end

#        @payment_summary.due_amount = 0

        if (payment_summary.save!)
        end

      end

    end

  end

end
