class PollOption < ActiveRecord::Base
  belongs_to :poll

  def self.create_vote(params, user)
    params[:poll_options].each do |poll_id, option_id|
      user_poll = UserPoll.new(:user_id => user.id, :poll_id => poll_id, :option_id => option_id)
      if user_poll.save
          poll_option = PollOption.find(option_id)
          poll_option.vote_count =  poll_option.vote_count + 1
          return poll_option.save
      end
    end
    return false
  end
end
