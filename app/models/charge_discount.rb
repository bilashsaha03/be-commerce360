class ChargeDiscount  < ActiveRecord::Base
  TYPES = {:PROMOTIONAL_DISCOUNT => "PROMOTIONAL_DISCOUNT", :CORPORATE_DISCOUNT => "CORPORATE_DISCOUNT"}

  belongs_to :order
end