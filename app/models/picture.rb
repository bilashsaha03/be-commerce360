class Picture < ActiveRecord::Base
  belongs_to :deal
  belongs_to :merchant
  belongs_to :notice
  belongs_to :product

  has_attached_file :data,
                    :styles => {
                        :tiny => "40x40>",
                        :thumb => "100x100>",
                        :small => "120x61>",
                        :product => "383x240>",
                        :product_thumb => "160x110>",
                        :product_medium => "200x125>",
                        :product_large => "574x360",
                        :medium => "450x230>",
                        :large => "685x350>",
                        :travel => "300x153>",
                    },
                    :url => PAPERCLIP_CONFIG["url"],
                    :path => PAPERCLIP_CONFIG["path"],
                    :default_url => PAPERCLIP_CONFIG["default_url"]

  validates_attachment_size :data, :less_than => 5.megabytes
  validates_attachment_content_type :data, :content_type => ['image/jpeg', 'image/gif', 'image/png']
  validates_attachment_presence :data
end
