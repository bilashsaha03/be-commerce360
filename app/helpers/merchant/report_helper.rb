module Merchant::ReportHelper
  def link_to_coupons_page(merchant)
    return merchant.name if (merchant.deals.present? && merchant.deals.size == 0) ||(merchant.products.present? && merchant.products.size == 0 )
    return link_to merchant.name, merchant_coupons_url(:id => merchant)

  end
end
