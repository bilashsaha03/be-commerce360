module Merchant::ProductsHelper
  def show_product_sizes(product)
    sizes = Array.new
    product.sizes.each do |size|
      sizes << size.name
    end
    return sizes.join(', ')
  end
end
