module MetaHelper
  def generate_meta_data(title, request_url)
    meta_content = Hash.new
    meta_content[:title] = meta_title(title)
    meta_content[:keywords] = meta_keywords()
    meta_content[:description] = meta_description(title)
    meta_content[:social_media_image] = meta_image_for_social_media()
    meta_content[:social_media_title] = meta_title_for_social_media(meta_content[:title])
    meta_content[:social_media_description] = meta_content[:description]
    meta_content[:request_url] = meta_request_url(request_url)
    return meta_content
  end

  def meta_request_url(request_url)
    if @deal.present? && current_user.present?
      referral_code = Referral.create_referral(current_user).code
      request_url = "#{request_url}?ref=#{referral_code}"
    end

    return request_url
  end

  def meta_title(title)
    if @deal.present?
      title = "#{@deal.title} on Akhoni - #{@deal.location.name} "
    elsif !show_title?
      title = SITE_CONFIG['meta']['title'].gsub(/#LOCATION#/, session[:location_id].to_s.capitalize)
    end

    return title
  end

  def meta_title_for_social_media(title)
    if @deal.present?
      if @deal.social_media_sharing_content.present?
        title = @deal.social_media_sharing_content
      else
        title = ''
        unless (strip_tags(@deal.description.to_s.strip)).match(/\d+%/)
          title = "Upto " if @deal.sub_deals.any?
          title << "#{@deal.highest_discount}% Discount on "
        end
        title << "#{@deal.description} "
        title << "at #{@deal.title}"
      end
    end
    return title
  end

  def meta_keywords()
    keywords = "#{SITE_CONFIG['meta']['keywords']}, #{session[:location_id]}"
    keywords << ", #{@deal.merchant.name}, #{@deal.title}, #{@deal.description}" if @deal.present?
    if @deals.present?
      titles = @deals.map { |deal| deal.title }
      keywords << ", #{titles.join(', ')}"
    end
    return keywords.gsub(/#LOCATION#/, session[:location_id].to_s.capitalize)
  end

  def meta_description(default_title)
    return "#{truncate(strip_tags(@deal.background), :length => 150, :separator => ' ')}".strip if @deal.present?
    return show_title? ? default_title : SITE_CONFIG['meta']['description'].gsub(/#LOCATION#/, session[:location_id].to_s.capitalize)
  end

  def meta_image_for_social_media()
    return root_url + "images/#{@deal.pictures.first.data.url(:large)}" if @deal.present? && @deal.pictures.first.present?
    return root_url + "images/#{SITE_CONFIG['logo']}"
  end
end