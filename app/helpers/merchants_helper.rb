module MerchantsHelper
  def render_featured_merchants
    merchants = Merchant.featured
    if merchants.any?
      render :partial => "merchants/featured_merchants", :locals => {:merchants => merchants}
    end
  end
end
