module ApplicationHelper
  def link_to_tag_current(name, tag, options={}, html_options={}, &block)
    content = link_to_unless_current(name, options, html_options, &block)
    current_page?(options) ? content_tag(tag, content) : content
  end

  def image_slider(obj, image_style, width = 685, height = 350, klass = 'deal',limit=nil)
    width = width || 685
    height = height || 350
    html = ' '
    if limit.nil?
      images = obj.pictures
    else
      images = obj.pictures[0..0]
    end



    if images.size == 0
      html = missing_image('', image_style)
    elsif images.size == 1
      html = image_tag('/images/featured-deal.jpg', :data_src => images.first.data.url(image_style), :width => width, :height => height, :class => klass)
    else
      ele_id = "slider#{obj.id}"

      html = ' <div class="wrapper">'
      html += ' <div class="slider-wrapper">'
      html += '  <div id="'+ele_id+'" class="slider nivoSlider">'

      images.each do |image|

        html += image_tag('/images/featured-deal.jpg', :data_src => image.data.url(image_style), :class => klass, :style => "width:#{width}px;height:#{height}px;")
      end

      html += '  </div> '
      html += ' </div> '
      html += '</div>'

      html += javascript_tag "initialize_image_slider('#{ele_id}');"

    end

    return html.html_safe
  end

  def show_picture(item, image_style, has_many = true)
    picture = has_many ? item.pictures.first : item.picture
    return missing_image('', image_style) if picture.nil?
    return image_tag(picture.data.url(image_style), :class => image_style.to_s)
  end

  def show_image_or_nothing(image, image_style)
    return nil if image.nil?
    return image_tag(image.data.url(image_style), :class => image_style.to_s)
  end

  def missing_image(image_title, image_style)
    return image_tag("pictures/#{image_style.to_s.downcase}_missing.png", :class => "#{image_style.to_s}_missing", :title => image_title)
  end

  def show_date(date, format = 'd')
    format = (format == 'dt') ? :long : :short
    return date.present? ? l(date.to_date, :format => :short).to_s.to_bddigit : ""
  end

  def link_to_print_coupon(coupon, title = '', options = {})
    return "" if !coupon.order.confirmed?
    coupon_path = coupon.order.deal.present? ? location_deal_order_print_coupon_path(coupon.order.deal.location, coupon.order.deal, coupon.order, {:id => coupon.id, :print => (options[:print].present? ? options[:print] : false)})
    : location_product_order_print_coupon_path(coupon.order.product.location, coupon.order.product, coupon.order, {:id => coupon.id, :print => (options[:print].present? ? options[:print] : false)})
    if title.present?
      coupon_path = link_to(title, coupon_path, :id => "coupon#{coupon.id}", :title => "Click here to open the coupon", :class => options[:class])
    end
    return coupon_path
  end

  def show_icon(ico)
    name = ''
    case ico
      when true
        name = 'tick'
      when false
        name = 'close'
    end
    return image_tag("#{name}_icon.png", :alt => name)
  end

  def collection_select_multiple(object, method, collection, value_method, text_method, options = {}, html_options = {})
    real_method = "#{method.to_s.singularize}_ids".to_sym
    collection_select(
        object, real_method,
        collection, value_method, text_method,
        options,
        html_options.merge({
                               :multiple => true,
                               :name => "#{object}[#{real_method}][]"
                           })
    )
  end

  def show_history(id, name, icon_css = nil)
    html = ' <span class="lightbox">'
    html += ' | ' if icon_css.nil?
    html += link_to('History', admin_audit_path(id, {:format => "#{name}"}), :class => "iframe #{icon_css}")
    html += '</span>'
    return html.html_safe
  end

  def show_referral_link(current_user, path = nil)
    referral = Referral.create_referral(current_user)
    return "#{path.nil? ? root_url : path}?ref=#{referral.code}"
  end

  def date_filter_enabled?()
    return params[:select_date].present? ? true : false
  end

  def show_gender(user)
    return '' if (user.gender.nil? || user.gender.blank?)
    return user.gender == "M" ? 'Male' : 'Female'
  end

  def bought_orders_count(user)
    return user.orders.where(:is_notified => true).count
  end

  def collector_options_using_email()
    collectors = User.with_role(:superadmin)
    return [] if collectors.nil?
    collectors.collect { |c| [(User.collector_name(c)), c.id] }
  end

  def can_view_orders?
    if current_user.merchants.any?
      current_user.merchants.each do |merchant|
        if merchant.is_enable_orders_view?
          return true
        end
      end
    end
    return false
  end
end


