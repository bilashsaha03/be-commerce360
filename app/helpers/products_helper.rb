module ProductsHelper
  def show_product_details_link(product, title = '', kclass= '')
    title = title.present? ? title : product.name
    if params[:deal_id].present?
      return link_to(title.to_s.html_safe, location_deal_product_path(product.deal.location, product.deal, product), :class => kclass)
    elsif params[:merchant_id].present?
      return link_to(title.to_s.html_safe, location_deal_product_path(product.location, product.deal, product), :class => kclass)
    else
      return link_to(title.to_s.html_safe, location_product_path(product.location, product), :class => kclass)
    end
  end

  def show_product_buy_link(product)
    sold_out_link = link_to(t('SOLD_OUT'), "javascript:void(0);", {:class => 'buy_now_btn ', :style=>'background: url("/images/buy_button_disabled.png") no-repeat;'})
    if !product.enabled? || product.booked?
      return sold_out_link
    end

    if product.active? || (product.pending? && current_user.present? && current_user.admin?)
      if product.sizes.any?
        if product.deal.present?
          return link_to t('BUY_NOW'), "javascript:void(0);", :class => 'buy_now_btn buy_gilt_item', :link => "#{new_location_deal_product_order_path(product.deal.location, product.deal, product)}"
        else
          return link_to t('BUY_NOW'), "javascript:void(0);", :class => 'buy_now_btn buy_gilt_item', :link => "#{new_location_product_order_path(product.location, product)}"
        end
      else
        if product.deal.present?
          return link_to(t('BUY_NOW'), new_location_deal_product_order_path(product.deal.location, product.deal, product), :class => 'buy_now_btn')
        else
          return link_to(t('BUY_NOW'), new_location_product_order_path(product.location, product), :class => 'buy_now_btn')
        end
      end
    else
      return link_to(t('NOTAVAILABLE'), "javascript:void(0);", :class => 'buy_now_btn closed')
    end
  end

  def show_product_buy_now_friend_button(product)
    if product.booked_without_wait_count?
      return ''
    end
    if product.active? || (product.pending? && current_user.present? && current_user.admin?)
      if product.sizes.any?
        if product.deal.present?
          return link_to t('BUY_FOR_FRIEND'), "javascript:void(0);", :class => 'friend_button buy-product-for-friend', :link => "#{new_location_deal_product_order_path(product.deal.location, product.deal, product, :gift => '1')} "
        else
          return link_to t('BUY_FOR_FRIEND'), "javascript:void(0);", :class => 'friend_button buy-product-for-friend', :link => "#{new_location_product_order_path(product.location, product, product, :gift => '1')}"
        end
      else
        if product.deal.present?
          return link_to(t('BUY_FOR_FRIEND'), new_location_deal_product_order_path(product.deal.location, product.deal, product, :gift => '1'), :class => 'buy-product-for-friend')
        else
          return link_to(t('BUY_FOR_FRIEND'), new_location_product_order_path(product.location, product, product, :gift => '1'), :class => 'buy-product-for-friend')
        end
      end
    else
      return ''
    end
  end

  def render_featured_products
    @products = Product.order_by_category.live.order_by_rating.featured.for(params[:location_id]).not_deal_specific()
    render :partial => "products/featured_products", :locals => {:products => @products}
  end

  def render_deal_products
      @deal = Deal.find(params[:deal_id], :include => [:products])
      if (@deal.pending? && (!current_user.present? || !current_user.admin?))
        page_not_found(@deal)
      end
      @products = @deal.products.order("created_at desc")
      render :partial => "products/products"
  end

  def render_all_products
    @products = Product.order_by_category.live.order_by_rating.for(params[:location_id]).not_deal_specific().order("category_id ASC")
    render :partial => "products/products"
  end

  def render_categorized_products
    @products = Product.live.order_by_rating.for(params[:location_id]).for_category(params[:cat_id].to_i).not_deal_specific()
    render :partial => "products/products"
  end


end
