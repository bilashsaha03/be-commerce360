module Manage::NotificationsHelper
  def link_to_receiver(notification_log)
    return notification_log.receiver_email if notification_log.receiver.nil? || cannot?(:show, :"manage/users")
    return link_to(notification_log.receiver_email, admin_user_path(notification_log.receiver))
  end

  def show_notification_actions(notification)
    html = ''
    html += link_to('Preview', preview_admin_notification_path(notification), :target => '_blank')
    if notification.draft?
      html += ' | ' + link_to('Edit', edit_admin_notification_path(notification))
      html += ' | ' + link_to('Test', test_admin_notification_path(notification))
      html += "<br />"
      html += link_to('Assign Users', assign_users_admin_notification_path(notification))
      html += ' | ' + link_to('Schedule', schedule_admin_notification_path(notification), :confirm => "You will not be able to change anything after schedule the notification. \nAre you sure, You want to continue?")
    end
    return html.html_safe
  end
end
