module Manage::ProductsHelper
  def show_product_sizes(product)
    sizes = Array.new
    product.sizes.each do |size|
      sizes << size.name
    end
    return sizes.join(', ')
  end

  def show_merchant_name (product)
    product.deal.present? ? product.deal.merchant.name : product.merchant.name
  end

    def show_product_deal_name (product)
    product.deal.present? ? product.deal.title : 'Individual Project'
  end

end
