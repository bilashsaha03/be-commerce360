module Manage::CouponsHelper
  def link_to_coupon_delete(coupon)
    return "" if coupon.order.paid? || coupon.order.coupons.size == 1
    return link_to 'Delete', admin_coupon_path(coupon),
                   :confirm => 'Are you sure, you want to delete the coupon?',
                   :method => :delete, :remote => true, :class => 'icon trash'
  end
end
