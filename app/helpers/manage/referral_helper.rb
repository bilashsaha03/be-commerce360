module Manage::ReferralHelper
  def medium_options
    return Referral.mediums.collect { |p|
      [p, p]
    }
  end

  def show_referable_link referable_id, referable_type
    html ='';
    puts referable_type
    if referable_type=="Deal"
      deal = Deal.find(referable_id)
      if deal.present?
        html= html + link_to(h(deal.title), location_deal_path(deal.location, deal))
      end
    elsif referable_type=='Product'
      product = Product.find(referable_id)
      html= html + link_to(h(product.title), location_deal_path(product.location, product))
    elsif referable_type==nil
      html= html + link_to('Akhoni',root_path)
    end
    return html.html_safe
  end


  def referable_type_options

    return Referral.referable_types.collect { |p|

      #if p=='Website'
      #  puts 'OKK'
      #end
      [p, p]
    }
  end

end
