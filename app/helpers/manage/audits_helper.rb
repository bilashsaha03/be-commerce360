module Manage::AuditsHelper
  def auditable_type_options()
    sources = Audit.select('distinct auditable_type')
    return [] if sources.nil?
    sources.collect{ |a| [a.auditable_type, a.auditable_type]}
  end
  def action_options()
    actions = Audit.select('distinct action')
    return [] if actions.nil?
    actions.collect{ |a| [a.action, a.action]}
  end
end