module Manage::CorporateDiscountsHelper
  def item_type_options()
    sources = CorporateDiscount.select('distinct item_type')
    return [] if sources.nil?
    sources.collect{ |i| [i.item_type, i.item_type]}
  end

end