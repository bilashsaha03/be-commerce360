module Manage::OrdersHelper
  def show_status(order)
    if order.paid?
      if order.referral_credit.to_f > 0.0
        html = "<b>Paid</b> by #{order.payment_type + ' and Referral Credit'}".html_safe
      else
        html = "<b>Paid</b> by #{order.payment_type}".html_safe
      end
      if order.latest_payment.collector.present?
        html += "<br />Collected by #{order.latest_payment.collector.name_or_email}".html_safe
      end
      return html
    elsif order.cancelled?
      return "Cancelled by #{order.canceled_user.name_or_email}"
    elsif order.confirmed? && order.user_id != order.confirmed_by
      return "Confirmed by #{order.confirmed_user.name_or_email if order.confirmed_user}"
    end
  end

  def show_order_actions(order)
    link_html = ""
    link_html += can?(:comment, :"manage/orders") ? link_to('Info', "javascript:void(0);", :class => 'icon comment add_comment', :id => "comment_#{order.id}", :order_id => order.id, :title => 'Delivery Info') : ''

    #if can?(:comment, :"manage/orders")
    if order.confirmed? && !order.cancelled?
      link_html += link_to('Coupon', location_deal_order_coupon_path(order.deal.location, order.deal, order), :class => 'icon coupon', :id => "coupon_#{order.id}", :title => 'See Coupon') if order.deal.present?
      link_html += link_to('Coupon', location_product_order_coupon_path(order.product.location, order.product, order), :class => 'icon coupon', :id => "coupon_#{order.id}", :title => 'See Coupon') if !order.deal.present? && order.product.present?
    end
    if order.deal.present? && order.deal.running? || order.product.present? && order.product.running?
      if !order.confirmed?
        if can?(:confirm, :"manage/orders")
          link_html += link_to('Confirmed', admin_order_confirm_path(order), :method => :post, :class => 'icon confirm', :id => "confirm_#{order.id}", :title => 'Confirm Order')
        end
      end

      if order.paid?
        if order.latest_payment.cash? && !order.redeemed?
          if can?(:cancel, :"manage/orders")
            link_html += link_to('Cancel', admin_order_cancel_path(order), :method => :post, :confirm => 'Are you sure, you want to cancel the order permanently?', :class => 'icon cancel', :id => "cancel#{order.id}", :title => 'Cancel the Order')
          end
        end
      elsif !(order.cancelled? || (order.deal.present? && order.deal.free_gift?))
        if can?(:paid, :"manage/orders")
          link_html += link_to('Delivered', admin_order_paid_path(order), :method => :post, :confirm => 'Are you sure, you want to mark the order as paid?', :class => 'icon paid', :id => "paid#{order.id}", :title => 'Mark as Paid')
        end
      end
    end
    link_html.html_safe
  end

  def deal_options
    deals = Array.new
    Deal.latest_drop_down.each do |d|
      if d.parent_deal?
        deals << [d.title_with_campaign_name.to_s.html_safe, d.id]
        Deal.child_collection(d.id).each do |c|
          deals << ["&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#{c.title_with_campaign_name}".html_safe, c.id]
        end
      end
    end
    return deals
  end


  def product_options
    return Product.latest_drop_down.collect { |p|
      [p.title_with_campaign_name, p.id]
    }
  end


  def merchant_specific_deal_options
    ids = current_user.merchants.collect { |p| p.id }
    deals = Array.new
    Deal.for_merchant(ids).each do |d|
      if d.parent_deal?
        deals << [d.title_with_campaign_name.to_s.html_safe, d.id]
        Deal.child_collection(d.id).each do |c|
          deals << ["&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#{c.title_with_campaign_name}".html_safe, c.id]
        end
      end
    end
    return deals
  end


  def merchant_specific_product_options
    ids = current_user.merchants.collect { |p| p.id }
    return Product.for_merchant(ids).all.collect { |p|
      [p.title_with_campaign_name, p.id]
    }
  end


  def render_shipping_address(order)
    if order.shipping_detail.present?
      return order.shipping_detail.address.to_s
    elsif order.gift.present?
      return order.gift.address.to_s
    else
      return order.user.address.to_s
    end
    return order.user.address.to_s
  end


  def show_order_status_for_merchant(order)
    if order.paid?
      if order.referral_credit.to_f > 0.0
        html = "<b>Paid</b> by #{order.payment_type + ' and Referral Credit'}".html_safe
      else
        html = "<b>Paid</b> by #{order.payment_type}".html_safe
      end
      return html
    elsif order.cancelled?
      return "Cancelled"
    elsif order.confirmed?
      return "Confirmed"
    end
  end


  def show_deal_or_product_details(order)
    html = ''
    if order.deal.present?
      if order.deal.parent_deal?
        html = "<strong> Deal: </strong> #{order.deal.title} <br/>"
      else
        html = "<strong> Deal: </strong> #{order.deal.parent.title} <br/> <strong> Sub-Deal:</strong> #{order.deal.title}"
      end
    end
    if order.product.present?
      html = html+"<strong> Product: </strong> #{order.product.name} <br/>"
    end
    if order.size.present?
      html = html+"<strong> Size: </strong> #{order.size.name} <br/>"
    end
    return html.html_safe
  end


end
