module DealsHelper
  def show_price_label(deal, price_info)
    if deal.price_label.present?
      html = deal.price_label
      html = html.sub("__", "#{deal.discounted_price.to_i}".to_bddigit)
      html = html.sub("??", "#{deal.actual_price.to_i}".to_bddigit)
    else
      upto = (price_info[:original] > price_info[:original_min] &&
          price_info[:original] == price_info[:original_max])
      html = "#{t("ORIGINAL_PRICE")} #{t('UPTO') if upto} " if I18n.locale == :en
      html = "#{t("ORIGINAL_PRICE")} " if I18n.locale == :bn
      html += "<span class=\"taka\">#{price_info[:original].to_i.to_bddigit}</span> #{t("TAKA")}"
      html += " #{t('UPTO') if upto}" if I18n.locale == :bn
    end
    return html.html_safe
  end

  def show_pay(deal, price_info, page = 'show')
    html = ''
    if deal.pay_label.present?
      pay_label=deal.pay_label.split(" ")
      if (page == 'multiple_deal')
        html = deal.pay_label.downcase.to_bddigit
        html = html.gsub("#{t('PAY').downcase}", "#{t("PAY")}<br /><span>")
        html = html.gsub(/upto/, "<sup class=\"upto-tag\">upto</sup>")
        html = html.gsub(/tk./, "<small style=\"font-size: 10px;\">Tk.</small></span>")
        html = html.gsub(/taka/, "<small style=\"font-size: 10px;\">Tk.</small></span>")
        html = html.gsub('pay', t('PAY'))
      elsif (page == 'newsletter')
        html = deal.pay_label.downcase
        html = html.gsub(/pay/, "Pay<br />")
        price = deal.pay_label.gsub(/[^0-9]/, '').to_i
        price_tag = "<span style=\"font-family: 'HelveticaRoundedLTBoldCnRg',Arial,Helvetica,sans-serif; color: rgb(255, 0, 96); font-size: 30px; letter-spacing: -1px; margin-bottom: 15px;\">#{price}</span>"
        price_tag = "#{price_tag}<br />"
        html = html.gsub(/#{price}/, price_tag)
      else
        html = deal.pay_label.gsub(/[^0-9]/, '').to_i.to_bddigit.to_s
        html += "<span style=\"font-size: 14pt\"> Tk. </span>"
      end
    else
      upto = (price_info[:pay] > price_info[:pay_min] && price_info[:pay] == price_info[:pay_max])
      if (page == 'multiple_deal')
        html = "#{t('PAY')}<br /><span>"
        html += '<sup class="upto-tag">upto</sup>' if upto
        html += "#{price_info[:pay].to_i} ".to_bddigit
        html +='<small style="font-size: 10px;">Tk.</small></span>'
      elsif (page == 'newsletter')
        html += "Pay "
        html += 'Upto' if upto
        html += "<br /><span style=\"font-family: 'HelveticaRoundedLTBoldCnRg',Arial,Helvetica,sans-serif; color: rgb(255, 0, 96); font-size: 30px; letter-spacing: -1px; margin-bottom: 15px;\">#{price_info[:pay].to_i}</span> "
        html +='<br/>Taka'
      else
        html = "#{price_info[:pay].to_i}".to_bddigit
        html += "<span style='font-size: 14pt'> #{t("TAKA")}</span>"
      end
    end
    return html.html_safe
  end

  def show_discount(deal, price_info, page = 'show')
    if deal.discount_label.present?
      discount_label=deal.discount_label.split(" ")
      if (page == 'multiple_deal')
        html= "#{t('DISCOUNT')}<br/><span>"
        discount_label.each do |d|
          if d.downcase == "upto"
            html += '<sup class="upto-tag">upto</sup>'
          elsif d.downcase=="discount"
          else
            html += d.to_bddigit
          end
          html += ' '
        end
        html += '</span>'
      elsif (page == 'past')
        html = deal.discount_label.to_bddigit
      else
        saving_upto = false
        discount_label.each do |d|
          if d.downcase=="upto"
            saving_upto=true
          end
        end
        html = " <div class=\"saving #{'upto' if saving_upto}\"> "
        discount_label.each do |d|
          if d.downcase=="upto"
            html += ' <p>Upto</p> '
          elsif d.downcase == 'discount'
          else
            html += ' <p class="save"> '
            html +=d.to_bddigit
          end
          html +=' '
        end
        html += ' </p>'
        html += " <p>#{t('SAVINGS')}</p> "
        html += '</div>'
      end
    else
      upto = (price_info[:discount] > price_info[:discount_min])

      if (page == 'multiple_deal')
        html= "#{t('DISCOUNT')}<br/><span>"
        html += '<sup class="upto-tag">upto</sup>' if upto
        html += "  #{price_info[:discount].to_i}%".to_bddigit
        html += '</span>'
      elsif (page == 'past')
        html = "#{'upto' if upto} #{price_info[:discount].to_i}%".to_bddigit
      else
        html = " <div class=\"saving #{'upto' if upto}\"> "
        html += ' <p>Upto</p> ' if upto
        html += ' <p class="save"> '
        html += "  #{price_info[:discount].to_i}%".to_bddigit
        html += ' </p>'
        html += " <p>#{t('SAVINGS')}</p> "
        html += '</div>'
      end
    end
    return html.html_safe
  end

  def show_save(deal, price_info, page = 'show')
    if deal.save_label.present?
      save_label=deal.save_label.split(" ")
      html = "#{t('SAVE')}<br><span>"
      save_label.each do |s|
        if s.downcase=="upto"
          html += '<sup class="upto-tag">upto</sup>'
        elsif s.downcase=="save"
        elsif s.downcase=="tk." || s.downcase=="taka"
          html +="<small style=\"font-size: 10px;\">#{s}</small></span>"
        else
          html +=s.to_bddigit
        end
        html +=' '
      end
      html = deal.save_label.to_bddigit if page == 'past'
    else
      upto = (price_info[:save] > price_info[:save_min] &&
          price_info[:save] == price_info[:save_max])

      html = "#{t('SAVE')}<br><span>"
      html += '<sup class="upto-tag">upto</sup>' if upto
      html += " #{price_info[:save].to_i}".to_bddigit
      html += '<small style="font-size: 10px;">Tk.</small></span>'
      html = "#{'upto' if upto} #{price_info[:save].to_i.to_s.to_bddigit} Tk." if page == 'past'
    end
    return html.html_safe
  end

  def show_time_counter(deal, details = false)
    html = "<h3>";

    if deal.active?
      html += details ? t("TIME_LEFT_TO_BUY_COUPON_TWO_LINE") : t("TIME_LEFT_TO_BUY_COUPON_ONE_LINE")
      if (deal.gilt? || deal.free_coupon? || deal.free_gift?) and deal.booked?
        html += "<span id=\"closed\" class=\"sold_out\">00:00:00</span>";
      else
        html += "<span id=\"time_remaining_counter#{deal.id}\">#{image_tag("loading.gif", :size => "160x33")}</span>";
      end
    elsif deal.pending?
      html += "<span id=\"time_remaining_counter#{deal.id}\" class=\"closed\">SCHEDULED ON</span>";
      html += "<p class=\"closed\">#{show_date(deal.start_date)}</p>";
    else
      html += "<span id=\"time_remaining_counter#{deal.id}\" class=\"closed\">#{t('CLOSED_ON')}</span>";
      html += "<p class=\"closed\">#{show_date(deal.end_date)}</p>";
    end
    html += "</h3>";

    return html.html_safe
  end

  def show_avail_date(deal, details = false)
    html = "<h3>";
    html += details ? t("TIME_LEFT_TO_USE_COUPON_TWO_LINE") : t("TIME_LEFT_TO_USE_COUPON_ONE_LINE")
    html += "<span>#{show_date(deal.valid_to)}</span>";
    html += "</h3>";
    return html.html_safe
  end

  def show_buy_now_friend_button(deal)
    if (deal.active? || (deal.pending? && current_user.present? && current_user.admin?)) &&
        !deal.free_coupon? && !deal.free_gift? && !deal.sub_deals.any? && !deal.sold_out? && deal.is_pay_by_card?
      if !(deal.gilt? && (deal.products.present? || deal.booked_without_wait_count?))
        return ("<img src=\"/images/purple-gift-box1.jpg\" height=\"24px\" width=\"30px\"/>" + link_to(t("BUY_FOR_FRIEND"), new_location_deal_order_path(deal.location, deal, :gift => '1'), :class => 'buy-now-friend')).html_safe
      end
    end
  end

  def show_buy_now_button(deal)
    deal_over_link = link_to(t('DEAL_OVER'), "javascript:void(0);", :class => 'button closed')
    sold_out_link = link_to(t('SOLD_OUT'), "javascript:void(0);", :class => 'button closed')
    sub_deal_page_link = link_to(t('VIEW_DETAILS_AND_BUY'), deals_location_deal_path(deal.location, deal), :class => 'button')
    buy_now_link = link_to(t('BUY_NOW'), new_location_deal_order_path(deal.location, deal), :class => 'button')
    free_coupon_link = link_to(t('GET_FREE_COUPON'), new_location_deal_order_path(deal.location, deal), :class => 'button')
    free_gift_link = link_to(t('FREE_GIFT'), new_location_deal_order_path(deal.location, deal), :class => 'button')

    if deal.active? || (deal.pending? && current_user.present? && current_user.admin?)
      if deal.free_coupon?
        return deal_over_link if deal.booked?
        return free_coupon_link
      elsif deal.free_gift?
        return deal_over_link if deal.booked?
        return free_gift_link
      elsif deal.gilt?
        return sold_out_link if deal.booked?
        return link_to(t('VIEW_DETAILS_AND_BUY'), location_deal_products_path(deal.location, deal), :class => 'button') if deal.products.any?
        return sub_deal_page_link if deal.sub_deals.any?
        return buy_now_link
      elsif deal.sub_deals.any?
        return sub_deal_page_link
      else
        return buy_now_link
      end
    else
      return deal_over_link
    end
  end


  def show_multiple_deal_button (deal)
    target = params[:action] == 'index_for_facebook' ? '_blank' : '_self'
    if deal.active?
      if (deal.gilt? || deal.free_coupon? || deal.free_gift?) and deal.booked?
        return link_to t('VIEW_DETAILS'), location_deal_path(deal.location, deal), :class => 'button', :target => target, :title => deal.title
      else
        return link_to (deal.free_gift? ? t('VIEW_DETAILS_AND_WIN') : t('VIEW_DETAILS_AND_BUY')), location_deal_path(deal.location, deal), :class => 'button', :target => target, :title => deal.title
      end
    end
  end

  def show_address(deal)
    return "" if deal.merchant.address.nil?
    html = "<div class=\"address\">"
    html += "<b>#{t('ADDRESS')}</b><br /><span>"
    html += deal.merchant.address
    html += "</span></div>"
    return html.html_safe
  end

  def show_pay_email(deal, price_info)
    html = ''
    if deal.pay_label.present?
      pay_label=deal.pay_label.split(" ")
      pay_label.each do |p|
        if p.to_i != 0
          html += "<span style=\"font-family: 'HelveticaRoundedLTBoldCnRg',Arial,Helvetica,sans-serif; color: rgb(255, 0, 96); font-size: 30px; letter-spacing: -1px; margin-bottom: 15px;\">#{p.to_s}</span>"
        else
          html +=p
        end
        html +=' '
      end
    else
      upto = (price_info[:pay] > price_info[:pay_min] &&
          price_info[:pay] == price_info[:pay_max])
      html += "Pay "
      html += 'Upto' if upto
      html += "<br /><span style=\"font-family: 'HelveticaRoundedLTBoldCnRg',Arial,Helvetica,sans-serif; color: rgb(255, 0, 96); font-size: 30px; letter-spacing: -1px; margin-bottom: 15px;\">#{price_info[:pay].to_i}</span> "
      html +='<br/>Taka'
    end
    return html.html_safe
  end

  def show_discount_email(deal, price_info, page = 'show')
    html=''
    if deal.discount_label.present?
      discount_label=deal.discount_label.split(" ")
      discount_label.each do |d|
        if d.to_i != 0
          html += "<br/><span style=\"font-family: 'HelveticaRoundedLTBoldCnRg',Arial,Helvetica,sans-serif; color: rgb(255, 0, 96); font-size: 30px; letter-spacing: -1px; margin-bottom: 15px;\">" +d.to_s+"</span>"
        else
          html += d
        end
        html += ' '
      end
      html += '</span>'
    else
      upto = (price_info[:discount] > price_info[:discount_min])
      html= upto ? '<b>Dis. </b>' : "<b>Discount</b> "
      html += 'Upto' if upto
      html += "<br /><span style=\"font-family: 'HelveticaRoundedLTBoldCnRg',Arial,Helvetica,sans-serif; color: rgb(255, 0, 96); font-size: 35px; letter-spacing: -1px; margin-bottom: 15px;\">#{price_info[:discount].to_i}%"
      html += '</span>'
    end
    return html.html_safe
  end

  def show_save_email(deal, price_info, page = 'past')
    html = ''
    if deal.save_label.present?
      save_label=deal.save_label.split(" ")
      save_label.each do |p|
        if p.to_i != 0
          html += "<br /><span style=\"font-family: 'HelveticaRoundedLTBoldCnRg',Arial,Helvetica,sans-serif; color: rgb(255, 0, 96); font-size: 30px; letter-spacing: -1px; margin-bottom: 15px;\">#{p.to_s}</span><br />"
        else
          html +=p
        end
        html +=' '
      end
    else
      upto = (price_info[:save] > price_info[:save_min] &&
          price_info[:save] == price_info[:save_max])
      html += "Save"
      html += 'Upto' if upto
      html += "<br /><span style=\"font-family: 'HelveticaRoundedLTBoldCnRg',Arial,Helvetica,sans-serif; color: rgb(255, 0, 96); font-size: 30px; letter-spacing: -1px; margin-bottom: 15px;\">#{price_info[:save].to_i}</span> "
      html +='<br/>Taka'
    end
    return html.html_safe
  end

  def show_price_label_email(deal, price_info)
    if deal.price_label.present?
      html = deal.price_label
      html = html.sub("__", "#{deal.discounted_price.to_i}")
      html = html.sub("??", "#{deal.actual_price.to_i}")
    else
      upto = (price_info[:original] > price_info[:original_min] &&
          price_info[:original] == price_info[:original_max])

      html = "Original Price #{'Upto ' if upto} #{price_info[:original].to_i} Taka"
    end
    return html.html_safe
  end

  def render_recent_deals
    deals = Deal.past.parent_deal.for(params[:location_id]).in_categories([0]).order_by_rating.limit(SITE_CONFIG['past_deal_count_to_show'])
    render :partial => "deals/recent_deals", :locals => {:deals => deals}
  end

  def render_featured_travels
    travels = Deal.live.parent_deal.featured.in_categories(1).for(params[:location_id]).order_by_rating()
    render :partial => "deals/featured_travels", :locals => {:travels => travels}
  end

  def render_featured_blink
    travels = Deal.live.parent_deal.featured.in_categories(2).for(params[:location_id]).order_by_rating()
    render :partial => "deals/featured_blink", :locals => {:travels => travels}
  end

  def render_featured_deals
    if params[:category_id].to_i == 1
      deals = Deal.live.parent_deal.featured.in_categories([1]).for(params[:location_id]).order_by_rating().includes(:translations)
    elsif params[:category_id].to_i == 2
      deals = Deal.live.parent_deal.featured.in_categories([2]).for(params[:location_id]).order_by_rating().includes(:translations)
    else
      deals = Deal.live.parent_deal.featured.in_categories([0]).for(params[:location_id]).order_by_rating().includes(:translations)
    end
    render :partial => "deals/deal", :collection => deals
  end

end
