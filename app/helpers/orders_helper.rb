module OrdersHelper
  def show_conditions(order)
    if order[:conditions].present?
      return order[:conditions]
    elsif (order.parent.present? && order.parent[:conditions].present?)
      return order.parent[:conditions]
    end
    return "<div style=\"height: 150px;\"></div>"
  end

  def link_to_order_payment(order)
    if order.deal.present?
      location_deal_order_payment_by_mobile_url(order.deal.location, order.deal, order, order.latest_payment)
    elsif order.deal && order.deal.product.present?
      location_deal_product_order_payment_by_mobile_url(order.product.location, order.product, order, order.latest_payment)
    elsif order.product.present?
      location_product_order_payment_by_mobile_url(order.product.location, order.product, order, order.latest_payment)
    end
  end

  def link_to_referral_payment(order)
    if order.deal.present?
      location_deal_order_payment_referral_url(order.deal.location, order.deal, order, order.latest_payment)
    elsif order.deal && order.deal.product.present?
      location_deal_product_order_payment_referral_url(order.product.location, order.product, order, order.latest_payment)
    elsif order.product.present?
      location_product_order_payment_referral_url(order.product.location, order.product, order, order.latest_payment)
    end
  end

  def link_to_order_checkout(order)
    if order.product.present?
      location_product_order_checkout_url(order.product.location, order.product, order)
    elsif order.deal.present?
      location_deal_order_checkout_url(order.deal.location, order.deal, order)
    elsif order.deal.product.present?
      location_deal_product_order_checkout(order.deal.product.location, order.deal, order.deal.product, order)
    end
  end

  def link_to_order_show(order)
    if order.product.present?
      location_product_order_url(order.product.location, order.product, order)
    elsif order.deal.present?
      location_deal_order_url(order.deal.location, order.deal, order)
    elsif order.deal.product.present?
      location_deal_product_order_url(order.deal.product.location, order.deal, order.deal.product, order)
    end
  end

  def link_to_order_book_path(order)
    if order.product.nil?
      return location_deal_order_book_path(order.deal.location, order.deal, order)
    elsif order.deal.nil?
      return location_product_order_book_path(order.product.location, order.product, order)
    else
      return location_deal_product_order_book_path(order.deal.location, order.deal, order.product, order)
    end
  end

  def show_friend_checkbox? (order)
    if (order.deal.present? && (order.deal.free_coupon? || order.deal.free_gift? ))  || !order.online_payable?
      return false
    end

    if (order.deal.present? && order.product.present?) || order.product.present?
      if order.size.present?
        return order.product.available_without_wait_count?(order.size_id)
      end
      return !order.product.booked_without_wait_count?
    elsif order.deal.present? && order.deal.gilt?
      return !order.deal.booked_without_wait_count?
    end
    return true
  end

  def label_for_order_button(order)
    button_title = t('BUY')
    if order.deal.present?
      button_title = t('GET_COUPON') if order.deal.free_gift?
      button_title = t('GET_FREE_COUPON') if order.deal.free_coupon?
    end
    return button_title
  end

  # In case of multiple payment, An user can use referral credit with other payment method if his referral credit is less than order amount
  def update_order_by_referral(order)
    if params[:use_referral].to_i == 1 && order && order.user.referral_balance.to_f <= order.amount.to_f
      order.referral_credit = order.user.referral_balance.to_f
      current_user.referral_credit = 0
      current_user.save!
      order.save!
    end
  end

end
