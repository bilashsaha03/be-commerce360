class OrderMailer < ActionMailer::Base
  layout 'system_email'
  default :from => "Akhoni.com <#{SITE_CONFIG['system_email']['order']}>"

  def booking_email(order)
    @order = order
    mail(:to => "#{order.user.name} <#{order.user.email}>",:subject => '[Akhoni.com] - Booking Confirmation')
  end

  def money_receipt_email(payment)
    @payment = payment
    @user = payment.order.user
    mail(:to => "#{@user.name} <#{@user.email}>",:subject => '[Akhoni.com] - Money Receipt')
  end

  def incomplete_email(order)
    @order = order
    mail(:to => "#{order.user.name} <#{order.user.email}>",:subject => '[Akhoni.com] - Incomplete Payment')
  end

  def coupon_email(order)
    @coupon_url = coupon_link(order)
    @user = order.user
    mail(:to => "#{@user.name} <#{@user.email}>",:subject => '[Akhoni.com] - Order Confirmation and Coupon')
  end

  def gift_receiver_email(payment)
    @order = payment.order
    @coupon_url = coupon_link_gift(@order)
    mail(:to => "#{@order.gift.firstname} #{@order.gift.lastname} <#{@order.gift.email}>",:subject => "[Akhoni.com] - Gift from #{@order.user.name_or_email}")
  end

  def gift_sender_email(payment)
    @order = payment.order
    @coupon_url = coupon_link(@order)
    mail(:to => "#{@order.user.name_or_email} <#{@order.user.email}>",:subject => '[Akhoni.com] - Gift Confirmation')
  end

  def cancel_email(order, recipient )
    @order = order
    @recipient  = recipient
    mail(:to => @recipient[:email], :subject => "[Akhoni.com] - Order - [#{@order.security_code_booking}] canceled")
  end

  def free_gift_email(order)
    @order = order
    mail(:to => "#{order.user.name} <#{order.user.email}>", :subject => '[Akhoni.com] - Booking Confirmation')
  end

  def order_email(order)
    @order = order

  end

  private
  def coupon_link(order)
    return order.deal.present? ? location_deal_order_coupon_url(order.deal.location, order.deal, order):
    location_product_order_coupon_url(order.product.location, order.product, order)
  end

   def coupon_link_gift(order)
    return order.deal.present? ? location_deal_order_coupon_url(order.deal.location, order.deal, order, :gcode => order.gift.verification_code):
    location_product_order_coupon_url(order.product.location, order.product, order, :gcode => order.gift.verification_code)
  end

end
