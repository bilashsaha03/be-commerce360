class NotificationMailer < ActionMailer::Base
  include_fragment_caching
  layout 'newsletter'
  default :from => "Akhoni.com <#{SITE_CONFIG['system_email']['newsletter']}>"
  add_template_helper(DealsHelper)

  def routine_email(notification, user)
    before_deliver()
    @user = user
    @notification = notification
    mail(:from => "#{notification.from_name} <#{SITE_CONFIG['system_email']['contact']}>",
         :to => "#{user.name} <#{user.email}>",
         :subject => notification.subject)
  end

  def ceo_email(notification, user)
    before_deliver()
    @user = user
    @notification = notification
    mail(:from => "#{notification.from_name} <#{SITE_CONFIG['system_email']['ceo']}>",
         :to => "#{user.name} <#{user.email}>",
         :subject => notification.subject)
    after_deliver()
  end

  def newsletter_email(notification, user, deal)
    before_deliver()
    @notification = notification
    @user = user
    @deal = deal

    @referral = Referral.create_referral(user)
    mail(:from => "#{notification.from_name} <#{SITE_CONFIG['system_email']['newsletter']}>",
         :to => "#{user.name} <#{user.email}>",
         :subject => notification.subject)
    after_deliver()
  end

  def pre_designed_html_email(notification, user)
    @user = user
    @notification = notification
    @referral = Referral.create_referral(user)
    mail(:from => "#{notification.from_name} <#{SITE_CONFIG['system_email']['newsletter']}>",
         :to => "#{user.name} <#{user.email}>",
         :subject => notification.subject) do |format|
      format.html { render :layout => false, :template => "../../#{@notification.designed_html.url}"}
    end
  end

  private
  def before_deliver()
    ActionMailer::Base.delivery_method = :ses_newsletter
  end

  def after_deliver()
    ActionMailer::Base.delivery_method = :ses_system_message
  end
end

