class MerchantMailer < ActionMailer::Base
  layout 'system_email'

  def send_approval_notification(product,current_user)
    receivers = ["info@akhoni.com"]
    @product = product
    @current_user = current_user
    mail(:to => receivers, :from => "info@akhoni.com", :subject => '[Akhoni.com] - Request for Approval')
  end

  def send_approved_notification(item)
    receivers = item.merchant.users.collect{|user| user.email}
    @item = item
    name = @item.class.to_s == 'Deal' ? item.title : item.name
    mail(:to => receivers, :from => "info@akhoni.com", :subject => "[Akhoni.com] - Your #{@item.class.to_s.downcase} (#{name}) has been approved")
  end

  def send_order_notification(order)
    @order = order
    receivers = order.orderable.merchant.users.collect{|user| user.email}
    item_name = order.orderable.class.to_s == 'Deal' ? order.orderable.title : order.orderable.name
    mail(:to => receivers, :from => "info@akhoni.com", :subject => "[Akhoni.com] - You have a new order of #{item_name}")
  end

end
