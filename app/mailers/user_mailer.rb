class UserMailer < ActionMailer::Base
  layout 'system_email'
  default :from => "Akhoni.com <#{SITE_CONFIG['system_email']['noreply']}>"

  def welcome_email(user)
    @user = user
    mail(:to => "#{user.name} <#{user.email}>",:subject => 'Welcome to Akhoni.com')
  end

  def referral_credit_email(referral_credit)
    @user = referral_credit.referred_by
    @friend = referral_credit.referred_to
    mail(:to => "#{@user.name} <#{@user.email}>", :subject => "You have earned #{Referral::CREDIT[:buy].to_i} Tk. credit")
  end
end
