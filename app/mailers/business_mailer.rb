class BusinessMailer < ActionMailer::Base
  layout 'system_email'
  default :from => "Akhoni.com <#{SITE_CONFIG['system_email']['business']}>"

  def get_featured(business)
    @business = business
    mail( :to => "#{business.contact_person_name} <#{business.email}>",:subject => 'Thank you for your interest in Akhoni.com')
  end

  def get_featured_sales_notification(business)
    @business = business
    mail( :to => "Akhoni.com - Business <#{SITE_CONFIG['system_email']['business']}>", :subject => 'Get Featured Request' )
  end
end
