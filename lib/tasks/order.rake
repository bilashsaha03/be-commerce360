namespace :order do
  task :mark_as_unpaid => :environment do
    puts 'Disabling notification'
    free_deals = Deal.where(:type_id => 3)
    free_deals.update_all(:skip_notification => true)
    free_deals.each do |deal|
      deal.orders.update_all(:is_notified => false)
      puts "Marking the orders as unpaid for deal - #{deal.title}"
      payments = Payment.where("payments.order_id in (select id from orders where deal_id = #{deal.id})")
      payments.update_all(" method='', status = 'INITIALIZE', paid_at = null ")
      puts "Deal ID - #{deal.id}"
    end
    puts 'Enabling notification'
    free_deals.update_all(:skip_notification => false)
    puts 'Completed to mark orders as unpaid'
  end
end