#
# This rake namespace is for updating(with default english data) model data for bangla content
#
namespace :translations do
  desc 'Updates bangla deals for existing deals with existing english content'
  task :update_bangla_deals => :environment do
    update_bangla_with_english_content_for(Deal)
    puts 'Bangla Deals translated with english content has been completed!'
  end

  desc 'Updates bangla product for existing products with existing english content'
  task :update_bangla_products => :environment do
    update_bangla_with_english_content_for(Product)
    puts 'Bangla Products translated with english content has been completed!'
  end

  desc 'Updates bangla merchants for existing merchants with existing english content'
  task :update_bangla_merchants => :environment do
    update_bangla_with_english_content_for(Merchant)
    puts 'Bangla Merchant translated with english content has been completed!'
  end

  def update_bangla_with_english_content_for(item)
    translated_fields = item.translated_attribute_names
    I18n.locale = :en
    english_items = item.all

    english_items.each do |item|
      I18n.locale = :en
      item_map = item.attributes.delete_if { |attr, val| !translated_fields.include?(attr.to_sym) }
      I18n.locale = :bn
      item.update_attributes(item_map)
      puts 'Proccessing - ' + item.id.to_s
    end
  end

end