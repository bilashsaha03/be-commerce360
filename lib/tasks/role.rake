namespace :role do
  desc 'Updates User\'s Role to 32(member) to all users whose role_mask is NULL'
  task :make_member => :environment do
    User.where("roles_mask is NULL").update_all(:roles_mask => 32)
    puts 'done!'
  end
end