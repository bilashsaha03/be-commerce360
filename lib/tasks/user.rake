#
# For User related rake tasks
#
namespace :user do
  desc "Updates all existing user's Phone number to corrected format(i,e 8801711333444)"
  task :format_phone => :environment do
    users = User.all(:conditions => ["phone IS NOT NULL"])
    for user in users do
      format_phone_number_of(user)
      puts 'Processing for user id ' + user.id.to_s
    end
    puts 'Total Processed Users for phone is ' + users.size.to_s
  end

  def format_phone_number_of(user)
    corrected_phone = user.phone.strip.delete(' ').delete('+').delete('-')
    corrected_phone = '88' + corrected_phone if !corrected_phone.match(/^88/)
    user.phone = corrected_phone
    corrected_phone = nil if !user.valid_phone?
    user.update_attribute(:phone, corrected_phone)
  end
end