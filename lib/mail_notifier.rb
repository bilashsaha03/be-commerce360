class MailNotifier
  def self.notify(row = nil)
    if row.present?
      MailNotifier.process_notification(row)
    else
      Notification.pending.each do |notification|
        NotificationsUser.of(notification.id).pending.each do |row|
          begin
            MailNotifier.process_notification(row)
          rescue Exception => error
          end
        end
      end
    end
  end

  def self.test_notification(notification, user)
    MailNotifier::process_mailer(notification, user)
  end

  def self.process_notification(row)
    row.update_attribute(:is_sent, true)
    MailNotifier::process_mailer(row.notification, row.user) if row.user.is_subscribed?
  end

  def self.process_mailer(notification, user)
    if notification.template == 'newsletter'
      NotificationMailer.newsletter_email(notification, user, notification.deal).deliver()
    elsif notification.template == 'ceo_notification'
      NotificationMailer.ceo_email(notification, user).deliver()
    elsif notification.template == 'pre_designed_html'
      NotificationMailer.pre_designed_html_email(notification, user).deliver()
    else
      NotificationMailer.routine_email(notification, user).deliver()
    end
  end
end
