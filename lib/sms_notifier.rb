class SmsNotifier
  def self.client
    @@client ||= Savon::Client.new(SMS_CONFIG["wsdl"])
  end

  def self.soap_actions
    return client.wsdl.soap_actions
  end

  def self.request(action, parameters)
    response = client.request(action) { |soap| soap.body = parameters }
    return response.to_hash
  end

  def self.invoke(action, parameters)
    response = client.send(action) { |soap| soap.body = parameters }
    return response.to_hash
  end

  def self.send_coupon_sms(coupon, phone_number = nil)
    return if !Rails.env.production? || phone_number.nil?
    message = SMS_CONFIG["Message"]
    message = message.sub("_FRIEND_NAME_", coupon.order.gift.present? ? "Gift From #{coupon.order.user.name_or_email.truncate(28)}," : '')
    message = message.sub("_COUPON_CODE_", "#{coupon.coupon_code}")
    message = message.sub("_QUANTITY_", "#{coupon.quantity}")
    message = message.sub("_PAID_AMOUNT_", "#{coupon.amount}")
    message = message.sub("_ITEM_NAME_", "#{coupon.item_name.truncate(30)}")
    message = message.sub("_VALID_DATE_", "#{coupon.valid_to.to_date}")
    if coupon.order.orderable.advance_payable?
      message = message.sub("_PAY_TO_MERCHANT_", "#{coupon.merchant_payable_amount}")
    else
      message = message.sub("Pay to Merchant: _PAY_TO_MERCHANT_,","")
    end
    message = message.sub("_MERCHANT_", "#{coupon.merchant_name.truncate(20)}")
    parameters = {'Username' => SMS_CONFIG["Username"],
                  'Password' => SMS_CONFIG["Password"],
                  'From' => SMS_CONFIG["From"],
                  'To'=> phone_number,
                  'Message' => message
    }
    response = self.request("send_text_message", parameters)
    return SmsNotification.save_response(response, message, coupon)
  end

  def self.send_mwallet_verification_sms(payment)
    return if !Rails.env.production? || payment.order.user.phone.nil?
    message =  "Your Mobile Transaction has been verified and BDT #{payment.amount} has been received successfully from your bKash account. -Akhoni.com"
    parameters = {'Username' => SMS_CONFIG["Username"],
                  'Password' => SMS_CONFIG["Password"],
                  'From' => SMS_CONFIG["From"],
                  'To'=> payment.order.user.phone,
                  'Message' => message
    }
    response = self.request("send_text_message", parameters)
    return SmsNotification.save_response(response, message)
  end

  def self.check_status(message_id)
    parameters = {'Username' => SMS_CONFIG["Username"], 'Password' => SMS_CONFIG["Password"], 'MessageId' => message_id.to_s}
    response = client.request("get_message_status") { |soap| soap.body = parameters }
    return response.to_hash
  end

end
