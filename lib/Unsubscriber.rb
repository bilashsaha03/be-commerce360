class Unsubscriber
  require 'net/imap'
  require 'openssl'

  attr_accessor :host, :port, :username, :password

  def initialize(settings)
    self.host = settings['host']
    self.port = settings['port']
    self.username = settings['username']
    self.password = settings['password']
  end

  def unsubscribe(folder = 'INBOX')
    client = Net::IMAP.new(self.host, self.port, true, nil, false)
    client.login(self.username, self.password)
    client.select(folder)
    client.search(["UNSEEN"]).each do |message_id|
      envelope = client.fetch(message_id, 'ENVELOPE')[0].attr['ENVELOPE']
      body = client.fetch(message_id, 'RFC822.TEXT')[0].attr['RFC822.TEXT']
      body = body.html_safe
      save_log(parse_and_prepare_log(envelope, body))
      client.store(message_id, '+FLAGS', [:SEEN])
    end
    client.logout
    client.disconnect
  end

  private
  def parse_and_prepare_log(envelope, body = '')
    #INITILIZE LOG NOTIFICATION
    log_notification = LogEmailNotification.new()
    #DEFAULT VALUES FROM ENVELOPE
    mail_sender = "#{envelope.sender[0].mailbox}@#{envelope.sender[0].host}"
    log_notification.receiver_email = mail_sender
    #RECEIVER AND SENDER
    to = nil
    from = body.scan(/From:.+\b([a-zA-Z0-9._%+-]+@akhoni.com)/).uniq
    to_arr = body.scan(/To:.+\b[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}\b/).uniq
    to = to_arr[0].scan(/\b([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4})\b/).uniq if to_arr.present? && to_arr.size > 0
    log_notification.receiver_email = to[0][0] if to.present?
    log_notification.notification_email = from[0][0] if from.present?
    #IP ADDRESS
    reqexp_ip = /\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/
    log_notification.ip_address = body.scan(reqexp_ip).uniq[0]
    log_notification.ip_address = envelope.subject.to_s.scan(reqexp_ip).uniq[0] if log_notification.ip_address.nil?
    #SENT DATE
    log_notification.sent_at = body.scan(/\Date: .+/).uniq[0].to_s.gsub("Date: ", "").to_datetime
    log_notification.subject = body.scan(/Subject: .+/).uniq[0].to_s.gsub("Subject: ", "")

    case mail_sender
      when "complaints@email-abuse.amazonses.com"
        log_notification.is_complained = true
        log_notification.bounce_type = 'Complain'
      when "MAILER-DAEMON@email-bounces.amazonses.com"
        log_notification.is_bounced = true
        log_notification.bounce_type = 'Delivery Failure'
        log_notification.is_verification_failed = true if log_notification.subject == 'Welcome to Akhoni.com'
      else
        #log_notification.receiver_email = '' if self.username != 'newsletter-unsubscribe@akhoni.com'
        log_notification.subject = envelope.subject
        log_notification.sent_at = envelope.date
        log_notification.is_unsubscribed = true
        log_notification.bounce_type = 'Unsubscribe'
    end

    return log_notification
  end

  def save_log(log_notification)
    return false if log_notification.receiver_email.nil?
    user = User.find_by_email(log_notification.receiver_email)
    return false if user.nil?
    log_notification.receiver = user
    log_notification.save
    return true
  end
end