class PaymentVerifier
  def self.client
    @@client ||= Savon::Client.new(PAYMENT_CONFIG["wsdl"])
  end

  def self.soap_actions
    return client.wsdl.soap_actions
  end

  def self.request(action, parameters)
    response = client.request(action) { |soap| soap.body = parameters }
    return response.to_hash
  end

  def self.invoke(action, parameters)
    response = client.send(action) { |soap| soap.body = parameters }
    return response.to_hash
  end
end
