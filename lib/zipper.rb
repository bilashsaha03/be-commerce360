class Zipper
  require 'zip/zip'
  require 'fileutils'

  def self.decompress(file_name, source, destination = nil)
    destination = source.to_s.sub('.zip', '') if destination.nil?     
    if File.exists?(source)
      FileUtils.rm_rf(destination) if File.exists?(destination)
      Zip::ZipFile.open(source) do |zip_file|
        zip_file.each do |f|
          f_path = File.join(destination, f.name)
          FileUtils.mkdir_p(File.dirname(f_path))
          zip_file.extract(f, f_path) unless File.exist?(f_path)
        end
      end
    end
  end
end