ENV["RAILS_ENV"] = "test"
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'mocha'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.(yml|csv) for all tests in alphabetical order.
  #
  # Note: You'll currently still have to declare fixtures explicitly in integration tests
  # -- they do not yet inherit this setting
  fixtures :all

  # Add more helper methods to be used by all tests here...
 def mock_model(klass, options = {})

    default_types = {
    :integer => (1000 + rand(1000)), :string => 'DefaultString', :text => 'DefaultText',
    :datetime => Time.now.utc, :date => Date.today, :boolean => 0, :float => 1.0}

    mock_klass = mock(klass.to_s)
    attributes = {}

    # for each column, we'll set the attribute hash equal to the default value for the column type
    klass.columns.reject{|c| c unless default_types.has_key?(c.type) }.each do |col|
      attributes[col.name.to_sym] = default_types[col.type.to_sym]
    end
    attributes[:to_param] = mock_klass.id.to_s

    klass_with_name = mock('klass_with_name')
    klass_with_name.stubs(:name).returns(klass.to_s)

    # options passed to this method should override default_types
    attributes.merge!(options)

    # Now we'll stub out our mock
    attributes.each do |k,v|
      mock_klass.stubs(k.to_sym).returns(v)
    end

    # this is for mocking associations:
    mock_klass.stubs(:[]=)

    # we want is_a? to return false
    mock_klass.stubs(:is_a?).returns(false)
    # unless called with the class this mock was made from
    mock_klass.stubs(:is_a?).with(klass).returns(true)

    mock_klass.stubs(:new_record?).returns(false)
    mock_klass.stubs(:class).returns(klass_with_name)

    # return the mock
    mock_klass
  end
end
