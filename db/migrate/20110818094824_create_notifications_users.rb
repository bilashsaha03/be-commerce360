class CreateNotificationsUsers < ActiveRecord::Migration
  def self.up
    create_table :notifications_users do |t|
      t.integer :user_id
      t.integer :notification_id
      t.boolean :is_sent, :default => false
      t.timestamps
    end

    Notification.all.each do |notification|
      notification.send_to_users.split(',').each do |email|
        if email.present?
          user = User.find_by_email(email)
          if user.present?
            NotificationsUser.create!({:user_id         => user.id,
                                      :notification_id => notification.id,
                                      :created_at      => notification.created_at,
                                      :updated_at      => notification.created_at,
                                      :is_sent         => true
                                     })
          end
        end
      end
    end

    remove_column :notifications, :send_to_users
  end

  def self.down
    drop_table :notifications_users
  end
end
