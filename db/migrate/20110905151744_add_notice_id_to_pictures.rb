class AddNoticeIdToPictures < ActiveRecord::Migration
  def self.up
    add_column :pictures, :notice_id, :integer
  end

  def self.down
    remove_column :pictures, :notice_id
  end
end
