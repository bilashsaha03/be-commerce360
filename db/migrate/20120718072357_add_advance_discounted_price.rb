class AddAdvanceDiscountedPrice < ActiveRecord::Migration
  def self.up
    add_column :deals, :discounted_price_for_advance_payment, :integer, :default => 0
    add_column :products, :discounted_price_for_advance_payment, :integer, :default => 0
  end

  def self.down
    remove_column :deals, :discounted_price_for_advance_payment
    remove_column :products, :discounted_price_for_advance_payment
  end
end
