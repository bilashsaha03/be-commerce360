class MerchantsTranslation < ActiveRecord::Migration
  def self.up
    Merchant.create_translation_table!({
        :name => :string,
        :address => :text
      }, {
        :migrate_data => true
      })
  end

  def self.down
    Deal.drop_translation_table! :migrate_data => true
  end
end
