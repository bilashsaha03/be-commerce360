class AddParentIdToDeals < ActiveRecord::Migration
  def self.up
    add_column :deals, :parent_id, :integer
  end

  def self.down
    remove_column :deals, :parent_id
  end
end
