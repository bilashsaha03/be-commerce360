class MoveReferralCreditFieldUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :referral_credit, :decimal
    drop_table :referral_credits
  end

  def self.down
    remove_column :users, :referral_credit
  end
end
