class AddHighlightsAndConditionsToDeals < ActiveRecord::Migration
  def self.up
    add_column :deals, :highlights, :text
    add_column :deals, :conditions, :text
  end

  def self.down
    remove_column :deals, :highlights
    remove_column :deals, :conditions
  end
end
