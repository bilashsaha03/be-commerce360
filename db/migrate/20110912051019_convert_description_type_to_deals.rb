class ConvertDescriptionTypeToDeals < ActiveRecord::Migration
  def self.up
    change_column :deals, :description, :string, :null => false

    Deal.all.each do |deal|
      desc = deal.description.gsub(/<p>/, '')
      desc = desc.gsub(/<\/p>/, '')
      deal.update_attribute(:description, "#{desc}")
    end
  end

  def self.down
    change_column :deals, :description, :text
  end
end
