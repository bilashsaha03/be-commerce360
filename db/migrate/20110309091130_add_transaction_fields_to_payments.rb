class AddTransactionFieldsToPayments < ActiveRecord::Migration
  def self.up
    add_column :payments, :updated_at, :datetime
    add_column :payments, :order_id, :integer
    add_column :payments, :transaction_id, :string
    add_column :payments, :validation_token, :string
    add_column :payments, :status, :string
  end

  def self.down
    remove_column :payments, :updated_at
    remove_column :payments, :order_id
    remove_column :payments, :transaction_id
    remove_column :payments, :validation_token
    remove_column :payments, :status
  end
end
