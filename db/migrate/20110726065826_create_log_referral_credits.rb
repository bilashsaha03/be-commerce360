class CreateLogReferralCredits < ActiveRecord::Migration
  def self.up
    create_table :log_referral_credits do |t|
      t.integer :referred_by_id
      t.integer :referred_to_id
      t.string :event
      t.datetime :first_visit
      t.decimal :amount

      t.timestamps
    end
  end

  def self.down
    drop_table :log_referral_credits
  end
end
