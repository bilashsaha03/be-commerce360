class AddRedeemToOrders < ActiveRecord::Migration
  def self.up
    add_column :orders, :redeemed_at, :datetime
    add_column :orders, :redeemed_by_user_id, :integer
  end

  def self.down
    remove_column :orders, :redeemed_by_user_id
    remove_column :orders, :redeemed_at
  end
end
