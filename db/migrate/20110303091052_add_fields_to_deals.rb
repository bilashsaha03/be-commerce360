class AddFieldsToDeals < ActiveRecord::Migration
  def self.up
    add_column :deals, :valid_from, :date
    add_column :deals, :valid_to, :date
    add_column :deals, :max_order_limit, :integer
  end

  def self.down
    remove_column :deals, :valid_from
    remove_column :deals, :valid_to
    remove_column :deals, :max_order_limit
  end
end
