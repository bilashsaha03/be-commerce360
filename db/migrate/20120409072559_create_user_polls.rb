class CreateUserPolls < ActiveRecord::Migration
  def self.up
    create_table :user_polls do |t|
      t.integer :poll_id
      t.integer :user_id
      t.integer :option_id

      t.timestamps
    end
  end

  def self.down
    drop_table :user_polls
  end
end
