class AddPaySaveDiscountToDeals < ActiveRecord::Migration
  def self.up
    add_column :deals, :pay_label, :string
    add_column :deals, :save_label, :string
    add_column :deals, :discount_label, :string

  end

  def self.down
    remove_column :deals, :pay_label
    remove_column :deals, :save_label
    remove_column :deals, :discount_label
  end
end
