class CreateGifts < ActiveRecord::Migration
  def self.up
    create_table :gifts do |t|
      t.integer  :order_id
      t.integer  :user_id
      t.string   :verification_code
      t.timestamps
    end
  end

  def self.down
    drop_table :gifts
  end
end
