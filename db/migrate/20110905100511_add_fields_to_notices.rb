class AddFieldsToNotices < ActiveRecord::Migration
 def self.up
    add_column :notices, :created_by, :integer, :default => nil
    add_column :notices, :updated_by, :integer, :default => nil
    add_column :notices, :is_deleted, :boolean, :default => false
  end

  def self.down
    remove_column :notices, :created_by
    remove_column :notices, :updated_by
    remove_column :notices, :is_deleted
  end
end
