class AddTypeIdToDeals < ActiveRecord::Migration
  def self.up
    add_column :deals, :type_id, :integer, :size => 3, :null => false, :default => 1
    #Migrate old data
    Deal.where(:is_gilt => true).update_all(:type_id => 2)
    Deal.where(:is_free => true).update_all(:type_id => 3)
    #Remove column
    remove_column :deals, :is_free
    remove_column :deals, :is_gilt
  end

  def self.down
    add_column :deals, :is_free, :boolean, :default => false, :null => false
    add_column :deals, :is_gilt, :boolean, :default => false, :null => false
    #Migrate old data
    Deal.where(:type_id => 2).update_all(:is_gilt => true)
    Deal.where(:type_id => 3).update_all(:is_free => true)
    Deal.where(:type_id => 4).update_all(:is_free => true)
    #Remove column
    remove_column :deals, :type_id
  end
end
