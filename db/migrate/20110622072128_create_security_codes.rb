class CreateSecurityCodes < ActiveRecord::Migration
  def self.up
    create_table :security_codes do |t|
      t.string :code
      t.string :entity
      t.integer :entity_id
      t.datetime :issued_at
      t.datetime :valid_upto

      t.timestamps
    end
  end

  def self.down
    drop_table :security_codes
  end
end
