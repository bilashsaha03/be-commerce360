class AddCanViewCustomerContactToMerchant < ActiveRecord::Migration
  def self.up
    add_column :merchants, :can_view_customer_contact, :boolean, :default => false
  end

  def self.down
    remove_column :merchants, :can_view_customer_contact
  end
end
