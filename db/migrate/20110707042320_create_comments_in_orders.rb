class CreateCommentsInOrders < ActiveRecord::Migration
  def self.up
    add_column :orders, :comment, :string
    add_column :orders, :commented_by, :integer
  end

  def self.down
   remove_column :orders, :comment, :string
   remove_column :orders, :commented_by, :integer
  end
end
