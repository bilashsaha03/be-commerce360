class AddCommentInPaymentSummaries < ActiveRecord::Migration
  def self.up
    add_column :payment_summaries, :comment, :string
  end

  def self.down
    remove_column :payment_summaries, :comment
  end
end
