class AddStatusToNotifications < ActiveRecord::Migration
  def self.up
    remove_column :notifications, :is_delivered
    add_column :notifications, :status, :string, :default => 'Draft'
    Notification.update_all("status = 'Sent'")
  end

  def self.down
    remove_column :notifications, :status
  end
end
