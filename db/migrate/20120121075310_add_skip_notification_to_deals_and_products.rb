class AddSkipNotificationToDealsAndProducts < ActiveRecord::Migration
  def self.up
    add_column :deals, :skip_notification, :boolean, :default => false, :null => false
    add_column :products, :skip_notification, :boolean, :default => false, :null => false
  end

  def self.down
    remove_column :deals, :skip_notification
    remove_column :products, :skip_notification
  end
end
