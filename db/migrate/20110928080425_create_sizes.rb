class CreateSizes < ActiveRecord::Migration
  def self.up
    create_table :sizes do |t|
      t.integer  :category_id
      t.string   :name
      t.string   :description
    end
  end

  def self.down
    drop_table :sizes
  end
end
