class AddOrderViewEnableToMerchants < ActiveRecord::Migration
  def self.up
    add_column :merchants, :is_enable_orders_view, :boolean, :default => false
  end

  def self.down
    remove_column :merchants, :is_enable_orders_view
  end
end
