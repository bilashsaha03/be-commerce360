class AddBlinkPriojonToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :blink_priyojon, :boolean, :default => false
  end

  def self.down
    remove_column :users, :blink_priyojon
  end
end
