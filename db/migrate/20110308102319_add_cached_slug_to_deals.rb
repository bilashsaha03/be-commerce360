class AddCachedSlugToDeals < ActiveRecord::Migration
  def self.up
    add_column :deals, :cached_slug, :string
    add_index  :deals, :cached_slug, :unique => true
  end

  def self.down
    remove_column :deals, :cached_slug
  end
end
