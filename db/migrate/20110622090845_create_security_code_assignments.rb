class CreateSecurityCodeAssignments < ActiveRecord::Migration
  def self.up
    create_table :security_code_assignments do |t|
      t.references :security_code
      t.string :entity
      t.integer :entity_id

      t.timestamp :created_at
      t.timestamp :valid_upto
    end
  end

  def self.down
    drop_table :security_code_assignments
  end
end
