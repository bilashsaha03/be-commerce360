class ChangeDefaultDealIdInOrders < ActiveRecord::Migration
  def self.up
   change_column :orders, :deal_id, :integer, :null => true, :default => 0
  end

  def self.down
  change_column :orders, :deal_id, :integer, :null => false, :default => nil
  end
end
