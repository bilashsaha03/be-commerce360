class CreatePaymentReceives < ActiveRecord::Migration
  def self.up
    create_table :payment_receives do |t|
      t.integer :summary_id
      t.integer :receiver_id
      t.integer :received_amount
      t.date    :received_at
      t.string  :comments
      t.timestamps
    end
  end

  def self.down
    drop_table :payment_receives
  end
end
