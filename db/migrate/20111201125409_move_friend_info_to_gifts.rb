class MoveFriendInfoToGifts < ActiveRecord::Migration
  def self.up
    add_column :gifts, :email, :string
    add_column :gifts, :firstname, :string
    add_column :gifts, :lastname, :string
    add_column :gifts, :address, :string
    add_column :gifts, :phone, :string
  end

  def self.down
    remove_column :gifts, :email
    remove_column :gifts, :firstname
    remove_column :gifts, :lastname
    remove_column :gifts, :address
    remove_column :gifts, :phone
  end
end
