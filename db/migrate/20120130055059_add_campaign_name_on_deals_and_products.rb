class AddCampaignNameOnDealsAndProducts < ActiveRecord::Migration
  def self.up
    add_column :deals, :campaign_name, :string
    add_column :products, :campaign_name, :string
  end

  def self.down
    remove_column :deals, :campaign_name
    remove_column :products, :campaign_name
  end
end
