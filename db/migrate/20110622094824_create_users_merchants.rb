class CreateUsersMerchants < ActiveRecord::Migration
  def self.up
    create_table :users_merchants do |t|
      t.references :user
      t.references :merchant
      t.string       :role
      t.string       :status

      t.timestamps
    end
  end

  def self.down
    drop_table :users_merchants
  end
end
