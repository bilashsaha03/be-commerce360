class CreatePaymentSummaries < ActiveRecord::Migration
  def self.up
    create_table :payment_summaries do |t|
      t.integer :collector_id
      t.integer :deal_id
      t.integer :collection_amount
      t.integer :due_amount
      t.timestamps
    end
  end

  def self.down
    drop_table :payment_summaries
  end
end
