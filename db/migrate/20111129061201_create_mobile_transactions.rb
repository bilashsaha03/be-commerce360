class CreateMobileTransactions < ActiveRecord::Migration
  def self.up
    create_table :mobile_transactions do |t|
      t.string :mobile_number
      t.decimal :amount
      t.string :transaction_id
      t.integer :user_id
      t.boolean :is_used

      t.timestamps
    end
  end

  def self.down
    drop_table :mobile_transactions
  end
end
