class CreateBusinesses < ActiveRecord::Migration
  def self.up
    create_table :businesses do |t|
      t.string :name, :null => false
      t.string :category, :null => false
      t.string :website, :null => true
      t.string :city, :null => false
      t.string :contact_person_name, :null => false
      t.string :email, :null => false
      t.string :phone, :null => false
      t.text :notes, :null => false
      t.timestamps
    end
  end

  def self.down
    drop_table :businesses
  end
end
