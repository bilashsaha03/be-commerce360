class MoveCouponAndVerificationCodesToCouponsTable < ActiveRecord::Migration
  def self.up
    Order.all.each do |order|
      Coupon.create(
          {
              :order_id => order.id,
              :quantity => order.quantity,
              :security_code_coupon => order.security_code_coupon,
              :security_code_verification => order.security_code_verification,
              :redeemed_at => order.redeemed_at,
              :redeemed_by_user_id => order.redeemed_by_user_id
          }
      )
    end

    remove_column :orders, :security_code_coupon
    remove_column :orders, :security_code_verification
    remove_column :orders, :redeemed_at
    remove_column :orders, :redeemed_by_user_id
  end

  def self.down
    add_column :orders, :security_code_coupon, :string
    add_column :orders, :security_code_verification, :integer
    add_column :orders, :redeemed_at, :datetime
    add_column :orders, :redeemed_by_user_id, :integer

    Coupon.all.each do |coupon|
      coupon.order.update_attributes(
          {
              :security_code_coupon => coupon.security_code_coupon,
              :security_code_verification => coupon.security_code_verification,
              :redeemed_at => coupon.redeemed_at,
              :redeemed_by_user_id => coupon.redeemed_by_user_id
          }
      )
    end
  end
end
