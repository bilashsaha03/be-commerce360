class AddAdvancePayableInDealsAndProducts < ActiveRecord::Migration
  def self.up
    add_column :deals, :advance_payable, :boolean, :default => false
    add_column :products, :advance_payable, :boolean, :default => false
  end

  def self.down
    remove_column :deals, :advance_payable
    remove_column :products, :advance_payable
  end
end
