class AddIsActiveToProductSizesAndProducts < ActiveRecord::Migration
  def self.up
    add_column :product_sizes, :is_active, :boolean, :default => true
    add_column :products, :is_active, :boolean, :default => true
  end

  def self.down
    remove_column :product_sizes, :is_active
    remove_column :products, :is_active
  end
end
