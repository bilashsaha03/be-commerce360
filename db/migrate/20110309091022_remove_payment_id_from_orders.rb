class RemovePaymentIdFromOrders < ActiveRecord::Migration
  def self.up
    remove_column :orders, :payment_id
  end

  def self.down
    add_column :orders, :payment_id, :integer
  end
end
