class CreatePayments < ActiveRecord::Migration
  def self.up
    create_table :payments do |t|
      t.float :amount, :default => 0.0
      t.integer :collector_id
      t.string :method
      t.datetime :paid_at
    end
  end

  def self.down
    drop_table :payments
  end
end
