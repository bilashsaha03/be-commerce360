class CreatePresses < ActiveRecord::Migration
  def self.up
    create_table :presses do |t|
      t.string :title
      t.text :description
      t.text :url
      t.datetime :published_at
      t.has_attached_file :logo

      t.timestamps
    end
  end

  def self.down
    drop_table :presses
  end
end
