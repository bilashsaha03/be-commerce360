class AlterDescriptionToDeals < ActiveRecord::Migration
  def self.up
    change_column :deals, :description, :string
  end

  def self.down
    change_column :deals, :description, :text
  end
end
