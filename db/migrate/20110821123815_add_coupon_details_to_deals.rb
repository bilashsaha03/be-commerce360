class AddCouponDetailsToDeals < ActiveRecord::Migration
  def self.up
    add_column :deals, :is_show_logo, :boolean, :default => true, :null => false
    add_column :deals, :coupon_header, :string, :default => nil
    add_column :deals, :is_show_value, :boolean, :default => true, :null => false
    add_column :deals, :is_show_paid_amount, :boolean, :default => true, :null => false
  end

  def self.down
    remove_column :deals, :is_show_logo
    remove_column :deals, :coupon_header
    remove_column :deals, :is_show_value
    remove_column :deals, :is_show_paid_amount
  end
end
