class MakeDueAndCollectionAmountDefaultZero < ActiveRecord::Migration
  def self.up
    change_column_default :payment_summaries, :collection_amount,  0
    change_column_default :payment_summaries, :due_amount,  0

  end

  def self.down
    change_column_default :payment_summaries, :collection_amount, nil
    change_column_default :payment_summaries, :due_amount, nil
  end
end
