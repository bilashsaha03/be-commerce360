class AddMerchantIdToPictures < ActiveRecord::Migration
  def self.up
    add_column :pictures, :merchant_id, :integer
  end

  def self.down
    remove_column :pictures, :merchant_id
  end
end
