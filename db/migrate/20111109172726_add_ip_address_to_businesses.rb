class AddIpAddressToBusinesses < ActiveRecord::Migration
  def self.up
    add_column :businesses, :ip_address, :string,
  end

  def self.down
    remove_column :businesses, :ip_address
  end
end
