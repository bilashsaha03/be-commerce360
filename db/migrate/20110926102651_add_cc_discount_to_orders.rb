class AddCcDiscountToOrders < ActiveRecord::Migration
  def self.up
    add_column :orders, :cc_discount, :integer ,:default => 0
  end

  def self.down
    remove_column :orders, :cc_discount
  end
end
