class AddSocialMediaContentToDeals < ActiveRecord::Migration
  def self.up
    add_column :deals, :social_media_sharing_content, :string
    Deal.all.each do |deal|
      sharing_content = "#{deal.highest_discount}% Discount on #{deal.description} at #{deal.title}"
      sharing_content = "Upto #{sharing_content}" if deal.sub_deals.any?
      deal.update_attribute(:social_media_sharing_content, sharing_content)
    end
  end

  def self.down
    remove_column :deals, :social_media_sharing_content
  end
end
