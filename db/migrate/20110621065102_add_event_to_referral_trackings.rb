class AddEventToReferralTrackings < ActiveRecord::Migration
  def self.up
    add_column :referral_trackings, :event, :string
  end

  def self.down
    remove_column :referral_trackings, :event
  end
end
