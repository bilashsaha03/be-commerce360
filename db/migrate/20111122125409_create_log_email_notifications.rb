class CreateLogEmailNotifications < ActiveRecord::Migration
  def self.up
    create_table :log_email_notifications do |t|
      t.integer  :receiver_id
      t.string   :receiver_email
      t.integer  :notification_id
      t.string   :notification_email
      t.string   :subject
      t.datetime :sent_at
      t.string   :ip_address
      t.string   :bounce_type
      t.boolean  :is_bounced, :default => false
      t.boolean  :is_complained, :default => false
      t.boolean  :is_unsubscribed, :default => false
      t.boolean  :is_verification_failed, :default => false
      t.text     :notes
      t.datetime :created_at
    end
  end

  def self.down
    drop_table :log_email_notifications
  end
end
