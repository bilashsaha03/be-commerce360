class CreateLogFailedRedeems < ActiveRecord::Migration
  def self.up
    create_table :log_failed_redeems do |t|
      t.references :user
      t.references :order
      t.datetime :created_at
    end
  end

  def self.down
    drop_table :log_failed_redeems
  end
end
