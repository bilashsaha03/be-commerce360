class AddReferralCreditInOrders < ActiveRecord::Migration
  def self.up
    add_column :orders, :referral_credit, :decimal, :default => 0
  end

  def self.down
    remove_column :orders, :referral_credit
  end
end
