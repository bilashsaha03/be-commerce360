class ChangeDateFormatToDatetimeInDeals < ActiveRecord::Migration
  def self.up
    change_column :deals, :start_date, :datetime
    change_column :deals, :end_date, :datetime
    change_column :deals, :valid_from, :datetime
    change_column :deals, :valid_to, :datetime
  end

  def self.down
    change_column :deals, :start_date, :date
    change_column :deals, :end_date, :date
    change_column :deals, :valid_from, :date
    change_column :deals, :valid_to, :date
  end
end
