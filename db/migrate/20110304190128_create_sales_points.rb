class CreateSalesPoints < ActiveRecord::Migration
  def self.up
    create_table :sales_points do |t|
      t.string :name
      t.integer :user_id
      t.text :address
      t.text :description
      t.float :lat
      t.float :long

      t.timestamps
    end
  end

  def self.down
    drop_table :sales_points
  end
end
