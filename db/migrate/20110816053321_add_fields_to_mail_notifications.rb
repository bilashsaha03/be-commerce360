class AddFieldsToMailNotifications < ActiveRecord::Migration
  def self.up
    remove_column :mail_notifications, :sent_date
    remove_column :mail_notifications, :updated_at
    rename_column :mail_notifications, :email_address, :send_to_users

    add_column :mail_notifications, :template, :string
    add_column :mail_notifications, :from_name, :string
    add_column :mail_notifications, :deal_id, :integer
    add_column :mail_notifications, :is_delivered, :boolean, :default => false, :null => false
  end

  def self.down
    add_column :mail_notifications, :sent_date, :date
    add_column :mail_notifications, :updated_at, :datetime
    rename_column :mail_notifications, :send_to_users, :email_address

    remove_column :mail_notifications, :template
    remove_column :mail_notifications, :from_name
    remove_column :mail_notifications, :deal_id
    remove_column :mail_notifications, :is_delivered
  end
end
