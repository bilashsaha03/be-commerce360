class AddHasStoreEnabledToMerchants < ActiveRecord::Migration
  def self.up
    add_column :merchants, :has_store_enabled, :boolean, :default => false
  end

  def self.down
    remove_column :merchants, :has_store_enabled
  end
end
