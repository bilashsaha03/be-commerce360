class AddVerificationCodeToOrders < ActiveRecord::Migration
  def self.up
    add_column :orders, :security_code_verification, :integer
  end

  def self.down
    remove_column :orders, :security_code_verification
  end
end
