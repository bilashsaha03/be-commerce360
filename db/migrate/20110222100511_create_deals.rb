class CreateDeals < ActiveRecord::Migration
  def self.up
    create_table :deals do |t|
      t.integer :merchant_id
      t.integer :location_id
      t.string  :title
      t.text    :description
      t.text    :background
      t.date    :start_date
      t.date    :end_date
      t.float   :actual_price , :null => false, :default => 0
      t.float   :discounted_price , :null => false, :default => 0
      t.integer :min_quantity
      t.integer :max_quantity
      t.integer :copyrighter_id,  :null => false
      t.integer :manager_id
      t.string  :status
      t.string  :picture
      t.timestamps
    end
  end

  def self.down
    drop_table :deals
  end
end
