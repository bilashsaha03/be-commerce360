class AddIsEnableMcouponToDealsAndProducts < ActiveRecord::Migration
  def self.up
    add_column :deals, :is_enable_mcoupon, :boolean, :default => true
    add_column :products, :is_enable_mcoupon, :boolean, :default => true
    change_column :deals, :is_featured, :boolean, :default => true
  end

  def self.down
    remove_column :deals, :is_enable_mcoupon
    remove_column :products, :is_enable_mcoupon
    change_column :deals, :is_featured, :boolean, :default => false
  end
end
