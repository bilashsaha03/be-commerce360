class AddWaitCountAndWaitMessageInDealsAndProducts < ActiveRecord::Migration
  def self.up
    add_column :deals, :wait_count, :integer, :default => 0
    add_column :products, :wait_count, :integer, :default => 0

    add_column :deals, :wait_message, :text
    add_column :products, :wait_message, :text
  end

  def self.down
    remove_column :deals, :wait_count
    remove_column :products, :wait_count

    remove_column :deals, :wait_message
    remove_column :products, :wait_message
  end
end
