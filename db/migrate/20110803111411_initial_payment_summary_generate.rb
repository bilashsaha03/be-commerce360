class InitialPaymentSummaryGenerate < ActiveRecord::Migration
  def self.up
    PaymentSummary.auto_generate
  end
end
