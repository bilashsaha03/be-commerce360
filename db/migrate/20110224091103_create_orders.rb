class CreateOrders < ActiveRecord::Migration
  def self.up
    create_table :orders do |t|
      t.integer :deal_id, :null => false
      t.integer :user_id, :null => false
      t.integer :payment_id
      t.integer :quantity, :null => false, :default => 1
      t.datetime :booking_at
      t.datetime :confirmed_at
      t.integer :confirmed_by
      t.string :security_code_order
      t.string :security_code_booking
      t.string :security_code_coupon
    end
  end

  def self.down
    drop_table :orders
  end
end
