class AddSuspendFieldsToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :is_active, :boolean, :default=>true, :null=>false
    add_column :users, :suspended_at, :datetime
    add_column :users, :suspended_reason, :string
  end

  def self.down
    remove_column :users, :is_active
    remove_column :users, :suspended_at
    remove_column :users, :suspended_reason
  end
end
