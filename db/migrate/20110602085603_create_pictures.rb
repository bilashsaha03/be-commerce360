class CreatePictures < ActiveRecord::Migration
  def self.up
    create_table :pictures do |t|
      t.integer :deal_id
      t.string :data_file_name
      t.string :data_content_type
      t.integer :data_file_size
      t.timestamps
    end

    #MIGRATE DATA
#    old_image_path = "#{Rails.root}/public/images/deals"
#    new_image_path = "#{Rails.root}/public/images/pictures"
#    Dir::mkdir(new_image_path) if !File.exists?(new_image_path)
#
#    Deal.all.each do |deal|
#      picture = Picture.new({:deal_id => deal.id,
#                             :data_file_name => deal.picture_file_name,
#                             :data_content_type => deal.picture_content_type,
#                             :data_file_size => deal.picture_file_size})
#      if picture.save
#        Dir::mkdir("#{new_image_path}/#{picture.id}") if !File.exists?("#{new_image_path}/#{picture.id}")
#        Dir.entries("#{old_image_path}/#{deal.id}").each do |i|
#          if i !='.' && i !='..'
#            FileUtils.cp_r Dir["#{old_image_path}/#{deal.id}/**"], "#{new_image_path}/#{picture.id}"
#          end
#        end
#      end
#    end

    #REMOVE COLUMN FROM DEAL
    remove_column :deals, :picture_file_name
    remove_column :deals, :picture_content_type
    remove_column :deals, :picture_file_size
  end

  def self.down
    #ADDED COLUMN TO DEAL
    add_column :deals, :picture_file_name, :string
    add_column :deals, :picture_content_type, :string
    add_column :deals, :picture_file_size, :integer

    #MIGRATE DATA
#    old_image_path = "#{Rails.root}/public/images/deals";
#    new_image_path = "#{Rails.root}/public/images/pictures";
#    Dir::mkdir(old_image_path) if !File.exists?(old_image_path)
#
#    Picture.group(:deal_id).all.each do |picture|
#      attributes = {:picture_file_name => picture.data_file_name,
#                    :picture_content_type => picture.data_content_type,
#                    :picture_file_size => picture.data_file_size}
#      picture.deal.update_attributes(attributes)
#
#      Dir::mkdir("#{old_image_path}/#{picture.deal_id}") if !File.exists?("#{old_image_path}/#{picture.deal_id}")
#      Dir.entries("#{new_image_path}/#{picture.id}").each do |f|
#        if f !='.' && f !='..'
#          FileUtils.cp_r Dir["#{new_image_path}/#{picture.id}/**"], "#{old_image_path}/#{picture.deal_id}/"
#        end
#      end
#    end

    drop_table :pictures
  end
end
