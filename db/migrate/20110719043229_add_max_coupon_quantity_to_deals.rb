class AddMaxCouponQuantityToDeals < ActiveRecord::Migration
  def self.up
    add_column :deals, :max_coupon_quantity, :integer, :default => nil
  end

  def self.down
    remove_column :deals, :max_coupon_quantity
  end
end
