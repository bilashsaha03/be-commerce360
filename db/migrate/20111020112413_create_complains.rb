class CreateComplains < ActiveRecord::Migration
  def self.up
    create_table :complains do |t|
      t.integer  :user_id
      t.string   :ip_address
      t.datetime :complained_at
      t.string   :notes
      t.string   :source
      t.timestamps
    end
  end

  def self.down
    drop_table :complains
  end
end
