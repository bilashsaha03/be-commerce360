class CreatePollOptions < ActiveRecord::Migration
  def self.up
    create_table :poll_options do |t|
      t.text :option
      t.integer :poll_id
      t.integer :vote_count, :default => 0

      t.timestamps
    end
  end

  def self.down
    drop_table :poll_options
  end
end
