class RenameMailNotificationsToNotifications < ActiveRecord::Migration
  def self.up
    rename_table :mail_notifications, :notifications
  end

  def self.down
    rename_table :notifications, :mail_notifications
  end
end
