class AddFieldsToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :firstname, :string
    add_column :users, :lastname, :string
    add_column :users, :phone, :string

    add_index :users, :phone, :unique => true
  end

  def self.down
    remove_column :users, :phone
    remove_column :users, :lastname
    remove_column :users, :firstname
  end
end
