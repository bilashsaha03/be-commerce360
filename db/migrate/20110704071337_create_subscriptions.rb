class CreateSubscriptions < ActiveRecord::Migration
  def self.up
    create_table :subscriptions do |t|
      t.string :email, :null => false
      t.integer :location_id, :null => true
      t.boolean :is_active, :null => false, :default => true
      t.timestamps
    end
  end

  def self.down
    drop_table :subscriptions
  end
end
