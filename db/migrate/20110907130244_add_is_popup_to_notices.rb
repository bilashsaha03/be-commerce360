class AddIsPopupToNotices < ActiveRecord::Migration
  def self.up
    add_column :notices, :is_popup, :boolean, :default => true, :null => false
  end

  def self.down
    remove_column :notices, :is_popup
  end
end
