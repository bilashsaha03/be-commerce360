class AddFieldsToProductsForPromotion < ActiveRecord::Migration
  def self.up
    add_column :products, :promotional_code, :string
    add_column :products, :promotional_discount, :float, :default => 0

  end

  def self.down
    remove_column :products, :promotional_code
    remove_column :products, :promotional_discount
  end
end
