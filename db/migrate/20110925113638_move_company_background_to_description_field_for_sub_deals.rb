class MoveCompanyBackgroundToDescriptionFieldForSubDeals < ActiveRecord::Migration
  def self.up
    Deal.all.each do |deal|
      if !deal.parent_deal? && deal.background.present?
        desc = deal.background.gsub(/(<[^>]*>)|\n|\r|&nbsp;|\t/s) {''}
        deal.update_attributes(:title => "#{desc}", :background => nil)
      end
    end
  end

  def self.down
  end
end
