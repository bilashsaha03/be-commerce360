class AddSummaryToDeals < ActiveRecord::Migration
  def self.up
    add_column :deals, :summary, :text
  end

  def self.down
    remove_column :deals, :summary
  end
end
