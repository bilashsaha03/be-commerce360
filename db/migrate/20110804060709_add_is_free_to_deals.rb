class AddIsFreeToDeals < ActiveRecord::Migration
  def self.up
    add_column :deals, :is_free, :boolean, :default => false, :null => false
  end

  def self.down
    remove_column :deals, :is_free
  end
end
