class CreateCorporateDiscounts < ActiveRecord::Migration
  def self.up
    create_table :corporate_discounts do |t|
      t.string  :domain
      t.float   :discount, :null=>false
      t.integer :item_id, :null=>false
      t.string  :item_type
      t.timestamps

    end
  end

  def self.down
    drop_table :corporate_discounts
  end
end