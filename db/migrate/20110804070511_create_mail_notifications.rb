class CreateMailNotifications < ActiveRecord::Migration
  def self.up
    create_table :mail_notifications do |t|
      t.string :email_address
      t.string  :subject
      t.text    :content
      t.date    :sent_date
      t.integer :sent_by,  :null => false
      t.timestamps
    end
  end

  def self.down
    drop_table :mail_notifications
  end
end
