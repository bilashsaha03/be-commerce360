class AddIsActiveToCategories < ActiveRecord::Migration
  def self.up
    add_column :categories, :is_active, :boolean, :default => false
  end

  def self.down
    remove_column :categories, :is_active
  end
end
