class CreateSettings < ActiveRecord::Migration
  def self.up
    create_table :settings do |t|
      t.string :meta_title
      t.string :meta_keywords
      t.string :meta_description
      t.string :multiple
      t.string :logo
      t.string :current_deal_label
      t.string :company_address
      t.string :office_phone
      t.string :business_phone
      t.string :hotline_number
      t.string :contact_number
      t.string :ceo_email
      t.string :contact_email
      t.string :business_email
      t.string :employment_email
      t.string :newsletter_email
      t.string :order_confirmation_email
      t.string :operating_time
      t.integer :past_deal_count_to_show
      t.string :background_image_path
      t.timestamps
    end
  end

  def self.down
    drop_table :settings
  end
end
