class AddUrlNameAndFeaturedToMerchants < ActiveRecord::Migration
  def self.up
    add_column :merchants, :url_name, :string
    add_column :merchants, :is_featured, :boolean, :default => false
  end

  def self.down
    remove_column :merchants, :url_name
    remove_column :merchants, :is_featured
  end
end
