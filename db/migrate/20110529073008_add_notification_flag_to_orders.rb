class AddNotificationFlagToOrders < ActiveRecord::Migration
  def self.up
    add_column :orders, :is_notified, :boolean
    add_column :orders, :notified_at, :datetime

    Order.all.each do |order|
      if order.paid?
        latest_payment = order.latest_payment
        if latest_payment.paid_at.nil?
          latest_payment.update_attributes({:paid_at => latest_payment.updated_at})
        end
        order.update_attributes({:is_notified => 1, :notified_at => latest_payment.paid_at})
      end
    end
  end

  def self.down
    remove_column :orders, :is_notified
    remove_column :orders, :notified_at
  end
end
