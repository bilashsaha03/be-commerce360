class CreateDealTranslations < ActiveRecord::Migration
  def self.up
    Deal.create_translation_table!({
        :title => :string,
        :description => :text,
        :background => :text,
        :highlights => :text,
        :conditions => :text,
        :price_label => :string,
        :pay_label => :string,
        :save_label => :string,
        :discount_label => :string
      }, {
        :migrate_data => true
      })
  end

  def self.down
    Deal.drop_translation_table!
  end
end
