class CreateProductSizes < ActiveRecord::Migration
  def self.up
    create_table :product_sizes do |t|
      t.integer :product_id
      t.integer :size_id
    end
    remove_column :products, :size_id
  end

  def self.down
    drop_table :product_sizes
    add_column :products, :size_id, :integer
  end
end
