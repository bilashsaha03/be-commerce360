class AddIsPayByCardToProducts < ActiveRecord::Migration
  def self.up
    add_column :products, :is_pay_by_card, :boolean, :default => true
  end

  def self.down
    remove_column :products, :is_pay_by_card
  end
end
