class CreateAuthorizations < ActiveRecord::Migration
  def self.up
    create_table :authorizations do |t|
      t.references :user
      t.string :provider
      t.string :uid
      t.string :firstname
      t.string :lastname
      t.string :nickname
      t.string :url
      t.text :credentials

      t.timestamps
    end
  end

  def self.down
    drop_table :authorizations
  end
end
