class RemoveSubscriptions < ActiveRecord::Migration
  def self.up
    add_column :users, :is_registered, :boolean, :default => true

    subscriptions = execute("select * from subscriptions")
    subscriptions.each do |subscription|
      user = User.find_by_email(subscription[1])
      if user.present?
        user.update_attribute(:is_subscribed, subscription[3])
      else
        User.create({:email => subscription[1],
                     :is_subscribed => subscription[3],
                     :is_registered => false})
      end
    end

    drop_table :subscriptions
  end

  def self.down
    create_table :subscriptions do |t|
      t.string :email, :null => false
      t.integer :location_id, :null => true
      t.boolean :is_active, :null => false, :default => true
      t.timestamps
    end

    User.where('is_registered = 0').each do |user|
      execute("insert into subscriptions(email, is_active) values('#{user.email}','#{user.is_subscribed}')")
    end

    remove_column :users, :is_registered
  end
end
