class DefaultCreditToReferralCredit < ActiveRecord::Migration
  def self.up
    change_column_default :referral_credits, :credit, 0
  end

  def self.down
    change_column_default :referral_credits, :credit, nil
  end
end
