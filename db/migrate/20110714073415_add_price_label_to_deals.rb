class AddPriceLabelToDeals < ActiveRecord::Migration
  def self.up
    add_column :deals, :price_label, :string
  end

  def self.down
    remove_column :deals, :price_label
  end
end
