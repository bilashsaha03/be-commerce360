class AddPolymorphicFieldsToReferrals < ActiveRecord::Migration
  def self.up
    remove_column :referrals, :action
    add_column :referrals, :referable_id, :integer
    add_column :referrals, :referable_type, :string
  end

  def self.down
    remove_column :referrals, :referable_id
    remove_column :referrals, :referable_type
  end
end
