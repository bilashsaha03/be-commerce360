class AddIsFeaturedToDeals < ActiveRecord::Migration
  def self.up
    add_column :deals, :is_featured, :boolean, :default => false
  end

  def self.down
    remove_column :deals, :is_featured
  end
end
