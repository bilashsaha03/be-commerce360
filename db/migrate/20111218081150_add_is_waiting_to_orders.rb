class AddIsWaitingToOrders < ActiveRecord::Migration
  def self.up
    add_column :orders, :is_waiting, :boolean, :default => false
  end

  def self.down
     remove_column :orders, :is_waiting
  end
end
