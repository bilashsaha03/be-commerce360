class MakeReceivedAmountDefaultZero < ActiveRecord::Migration
  def self.up
    change_column_default :payment_receives, :received_amount, 0
  end

  def self.down
    change_column_default :payment_receives, :received_amount, nil
  end
end


