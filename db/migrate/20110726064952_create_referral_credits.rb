class CreateReferralCredits < ActiveRecord::Migration
  def self.up
    create_table :referral_credits do |t|
      t.references :user
      t.decimal :credit
      t.timestamps
    end
  end

  def self.down
    drop_table :referral_credits
  end
end
