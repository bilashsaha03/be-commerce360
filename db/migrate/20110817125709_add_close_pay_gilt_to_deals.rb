class AddClosePayGiltToDeals < ActiveRecord::Migration
  def self.up
    add_column :deals, :is_close, :boolean, :default => false, :null => false
    add_column :deals, :is_pay_by_card, :boolean, :default => true, :null => false
    add_column :deals, :is_gilt, :boolean, :default => false, :null => false
  end

  def self.down
    remove_column :deals, :is_close
    remove_column :deals, :is_pay_by_card
    remove_column :deals, :is_gilt
  end
end
