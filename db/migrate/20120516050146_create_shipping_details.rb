class CreateShippingDetails < ActiveRecord::Migration
  def self.up
    create_table :shipping_details do |t|
      t.integer :order_id
      t.decimal :amount
      t.text :address

      t.timestamps
    end
    add_index :shipping_details, :order_id
  end

  def self.down
    drop_table :shipping_details
  end
end
