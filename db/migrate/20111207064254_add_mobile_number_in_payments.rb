class AddMobileNumberInPayments < ActiveRecord::Migration
  def self.up
    add_column :payments, :mobile_number, :string
  end

  def self.down
    remove_column :payments, :mobile_number
  end
end
