class AddFieldsToDealsForPromotion < ActiveRecord::Migration
  def self.up
    add_column :deals, :promotional_code, :string
    add_column :deals, :promotional_discount, :float, :default => 0

  end

  def self.down
    remove_column :deals, :promotional_code
    remove_column :deals, :promotional_discount
  end
end
