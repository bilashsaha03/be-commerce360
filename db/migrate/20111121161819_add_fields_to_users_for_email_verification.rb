class AddFieldsToUsersForEmailVerification < ActiveRecord::Migration
  def self.up
    add_column :users, :is_verified, :boolean, :default => true
    add_column :users, :bounce_count, :integer, :default => 0
    add_column :users, :complain_count, :integer, :default => 0
  end

  def self.down
    remove_column :users, :is_verified
    remove_column :users, :bounce_count
    remove_column :users, :complain_count
  end
end
