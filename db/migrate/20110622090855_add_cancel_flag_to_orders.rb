class AddCancelFlagToOrders < ActiveRecord::Migration
  def self.up
    add_column :orders, :canceled_by, :integer, :default => 0
    add_column :orders, :canceled_at, :datetime
  end

  def self.down
    remove_column :orders, :canceled_by
    remove_column :orders, :canceled_at
  end
end