class AddDiscountCcPaymentToDeals < ActiveRecord::Migration
  def self.up
    add_column :deals, :discount_cc_payment, :integer ,:default => 0
  end

  def self.down
    remove_column :deals, :discount_cc_payment
  end
end
