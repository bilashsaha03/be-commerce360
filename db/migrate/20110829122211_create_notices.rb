class CreateNotices < ActiveRecord::Migration
  def self.up
    create_table :notices do |t|
      t.string :popup_title
      t.text :popup_desc
      t.string :popup_image
      t.string :widget_title
      t.string :widget_logo
      t.string :widget_footer
      t.datetime :start_date_time
      t.datetime :end_date_time
      t.boolean :published, :default => false
      t.timestamps
    end
  end

  def self.down
    drop_table :notices
  end
end
