class CreateBanners < ActiveRecord::Migration
  def self.up
    create_table :banners do |t|
      t.string :title
      t.string :link
      t.integer :rank
      t.boolean :is_active
      t.string :position
      t.text :description

      t.timestamps
    end
    add_column :banners, :picture_file_name, :string
    add_column :banners, :picture_content_type, :string
    add_column :banners, :picture_file_size, :integer
  end

  def self.down
    #
    #remove_attachment :banners, :picture
    drop_table :banners
  end
end
