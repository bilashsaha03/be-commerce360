class CreateCoupons < ActiveRecord::Migration
  def self.up
    create_table :coupons do |t|
      t.integer  :order_id
      t.integer  :quantity
      t.string   :security_code_coupon
      t.integer  :security_code_verification
      t.datetime :redeemed_at
      t.integer  :redeemed_by_user_id
      t.datetime :created_at
    end
  end

  def self.down
    drop_table :coupons
  end
end
