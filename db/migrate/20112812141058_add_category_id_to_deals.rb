class AddCategoryIdToDeals < ActiveRecord::Migration
  def self.up
    add_column :deals, :category_id, :integer, :default => 0
  end

  def self.down
     remove_column :deals, :category_id
  end
end
