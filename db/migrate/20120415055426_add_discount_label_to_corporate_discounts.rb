class AddDiscountLabelToCorporateDiscounts < ActiveRecord::Migration
  def self.up
    add_column :corporate_discounts, :discount_label_en, :string
    add_column :corporate_discounts, :discount_label_bn, :string
  end

  def self.down
    remove_column :corporate_discounts, :discount_label_en
    remove_column :corporate_discounts, :discount_label_bn
  end
end
