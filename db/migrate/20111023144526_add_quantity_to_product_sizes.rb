class AddQuantityToProductSizes < ActiveRecord::Migration
  def self.up
    add_column :product_sizes, :quantity, :integer, :default=>0, :null=>false
  end

  def self.down
    remove_column :product_sizes, :quantity
  end
end
