class AddDeliveryDateInOrders < ActiveRecord::Migration
  def self.up
    add_column :orders, :delivery_date, :datetime
  end

  def self.down
    remove_column :orders, :delivery_date
  end
end
