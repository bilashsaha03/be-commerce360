class RemoveInitializedPaymentEntryForPaidOrders < ActiveRecord::Migration
  def self.up
    Order.all.each do |order|
      if order.paid?
        order.payments.each do |payment|
          payment.destroy() if payment.status == 'INITIALIZE'
        end
      end
    end
  end

  def self.down
  end
end
