class AddShippingCostInfoToProductsDeals < ActiveRecord::Migration
  def self.up
    add_column :products, :weight, :decimal, :default => 0.0, :precision => 10, :scale => 2
    add_column :products, :min_shipping_weight, :decimal, :default => 0.0, :precision => 10, :scale => 2
    add_column :products, :min_shipping_cost_dhaka, :decimal, :default => 0.0, :precision => 10, :scale => 2
    add_column :products, :min_shipping_cost_bd, :decimal, :default => 0.0, :precision => 10, :scale => 2
    add_column :products, :min_shipping_cost_world, :decimal, :default => 0.0, :precision => 10, :scale => 2

    add_column :deals, :weight, :decimal, :default => 0.0, :precision => 10, :scale => 2
    add_column :deals, :min_shipping_weight, :decimal, :default => 0.0, :precision => 10, :scale => 2
    add_column :deals, :min_shipping_cost_dhaka, :decimal, :default => 0.0, :precision => 10, :scale => 2
    add_column :deals, :min_shipping_cost_bd, :decimal, :default => 0.0, :precision => 10, :scale => 2
    add_column :deals, :min_shipping_cost_world, :decimal, :default => 0.0, :precision => 10, :scale => 2

    add_column :shipping_details, :choice, :string
  end

  def self.down
    remove_column :products, :weight
    remove_column :products, :min_shipping_weight
    remove_column :products, :min_shipping_cost_dhaka
    remove_column :products, :min_shipping_cost_bd
    remove_column :products, :min_shipping_cost_world

    remove_column :deals, :weight
    remove_column :deals, :min_shipping_weight
    remove_column :deals, :min_shipping_cost_dhaka
    remove_column :deals, :min_shipping_cost_bd
    remove_column :deals, :min_shipping_cost_world

    remove_column :shipping_details, :choice
  end
end
