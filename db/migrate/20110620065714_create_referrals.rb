class CreateReferrals < ActiveRecord::Migration
  def self.up
    create_table :referrals do |t|
      t.references :user
      t.string :code
      t.string :action
      t.string :medium
      t.integer :visit_count
      t.integer :success_count

      t.timestamps
    end
  end

  def self.down
    drop_table :referrals
  end
end
