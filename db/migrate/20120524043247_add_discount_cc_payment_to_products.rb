class AddDiscountCcPaymentToProducts < ActiveRecord::Migration
  def self.up
    add_column :products, :discount_cc_payment, :integer, :default => 0
  end

  def self.down
    remove_column :products, :discount_cc_payment
  end
end
