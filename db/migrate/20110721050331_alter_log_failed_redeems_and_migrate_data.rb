class AlterLogFailedRedeemsAndMigrateData < ActiveRecord::Migration
  def self.up
    log_failed_redeems = execute("select * from log_failed_redeems")

    rename_column(:log_failed_redeems, :order_id, :coupon_id)

    log_failed_redeems.each do |log_failed_redeem|
      coupon = Coupon.find_by_order_id(log_failed_redeem[2])
      if coupon.present?
        execute("update log_failed_redeems set coupon_id = '#{coupon.id}' where id = '#{log_failed_redeem[0]}'")
      end
    end
  end

  def self.down
    log_failed_redeems = execute("select * from log_failed_redeems")

    rename_column(:log_failed_redeems, :coupon_id, :order_id)

    log_failed_redeems.each do |log_failed_redeem|
      coupon = Coupon.find_by_order_id(log_failed_redeem[2])
      if coupon.present?
        execute("update log_failed_redeems set order_id = '#{coupon.order_id}' where id = '#{log_failed_redeem[0]}'")
      end
    end
  end
end
