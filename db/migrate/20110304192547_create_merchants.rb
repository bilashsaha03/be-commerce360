class CreateMerchants < ActiveRecord::Migration
  def self.up
    create_table :merchants do |t|
      t.string :name
      t.string :logo
      t.text :address
      t.string :contact_person
      t.string :contract_number
      t.text :contact_person_detail
      t.string :email
      t.string :website
      t.float :lat
      t.float :long

      t.timestamps
    end
  end

  def self.down
    drop_table :merchants
  end
end
