class CreateProducts < ActiveRecord::Migration
  def self.up
    create_table :products do |t|
      t.integer  :deal_id, :null => false
      t.integer  :merchant_id, :null => false
      t.string   :name
      t.string   :description
      t.integer  :quantity, :null => false
      t.float    :actual_price , :null => false, :default => 0
      t.float    :discounted_price , :null => false, :default => 0
      t.integer  :size_id
      t.integer  :category_id
      t.integer  :manager_id
      t.string   :status
      t.timestamps
    end
  end

  def self.down
    drop_table :products
  end
end
