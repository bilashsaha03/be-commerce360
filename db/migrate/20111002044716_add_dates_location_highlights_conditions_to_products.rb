class AddDatesLocationHighlightsConditionsToProducts < ActiveRecord::Migration
  def self.up
    add_column :products, :start_date, :datetime
    add_column :products, :end_date, :datetime
    add_column :products, :valid_from, :datetime
    add_column :products, :valid_to, :datetime
    add_column :products, :highlights, :text
    add_column :products, :conditions, :text
    add_column :products, :location_id, :integer
    add_column :products, :cached_slug, :string
    add_index  :products, :cached_slug, :unique => true
  end

  def self.down
    remove_column :products, :start_date
    remove_column :products, :end_date
    remove_column :products, :valid_from
    remove_column :products, :valid_to
    remove_column :products, :highlights
    remove_column :products, :conditions
    remove_column :products, :location_id
    #remove_column :products, :cached_slug
  end
end