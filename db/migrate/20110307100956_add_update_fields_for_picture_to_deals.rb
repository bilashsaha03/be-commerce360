class AddUpdateFieldsForPictureToDeals < ActiveRecord::Migration
  def self.up
    rename_column :deals, :picture, :picture_file_name
    add_column :deals, :picture_content_type, :string
    add_column :deals, :picture_file_size, :integer
  end

  def self.down
    rename_column :deals, :picture_file_name, :picture
    remove_column :deals, :picture_content_type
    remove_column :deals, :picture_file_size
  end
end