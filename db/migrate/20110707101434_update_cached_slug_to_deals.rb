class UpdateCachedSlugToDeals < ActiveRecord::Migration
  def self.up
    remove_column :deals, :cached_slug
    add_column :deals, :cached_slug, :string
    add_index :deals, [:location_id, :cached_slug], :unique => true
  end

  def self.down
    remove_column :deals, :cached_slug
    add_column :deals, :cached_slug, :string
    add_index  :deals, :cached_slug, :unique => true
  end
end
