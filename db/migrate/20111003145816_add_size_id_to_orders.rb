class AddSizeIdToOrders < ActiveRecord::Migration
  def self.up
    add_column :orders, :size_id, :integer
  end

  def self.down
    remove_column :orders, :size_id
  end
end
