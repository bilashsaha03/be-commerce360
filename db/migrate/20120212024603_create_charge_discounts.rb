class CreateChargeDiscounts < ActiveRecord::Migration
  def self.up
    create_table :charge_discounts do |t|
      t.float  :amount,:null=>false
      t.integer :order_id,:null=>false
      t.string  :charge_discount_type
      t.timestamps

    end
  end

  def self.down
    drop_table :charge_discounts
  end
end