class AddDealRatingToDeals < ActiveRecord::Migration
  def self.up
    add_column :deals, :rating, :integer
  end

  def self.down
    remove_column :deals, :rating
  end
end
