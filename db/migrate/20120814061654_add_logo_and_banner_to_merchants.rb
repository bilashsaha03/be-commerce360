class AddLogoAndBannerToMerchants < ActiveRecord::Migration
  def self.up
    add_column :merchants, :logo_file_name, :string
    add_column :merchants, :logo_content_type, :string
    add_column :merchants, :logo_file_size, :integer

    add_column :merchants, :banner_file_name, :string
    add_column :merchants, :banner_content_type, :string
    add_column :merchants, :banner_file_size, :integer
  end

  def self.down
    remove_column :merchants, :logo_file_name
    remove_column :merchants, :logo_content_type
    remove_column :merchants, :logo_file_size

    remove_column :merchants, :banner_file_name
    remove_column :merchants, :banner_content_type
    remove_column :merchants, :banner_file_size
  end
end
