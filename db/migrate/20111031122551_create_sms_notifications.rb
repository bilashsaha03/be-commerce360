class CreateSmsNotifications < ActiveRecord::Migration
  def self.up
    create_table :sms_notifications do |t|
      t.integer  :coupon_id
      t.integer  :message_id
      t.string   :message
      t.integer  :status
      t.string   :status_text
      t.integer  :error_code
      t.boolean  :is_sent
      t.string   :error_text
      t.timestamps
    end
  end

  def self.down
    drop_table :sms_notifications
  end
end
