# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ :name => 'Chicago' }, { :name => 'Copenhagen' }])
#   Mayor.create(:name => 'Daley', :city => cities.first)

admin = User.new
admin.email = 'admin@email.com'
admin.password = 'admin'
admin.password_confirmation = 'admin'
admin.roles_mask = 1
admin.save

Location.create(:name => 'Dhaka', :url => 'dhaka', :is_default => true)
