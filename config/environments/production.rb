Akhoni::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb

  # The production environment is meant for finished, "live" apps.
  # Code is not reloaded between requests
  config.cache_classes = true

  # Full error reports are disabled and caching is turned on
  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true

  # Specifies the header that your server uses for sending files
  config.action_dispatch.x_sendfile_header = "X-Sendfile"

  # For nginx:
  # config.action_dispatch.x_sendfile_header = 'X-Accel-Redirect'

  # If you have no front-end server that supports something like X-Sendfile,
  # just comment this out and Rails will serve the files

  # See everything in the log (default is :info)
  # config.log_level = :debug

  # Use a different logger for distributed setups
  # config.logger = SyslogLogger.new

  # Use a different cache store in production
  # config.cache_store = :mem_cache_store

  config.cache_store = :dalli_store, '127.0.0.1', { :namespace => "akhoni", :expires_in => 1.day, :compress => true }

  # Disable Rails's static asset server
  # In production, Apache or nginx will already do this
  config.serve_static_assets = false

  # Enable serving of images, stylesheets, and javascripts from an asset server
  # config.action_controller.asset_host = "http://assets.example.com"

  # Disable delivery errors, bad email addresses will be ignored
  # config.action_mailer.raise_delivery_errors = false
  config.action_mailer.raise_delivery_errors = false
  config.action_mailer.default_url_options = { :host => 'akhoni.com' }
  config.action_mailer.delivery_method = :ses_system_message
  #config.action_mailer.smtp_settings = {
  #    :address              => "smtp.gmail.com",
  #    :port                 => 587,
  #    :domain               => 'akhoni.com',
  #    :user_name            => 'noreply-001@akhoni.com',
  #    :password             => 'orderconfirm11',
  #    :authentication       => 'plain',
  #    :enable_starttls_auto => true
  #}

  # Enable threaded mode
  # config.threadsafe!

  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation can not be found)
  config.i18n.fallbacks = true

  # Send deprecation notices to registered listeners
  config.active_support.deprecation = :notify

  # Rotating log file based on file size. The 2nd argument being the number of .log files you’d like to keep,
  # and the 3rd being the size in bytes that the files are allowed to reach before it will be rotated.
  # config.logger = Logger.new(Rails.root.join("log",Rails.env + ".log"), 10, 5*1024*1024)
  # setting the log level
  # config.logger.level = Logger::INFO
  ROOT_URL = 'http://akhoni.com'
end
