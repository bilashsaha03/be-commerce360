# ATTACHMENT
PAPERCLIP_CONFIG = YAML.load_file("#{Rails.root.to_s}/config/picture.yml")[Rails.env]
Paperclip.options[:command_path] = PAPERCLIP_CONFIG["command_path"]

#PAYMENT GATEWAY
PAYMENT_CONFIG = YAML.load_file("#{Rails.root.to_s}/config/sslcommerz.yml")[Rails.env]

#SMS GATEWAY
SMS_CONFIG = YAML.load_file("#{Rails.root.to_s}/config/sms_settings.yml")[Rails.env]

#SITE SETTINGS
SITE_CONFIG = YAML.load_file("#{Rails.root.to_s}/config/site_settings.yml")
SITE_CONFIG["email_templates"] = ['plain', 'newsletter','ceo_notification','pre_designed_html']

#PAGINATION
ITEM_PER_PAGE = 20

#HTTP BASIC AUTH USERS
HTTP_AUTH_USERS = YAML.load_file("#{Rails.root.to_s}/config/staging_server_users.yml")
PRODUCTION_ROOT_URL = 'http://akhoni.com'
