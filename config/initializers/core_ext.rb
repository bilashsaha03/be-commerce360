# encoding: utf-8

#
# All String, Fixnum, Float and DateTime class will be extended by this module
#
module CoreExt
    
  #
  # Converts to Bangla digits only when applied to Fixnum, Float, String and DateTime object
  # Example: "123".to_bddigit
  # "abc89efg".to_bddigit
  # Deal.first.start_date.to_bddigit
  # 89.567.to_bddigit
  #
  def to_bddigit
    item = self
    return item if I18n.locale == :en
    bangla_digit_map = {'0' => '০', '1' => '১', '2' => "২", '3' => '৩', '4' => '৪', '5' => '৫', '6' => '৬', '7' => '৭', '8' => '৮', '9' => '৯'}
    translated_item = ""
    item.to_s.split("").each do |c|
      if c.to_i > 0 || c == '0'
        translated_item += bangla_digit_map[c]
      else
        translated_item += c
      end
    end
    return translated_item
  end
end

[String, Fixnum, Float, ActiveSupport::TimeWithZone, Time, Date].each do |item|
  item.send(:include, CoreExt)
end

#module FileUtils
#  def self.remove_cache_dir
#    Dir.glob("#{Rails.root}/tmp/cache/*").each do |folder|
#      if File.directory?(folder)
#        FileUtils.remove_dir(folder, true)
#      end
#    end
#  end
#end