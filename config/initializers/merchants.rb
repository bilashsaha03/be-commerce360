class MerchantsRouts
  def self.valid?(merchant)
    return false if merchant.match("manage")
    merchant = CGI::unescape(merchant)
    merchant = merchant.split("/").last
    merchant = merchant.split("?").first
    return Merchant.find_by_url_name(merchant).present?
  end
end