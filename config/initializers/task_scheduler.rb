require 'rufus/scheduler'
require 'rubygems'
require 'rake'

if Rails.env.production?
  #00 34 14 - seconds minutes hours
  scheduler = Rufus::Scheduler.start_new
  scheduler.cron '00 01 00 * * 1-5' do
    #MailNotifier.notify()
    Rails.cache.clear
  end

  scheduler.cron '0 7 * * 1-5' do
    #Unsubscriber.new().unsubscribe()
  end
end

