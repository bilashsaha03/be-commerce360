Akhoni::Application.routes.draw do
  devise_for :users, :controllers => {
      :registrations => 'registrations',
      :omniauth_callbacks => "users/omniauth_callbacks"
  } do
    get "/login" => "devise/sessions#new"
    get "/logout" => "devise/sessions#destroy"
    get "/settings" => "devise/registrations#edit"
  end

  match "/:merchant_name", :to => "merchants#show", :constraints => lambda{|req| MerchantsRouts::valid?(req.env["REQUEST_URI"]) }
  match "/estores", :to => "merchants#index", :as => "stores"

  match "/:location_id/index_for_facebook/index.php", :to => "deals#index_for_facebook"
  match "/:location_id/index_for_facebook/index.html", :to => "deals#index_for_facebook"

  match "/social-media-likes-featured-deal/:deal_id", :to => "social_media#likes", :as => "social_media_likes"
  match "/social-media-facebook-like-box",  :to => "social_media#fb_like_box", :as => "fb_like_box"
  match "/social-media-google-map/:deal_id",  :to => "deals#google_map", :as => "google_map"

  match "vote", :to => "poll#vote", :as => "vote"

  #ADMIN PANEL
  namespace :manage, :as => 'admin' do
    resources :home, :businesses, :notices, :audits, :payments, :merchants, :mobile_transactions, :corporate_discounts, :presses, :polls ,:categories, :banners
    resources :referrals do
      get :reset_credits, :on => :collection
    end
    resources :coupons do
      post :resend_sms
    end
    resources :notifications do
      get :unsubscribe, :logs, :referral, :on => :collection
      get :assign_users, :preview, :schedule, :send_now, :test, :on => :member
    end
    resources :products do
      get :sizes, :on => :collection
    end
    resources :users do
      put :activate, :suspend, :on=> :member
      get :verify, :on => :collection
      get :stat_report, :on=>:collection
      get :login_as, :on => :collection
      post :login, :on => :collection
    end
    resources :deals do
      get :sort, :on => :collection
      post :update_order, :on => :collection
      get :stat_report, :on=>:collection
    end
    resources :orders do
      post :confirm, :paid, :cancel, :comment
      get :comment, :on => :collection
    end
    resources :payment_summaries do
      get :receive, :on => :collection
    end
    match 'users/verify' => 'users#verify'
    match 'UploadBackgroundImage' => 'home#new_background_image'
    match 'SaveBackgroundImage' => 'home#save_background_image'
    match 'clear_cache' => "settings#clear_cache"
    root :to => 'home#index'
  end

  #MERCHANT PANEL
  namespace :merchant do
    resources :home
    resources :products do
      get :sizes, :on => :collection
    end
    match ':id/coupons', :to => 'coupons#index', :as => 'coupons'
    match ':id/redeemed-coupons', :to => 'coupons#redeemed', :as => 'redeemed_coupons'
    match 'verify-coupon', :to => 'coupons#verify', :as => "coupon_verify"
    match 'merchants_orders', :to => 'report#merchants_orders', :as => "merchants_orders"
    match 'send_approval_notification', :to => 'products#send_approval_notification', :as => "send_approval_notification"
    match 'report(/:action(/:id))', :controller => 'report'
    root :to => 'home#index'
  end

  #STATIC PATH
  match 'getfeatured' => 'businesses#new'
  match 'getfeatured/submit' => 'businesses#create'
  match 'getfeatured/thanks' => 'businesses#thanks'
  match 'about' => 'home#about'
  #match 'HowAkhoniWorks' => 'home#how'
  match 'TermsOfUse' => 'home#terms'
  match 'PrivacyPolicy' => 'home#privacy'
  match 'PageNotFound' => 'home#page_not_found'
  match 'ContactUs' => 'home#contact'
  match 'InstantPurchase' => 'home#credit_card_purchase'
  match 'CashPurchase' => 'home#cash_purchase'
  match 'bkashInstruction' => 'home#bkash_instruction'
  match 'couponOnYourMobile' => 'home#coupon_on_your_mobile'
  match 'career' => 'home#jobs'
  match 'Press' => 'home#press'
  match '/notices/:id' => 'notices#show'
  match ':location/notices/:id' => 'notices#show'
  match ':location/:deal_id/free_coupon' => 'orders#free_coupon'
  match 'digital-mind-2012', :to=> 'home#digital_mind', :as => 'digital_mind'
  match 'deal-corporate-discount/:deal_id/:item_type', :to => 'deals#deal_corporate_discount', :as => "deal_corporate_discount"
  match 'product-corporate-discount/:product_id/:item_type', :to => 'products#product_corporate_discount', :as => "product_corporate_discount"
  match 'decon' => 'home#decon'

  resources :subscriptions, :unsubscribe, :notices
  resources :referrals do
    get :history, :on => :collection
  end

  #FRONT END
  resources :locations, :path => '' do
    get :determine, :on => :collection

    #PRODUCTS
    resources :products do
      resources :orders, {:path => 'buy'} do
        get :checkout, :coupon, :book, :print_coupon, :waiver
        resources :payments do
          post :success, :canceled, :failed, :by_mobile, :referral
          get :error, :incomplete, :invalid
        end
      end
    end

    resources :deals, :path => '' do
      get :index_for_facebook, :on => :collection
      get :deals, :to => :sub_deals, :on => :member
      get :past, :on => :collection
      get :expire, :on => :collection

      resources :products do
        resources :orders, {:path => 'buy'} do
          get :checkout, :coupon, :book, :print_coupon, :waiver
          resources :payments do
            post :success, :canceled, :failed, :by_mobile, :referral
            get :error, :incomplete, :invalid
          end
        end
      end

      resources :orders, {:path => 'buy'} do
        get :checkout, :coupon, :book, :print_coupon, :waiver
        resources :payments do
          post :success, :canceled, :failed, :by_mobile, :referral
          get :error, :incomplete, :invalid
        end
      end
    end
  end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => "locations#index"

  # See how all your routes lay out with "rake routes"
  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match 'manage/orders/:id(.:format)' => 'manage/orders#comment'
  match ':controller(/:action(/:id(.:format)))'
#  match 'report(/:action(/:id))'
end
